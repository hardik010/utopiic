import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TextInput
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import { TouchableOpacity as TouchableOpacityTwo } from 'react-native';
import ParallaxScrollView from 'react-native-parallax-scroll-view';

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';
import Carousel from 'react-native-banner-carousel';


import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList ,TouchableOpacity} from 'react-native-gesture-handler';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { color } from 'react-native-reanimated';
import HomeHeader from '../components/home_header';
import BottomTab from '../components/bottom_tab';
import ExperienceComponent from '../components/experience_component';
import config from '../config/config';
import { postRequestNoBodyApi, postRequestNoBodyApiWithToken } from '../redux/actions/api_actions';
import DestinationComponent from '../components/destination_component';
import BrandsComponent from '../components/brands_component';
import BlogComponent from '../components/blog_component';
import LinearGradient from 'react-native-linear-gradient';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder'
import ParallaxHeader from '../components/parallax_header';


const SHIMMER=[
    {'id':1,'name':'abc'},
    {'id':2,'name':'xyz'}
]

const HomeData: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const apiReducer = useSelector(state => state.apiReducer)

    const dispatch = useDispatch()

    const [banner, setBanner] = useState([]);
    const [countryList, setCountryList] = useState([]); 
    const [experienceList, setExperienceList] = useState([]); 
    const [brandList, setBrandList] = useState([]); 
    const [blogList, setBlogList] = useState([]); 

    function fetchBrands(){
        const URL = config.BASE_URL + config.GET_SUPPLIERS
        dispatch(postRequestNoBodyApiWithToken(URL,userReducer.user.token))
        .then(response=> {
           // console.log('Response - ',JSON.stringify(response))
            if(response.status){
                // if(response.data.Suppliers.length < 6) setBrandList(response.data.Suppliers)
                // else setBrandList(response.data.Suppliers.slice(0,5))
                setBrandList(response.data.Suppliers)
            }
            else showMessage(response.message)
        })
        .catch(error => {
             showMessage(error.message)
             //console.log('Error - ',error)
        })
    }

    function fetchHomeData(){
        const URL = config.BASE_URL + config.HOME
        
        dispatch(postRequestNoBodyApi(URL))
        .then(response=> {
            //console.log('Response - ',JSON.stringify(response))
            if(response.status) {
                let temp =[]
                let tempBanner =[]
                response.data.banner.map(e =>{
                    if(e.url.length > 0) temp.push(e)
                    else tempBanner.push(e)
                })
                setBanner(tempBanner)
                setCountryList(response.data.countrylist)
                setExperienceList(response.data.packages)
                setBlogList(temp)
            }
        })
        .catch(error => {
            showMessage(error.message)
            //console.log('Error - ',error)
        })
    }

    useEffect(() =>{
        fetchHomeData()
        fetchBrands()
    },[])

   function navigateAfterBanner(item){
       //console.log('click')
       let desc = item.description
       if(desc == 'Hotels') props.navigation.navigate("hotels")
       else if(desc == 'Brands') props.navigation.navigate("brands")
       else if(desc == 'Sustainable Attractions') props.navigation.navigate("attractions")
       else if(desc == 'Destinations') props.navigation.navigate("countries")
       else if(desc == 'Experiences') props.navigation.navigate("experiences")
       else showMessage("Coming Soon",false)

    }

    function renderPage(item,index){
        return (
            <TouchableOpacityTwo  onPress={()=> navigateAfterBanner(item)}
             key={index}>
                 <Image style={styles.bannerImage} 
                  resizeMode='cover'
                  source={{uri:item.image}} />
            </TouchableOpacityTwo>
        );
    }


    function navigateToExperienceDetails(item){
        props.navigation.navigate('experience_detail',{id:item.package_id})
    }

    function _renderExperiences ({item, index}){
        return (
            <TouchableOpacity onPress={()=> navigateToExperienceDetails(item)}>
           <ExperienceComponent
            object = {item} 
            index ={index}
            {...props}/>
            </TouchableOpacity>
        )
      }

      function navigateToDestinationDetails(id){
        props.navigation.navigate('country_details',{'id':id})
    }

      function _renderCountries ({item, index}){
        return (
            <TouchableOpacity onPress={()=> navigateToDestinationDetails(item.id)}>
           <DestinationComponent
            object = {item} 
            index ={index}
            {...props}/>
            </TouchableOpacity>
        )
      }

    function navigateToDetails(item){
        props.navigation.navigate('brand_details',{'item':item})
    }

      function _renderBrands ({item, index}){
        return (
        <TouchableOpacity onPress={() => navigateToDetails(item)}>
           <BrandsComponent
            object = {item} 
            index ={index}
            {...props}/>
         </TouchableOpacity>
        )
      }


      function navigateToWeb(item){
        props.navigation.navigate('view_web',{'url':item.url})
     }


      function _renderBlogs ({item, index}){
        return (
            <TouchableOpacity onPress={() => navigateToWeb(item)}>
           <BlogComponent
            object = {item} 
            index ={index}
            {...props}/>
            </TouchableOpacity>
        )
      }


      function _renderShimmer ({item, index}){
        return (
            <View>
            <ShimmerPlaceHolder 
                style={styles.shimmerStyle}
                LinearGradient={LinearGradient}
                    visible={!(apiReducer.loading)}>
                   
                </ShimmerPlaceHolder>
                 <ShimmerPlaceHolder 
                 style={styles.shimmerStyleText}
                LinearGradient={LinearGradient}
                visible={!(apiReducer.loading)}></ShimmerPlaceHolder>
             </View>
        )
      }

      function navigateToCountries(){
          props.navigation.navigate('countries')
      }

    function navigateToExperiences(){
        props.navigation.navigate('experiences')
    }

    function navigateToBrands(){
        props.navigation.navigate('brands')
    }

    function navigateToBlogs(){
        props.navigation.navigate('blog_list')
    }

    function navigateToSearch(){
        props.navigation.navigate('search')
    }

        function _onToggle(){
            console.log("___")
          props.navigation.toggleDrawer()
        }

    const HeaderTop = (props) =>{
        return(
                       <View style={styles.headerTop}>
                         <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between',padding:10}}>
                         
                                <TouchableOpacity>
                                <FastImage 
                                source={require('../assets/logo.png')}
                                style={{height:25,width:25}} 
                                resizeMode={FastImage.resizeMode.contain}/>
                            </TouchableOpacity> 
                            
                            <TouchableOpacity onPress={()=> props.navigation.toggleDrawer()}>
                                <FastImage 
                                source={require('../assets/menu.png')}
                                style={[{height:20,width:20},{marginLeft:14}]} 
                                resizeMode={FastImage.resizeMode.contain}/>
                            </TouchableOpacity>
                        </View>
                            <ImageBackground 
                            source={require('../assets/home-banner.png')}
                             style={styles.imageBackground}>
                                    <View>
                                        <Text style={[styles.heading,{fontSize:18}]}>Hi {userReducer.user.name},</Text>  
                                        <Text style={styles.heading}>Let's unlock experiences together.</Text> 
                                    </View>

                                   <View/>

                                    <TouchableOpacity onPress={navigateToSearch}
                                     style={styles.buttonContainer}>
                                            <Text style={{color:colors.COLOR_PRIMARY}}>Search ...</Text>
                                            <FastImage 
                                            source={require('../assets/search.png')}
                                            style={{height:20,width:20}} 
                                            resizeMode={FastImage.resizeMode.contain}/>
                                    </TouchableOpacity>
                        </ImageBackground>
                       </View>
        );
    }


    const Body = () =>{
        return(
            <>
               <View style={{height:20}}/>

             {
                banner.length > 0
                  ?
                  <Carousel
                  style={{alignSelf:'center'}}
                  autoplay  autoplayTimeout={4000}
                  loop index={0} pageSize={dimensions.SCREEN_WIDTH} >
                  {banner.map((image, index) => renderPage(image, index))}
              </Carousel> 
              :
              <ShimmerPlaceHolder 
              style={{height:dimensions.SCREEN_HEIGHT * 0.29,width:dimensions.SCREEN_WIDTH}}
              LinearGradient={LinearGradient}
               visible={!(apiReducer.loading)}>
              </ShimmerPlaceHolder>
              } 


            <View style={{height:20}}/>

    {/** experiences */}
    <View style={styles.listContainer}>
        <View style={styles.rowContainer}>
            <Text style={styles.listLabel}>Experiences</Text>
            <TouchableOpacity style={styles.rowContainer} onPress={()=>{navigateToExperiences()} }>
                <Text style={styles.viewLabel}>View all</Text>
                <FastImage 
                source={require('../assets/forward.png')}
                style={styles.nextImage} 
                resizeMode={FastImage.resizeMode.cover}/>
                </TouchableOpacity>
        </View>


        <FlatList
            style={{alignSelf:'center',width:'100%'}}
            horizontal={true}
            data={experienceList.length > 0 ? experienceList : SHIMMER}
            showsHorizontalScrollIndicator={false}
            renderItem={(item,index) => 
                experienceList.length > 0 ? 
                _renderExperiences(item,index) : _renderShimmer(item,index)}
            keyExtractor={item => item.id}
        />

        </View>

        {/** experiences end */}





        {/** destinations */}
    <View style={{marginTop:0}}/>
    <View style={styles.listContainer}>
        <View style={styles.rowContainer}>
            <Text style={styles.listLabel}>Destinations</Text>
            <TouchableOpacity style={styles.rowContainer} onPress={()=>{navigateToCountries()} }>
                <Text style={styles.viewLabel}>View all</Text>
                <FastImage 
                source={require('../assets/forward.png')}
                style={styles.nextImage} 
                resizeMode={FastImage.resizeMode.cover}/>
                </TouchableOpacity>
        </View>


        <FlatList
            style={{marginVertical:10,alignSelf:'flex-start'}}
            horizontal={true}
            data={countryList.length > 0 ? countryList : SHIMMER}
            showsHorizontalScrollIndicator={false}
            renderItem={(item,index) => 
                countryList.length > 0 ? 
                _renderCountries(item,index) : _renderShimmer(item,index)}
            keyExtractor={item => item.id}
        />

        </View>

        {/** Destinations end */}


        <View style={{marginTop:-20}}/>
        {/** Green Brands */}
    <View style={styles.listContainer}>
        <View style={styles.rowContainer}>
            <Text style={styles.listLabel}>Green Brands</Text>
            <TouchableOpacity style={styles.rowContainer} onPress={()=>{navigateToBrands()} }>
                <Text style={styles.viewLabel}>View all</Text>
                <FastImage 
                source={require('../assets/forward.png')}
                style={styles.nextImage} 
                resizeMode={FastImage.resizeMode.cover}/>
                </TouchableOpacity>
        </View>


        <FlatList
            style={{marginVertical:10,alignSelf:'center'}}
            horizontal={true}
            data={brandList.length > 0 ? brandList : SHIMMER}
            showsHorizontalScrollIndicator={false}
            renderItem={(item,index) => 
                brandList.length > 0 ? 
                _renderBrands(item,index) : _renderShimmer(item,index)}
            
            keyExtractor={item => item.id}
        />

        </View>

        {/** Green Brands end */}




        
        <View style={{marginTop:-40}}/>

        {/** Blogs */}
    <View style={styles.listContainer}>
        <View style={styles.rowContainer}>
            <Text style={styles.listLabel}>Blogs</Text>
            <TouchableOpacity style={styles.rowContainer} onPress={()=>{navigateToBlogs()} }>
                <Text style={styles.viewLabel}>View all</Text>
                <FastImage 
                source={require('../assets/forward.png')}
                style={styles.nextImage} 
                resizeMode={FastImage.resizeMode.cover}/>
                </TouchableOpacity>
        </View>


        <FlatList
            style={{marginVertical:10,alignSelf:'center',width:'100%'}}
            horizontal={true}
            data={blogList.length > 0 ? blogList : SHIMMER}
            showsHorizontalScrollIndicator={false}
            renderItem={(item,index) => 
                blogList.length > 0 ? 
                _renderBlogs(item,index) : _renderShimmer(item,index)}
            keyExtractor={item => item.id}
        />

        </View>

        {/** Blogs end */}

            </>
        );
    }
    return (
       <>
         
        <ParallaxScrollView
           bounces={false}
          backgroundColor="black"
          contentBackgroundColor="black"
          //backgroundSpeed={100}
          parallaxHeaderHeight={dimensions.SCREEN_HEIGHT * 0.55}
          stickyHeaderHeight={60}
          renderStickyHeader = {() => (
                <>
                    <ParallaxHeader haveSearch={true} {...props}/>
                </>
           )}
          renderForeground={() => (
           <HeaderTop  {...props}/>
          )}
          >

              <Body/>
        
                        
        </ParallaxScrollView>
         {apiReducer.loading && <ProgressBar/>}
         </>
  
      );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        flexGrow:1
    },
    heading:{
        color:colors.WHITE,
        lineHeight:26,
        fontSize:14,
        fontWeight:'800',
        alignSelf:'center',
        maxWidth:'90%',
        margin:4,
        alignSelf:'flex-start',
        marginHorizontal:10,
        textTransform:'capitalize'
    },
    headingSub:{
        color:colors.COLOR_PRIMARY,
        fontWeight:'400',
        fontSize:13,
        width:'70%',
        textAlign:'center'
    },
    rowContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    labelText:{
        color:colors.COLOR_PRIMARY,
        fontSize:13
    },
    bannerImage:{
         width: dimensions.SCREEN_WIDTH * 0.99, 
        height: undefined,
        alignSelf:'center',borderRadius:0,marginTop:0,backgroundColor:'#808080' ,
        aspectRatio:3/2
    },
   listContainer:{
       width:'100%',
       padding:10,
       marginVertical:1,
   },
   listLabel:{
       color:colors.COLOR_PRIMARY,
       textTransform:'uppercase',
       fontWeight:'500',
       fontSize:16
    },
    nextImage:{
        height:20,width:20,marginHorizontal:6
    },
    viewLabel:{
        color:colors.COLOR_PRIMARY,
        textTransform:'uppercase',
        fontWeight:'400',
        fontSize:12
     },

     shimmerStyle:{
         height:160,
         width:dimensions.SCREEN_WIDTH * 0.42,
         margin:10,backgroundColor: '#ebebeb',overflow: 'hidden'
        }
    ,shimmerStyleText:{
        height:20,
       width:dimensions.SCREEN_WIDTH * 0.38,
       margin:7,
       alignSelf:'center',
       borderRadius:20,
       backgroundColor: '#ebebeb',overflow: 'hidden'
    },
    buttonContainer:{
        alignSelf:'center',
        width:dimensions.SCREEN_WIDTH * 0.7,height:50,
        borderColor:colors.COLOR_PRIMARY,borderRadius:25,borderWidth:3,flexDirection:'row',
        justifyContent:'space-between',alignItems:'center',margin:15,paddingHorizontal:10
    },
    headerTop:{height:dimensions.SCREEN_HEIGHT * 0.55,width:dimensions.SCREEN_WIDTH},
    imageBackground:{
        justifyContent:'space-between',height:dimensions.SCREEN_HEIGHT * 0.50,
         width:dimensions.SCREEN_WIDTH,padding:15,paddingHorizontal:20
        }

})


export default HomeData;


