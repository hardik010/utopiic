import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';

import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { postRequestNoBodyApi, postRequestNoBodyApiWithToken } from '../redux/actions/api_actions';
import config from '../config/config';
import HomeHeader from '../components/home_header';
import Carousel from 'react-native-banner-carousel';




const CountryDetails: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const apiReducer = useSelector(state => state.apiReducer)
    const dispatch = useDispatch()

    const [data, setData] = useState(''); 
    const [images, setImages] = useState([]); 
    const [temperature, setTemperature] = useState(0.0); 
    const [brands, setBrands] = useState([]); 
    const [moreVisibility, setMoreVisibility] = useState(false); 

     
    function getWeather(country) {
     const openweather_api = "http://api.openweathermap.org/data/2.5/weather?q="+country+"&appid=f6c9f143d6581381662978157abcb2aa&units=imperial"
     fetch(openweather_api)
     .then(res => res.json())
     .then((data) => {
       //console.log('data', JSON.stringify(data))
       if(data.cod == 200){
         if(data.main){
           var temperature = data.main.temp // in farenheight
           setTemperature(temperature)
         }
       }
      
     })
   
 }

    function fetchCountryDetails(){
        const URL = config.BASE_URL + config.COUNTRY_DETAILS + props.route.params.id
        dispatch(postRequestNoBodyApiWithToken(URL,userReducer.user.token))
        .then(response=> {
            //console.log('Response - ',JSON.stringify(response))
            setData(response.data)
            setImages(response.data.images)
            getWeather(response.data.name)
        })
        .catch(error => {
           showMessage(error.message)
            //console.log('Error - ',error)
        })
    }

    useEffect(() =>{
        fetchCountryDetails()
    },[])

   
    function renderPage(item,index){
        return (
            <View key={index}>
                 <FastImage
                style={{ width: dimensions.SCREEN_WIDTH, height: '100%' }}
                source={{ uri: item,
                    priority: FastImage.priority.normal,
                }}
            resizeMode={FastImage.resizeMode.cover}/>
            </View>
        );
    }

   function _getAbout(){
        if(moreVisibility){
          return(
            <Text  style={styles.aboutText}>
                {data.facts.about+ '  ' }
                <View style={styles.divider}/>
             <TouchableOpacity onPress={()=> setMoreVisibility(!moreVisibility)}
                 style={styles.readContainer}>
                    <Text style={styles.readText}>Done </Text>
                </TouchableOpacity>
            </Text>
          )
        }
        else{
          return(
            <View>
            <Text  style={styles.aboutText}>
                {data.facts.about.substring(0,200) + '  ' }
            </Text>
            <View style={styles.divider}/>
             <TouchableOpacity onPress={()=> setMoreVisibility(!moreVisibility)}
                 style={styles.readContainer}>
                    <Text style={styles.readText}>Read More </Text>
                </TouchableOpacity>
            </View>
          )
        }
      }

      function navigateToDetails(item){
        props.navigation.navigate('attraction_details',{'id':item.id})
    }

  
 function _renderAttractions({item, index}) {
        ////console.log(index % 2 == 0 ? 'true':'false')
          if(index % 2 == 0){
            return (
              <TouchableOpacity  onPress={()=>navigateToDetails(item)}
              style={{flexDirection:'row',marginVertical:12,height:180}}>
                 <FastImage source={{uri:item.images}}
                 resizeMode={FastImage.resizeMode.cover}
                 style={styles.attractionsImage}/>
        
        
                  <View style={{flex:7,justifyContent:'space-evenly',marginHorizontal:10}}>
                  <Text style={{color:'white',marginBottom:10}}>{item.name}</Text>
                  <Text style={{color:'grey',fontSize:11}}>{item.description.substring(0,300)} ...</Text>
                  <View style={styles.readContainer}>
                    <Text style={styles.readText}>Read More </Text>
                  </View>
                 </View>
              </TouchableOpacity>
          );
          }
          else{
            return(
              <TouchableOpacity  onPress={()=>navigateToDetails(item)}
              style={{flexDirection:'row',marginVertical:12,height:180}}>
                
        
                  <View style={{flex:7,justifyContent:'space-evenly',marginHorizontal:10}}>
                  <Text style={{color:'white',marginBottom:10}}>{item.name}</Text>
                  <Text style={{color:'grey',fontSize:11}}>{item.description.substring(0,300)} ...</Text>
                  <View style={styles.readContainer}>
                    <Text style={styles.readText}>Read More </Text>
                  </View>
                 </View>
        
                 <FastImage source={{uri:item.images}}
                 resizeMode={FastImage.resizeMode.cover}
                 style={styles.attractionsImage}/>
        
              </TouchableOpacity>
            )
          }
         
        }
   
 return (
    <>
      <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
      <HomeHeader haveBack {...props}/>
      <ScrollView 
        bounces={false}
        useAnimatedScrollView={true}
        contentContainerStyle={styles.scrollContainer}>
        {/** image  */}
        <View style={styles.imageContainer}>
             {
              images && images.length >0 ?
              <Carousel
              style={{alignSelf:'center'}}
              autoplay  autoplayTimeout={4000}
              in
              loop index={0} pageSize={dimensions.SCREEN_WIDTH}>
               {images.map((image, index) => renderPage(image, index))}
               </Carousel>  
               :null

            }
            <View style={styles.nameContainer}>
                <View>
                <Text style={styles.nameText}>{data.name} </Text> 
                </View>
            </View>
        </View>
        {/** image end */}

        <View style={{margin:12}}>

        {
            data.facts && data.facts.about.length > 200 ?
             _getAbout()
           :
            <Text style={styles.aboutText}>{data.facts ? data.facts.about : ''}
            </Text>
         }
          </View>


         {/** facts  */}
         <Text style={styles.heading}>Facts</Text>

         <View style={[styles.rowContainer,{marginVertical:5}]}>
              <FastImage 
              source={require('../assets/currency_yellow.png')}
              style={styles.factsImage} 
              resizeMode={FastImage.resizeMode.cover}/>
              <Text style={styles.factData}>{(data.facts && data.facts.currency)
              ? data.facts.currency : ''}</Text>
          </View>


          <View style={[styles.rowContainer,{marginVertical:5}]}>
              <FastImage 
              source={require('../assets/weather_yellow.png')}
              style={styles.factsImage} 
              resizeMode={FastImage.resizeMode.cover}/>
              <Text style={styles.factData}>{temperature} C</Text>
          </View>


          <View style={[styles.rowContainer,{marginVertical:5}]}>
              <FastImage 
              source={require('../assets/bulb.png')}
              style={styles.factsImage} 
              resizeMode={FastImage.resizeMode.cover}/>
              <Text style={[styles.factData]}>{(data.facts && data.facts.electic_plug)
              ? data.facts.electic_plug : ''}</Text>
          </View>


          <View style={[styles.rowContainer,{marginVertical:5}]}>
              <FastImage 
              source={require('../assets/language.png')}
              style={styles.factsImage} 
              resizeMode={FastImage.resizeMode.cover}/>
              <Text style={styles.factData}>{(data.facts && data.facts.languages)
              ? data.facts.languages : ''}</Text>
          </View>


            {/** attractions */}
                {
                   data.main_attractions &&  data.main_attractions.length > 0 ?
                    <View>
                            <Text style={styles.heading}>Attractions</Text>
                            <FlatList
                            style={{marginVertical:10}}
                            horizontal={false}
                            data={data.main_attractions}
                            showsVerticalScrollIndicator={false}
                            renderItem={(item,index) => _renderAttractions(item,index)}
                            keyExtractor={item => item.id}
                            /> 
                    </View>
                    : null
                }


      </ScrollView>
      {apiReducer.loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        flexGrow:1
    },
   
    rowContainer:{
        flexDirection:'row',
        alignItems:'center',
    },
    imageContainer:{
        width:dimensions.SCREEN_WIDTH,
        height:dimensions.SCREEN_HEIGHT * 0.35,
        backgroundColor:colors.COLOR_PRIMARY
    },
    nameContainer:{
        width:dimensions.SCREEN_WIDTH,
        padding:20,
        justifyContent:'center',
        alignItems:'center',
        position:'absolute',
        top:0,
        left:0,
        right:0,
        backgroundColor:'rgba(0,0,0,0.0001)'
    },
    nameText:{
        color:colors.WHITE,
        fontSize:26,fontWeight:'bold',
        textTransform:'uppercase'
    },
    heading:{
        color:colors.COLOR_PRIMARY,
        margin:10,
        fontSize:20,
        textTransform:'uppercase',
        fontWeight:'700',
        alignSelf:'center'
    },
    factData:{
        color:colors.COLOR_PRIMARY,
        marginLeft:10,
        fontSize:13,textAlign:'left',
        width:'80%'
    },
    factsImage:{
        height:50,
        width:50,
        marginHorizontal:10
    },
    attractionsContainer:{
        width:'100%',
        height:190,
        borderRadius:10,
        backgroundColor:'grey',overflow:'hidden'
      },
      attractionsImage:{
        flex:4,
        height:'100%',
        marginHorizontal:10
      },
      readContainer:{
        backgroundColor:colors.COLOR_PRIMARY,
        paddingHorizontal:7,
        paddingVertical:6,
        alignItems:'center',
        justifyContent:'center',
        width:dimensions.SCREEN_WIDTH*0.18,
        marginTop:10
      },
      readText:{
        fontSize:11
      },
      aboutText:{
          color:colors.WHITE,
          fontSize:14
     },
     divider:{
         backgroundColor:colors.COLOR_PRIMARY,
         height:1,
         width:dimensions.SCREEN_WIDTH,
         marginVertical:6
     }
   

})


export default CountryDetails;


