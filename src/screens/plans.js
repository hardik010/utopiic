import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';

import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList } from 'react-native-gesture-handler';
import config from '../config/config';
import { postRequestNoBodyApi } from '../redux/actions/api_actions';





const Plans: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false); 
    const [plansList, setPlansList] = useState([]);
    const [selectedIndex, setSelectedIndex] = useState(null);


  function fetchPlans(){
    const URL = config.BASE_URL + config.USER_PLAN
    dispatch(postRequestNoBodyApi(URL))
    .then(response=> {
       // console.log('Response - ',JSON.stringify(response))
        if(response.status) {
            setPlansList(response.data)
        }
        else {
          showMessage(response.message)
        }
    })
    .catch(error => {
        showMessage(error.message)
        //console.log('Error - ',error)
    })
}

    useEffect(() =>{
        //console.log('------>>>',props.route.params.profile)
        fetchPlans()
    },[])



   function  onPress  (index) {
       setSelectedIndex(index)
   }

   function isValid(){
     //console.log(selectedIndex)
     if(selectedIndex == null) showMessage('Select Plan First')
     else {
        let profile = props.route.params.profile
         profile.plan = plansList[selectedIndex]
         props.navigation.navigate('payment_method',{'profile':profile})
     }
   }

    function renderItem (item,index)  {

        return(
           <View style={styles.planContainer}>
                <View style={styles.imageContainer}>
                  <FastImage 
                    source={{uri:item.icon}}
                    style={{height:60,width:46}} 
                    resizeMode={FastImage.resizeMode.contain}/>
                </View>

                <View style={styles.textContainer}>
                  <Text style={styles.titleText}>{item.plan_name}</Text>
                 <Text style={styles.priceText}>₹ {item.plan_price}/{item.plan_validity}</Text>
                  <View style={{height:20}}/>
                  <Text style={styles.priceText}>{item.description}</Text>
                </View>

              <View style={{flex:1}}>
               <RadioButton
                size={12}
                innerColor={colors.COLOR_PRIMARY}
                outerColor={colors.COLOR_PRIMARY}
                animation={'bounceIn'}
                isSelected={selectedIndex == index}
                onPress={() => {onPress(index)}}
                />
              </View>
           </View>
        )
      }

    function _onOkay(){
            props.navigation.navigate('signup')
            //props.navigation.navigate('account')
    }

    const itemSeparator = () => {
      return (
        <View style={styles.seperator} />
      );
    }

 return (
    <>
      <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
       <BackHeader {...props}/>
        <InputScrollView 
        bounces={false}
        useAnimatedScrollView={true}
        contentContainerStyle={styles.scrollContainer}>

        <View style={{height:40}}/>
          <Text style={styles.heading}>Choos the plan thag will jumpstart your substainable lifestyle and guilt-free travel & explorations.</Text>
        <View style={{height:50}}/>       

        <View style={styles.seperator} />
               <FlatList 
                contentContainerStyle={{alignItems:'center'}}
                showsVerticalScrollIndicator={false}
                scrollEnabled={false}
                ItemSeparatorComponent={itemSeparator}
                data={plansList}
                renderItem={({ item ,index}) => renderItem(item,index)}
                keyExtractor={(item) => item.plan_id } />         
         <View style={styles.seperator} />

          <CustomButton 
          style={{width :dimensions.SCREEN_WIDTH * 0.8,
            marginTop:55,marginBottom:19,alignSelf:'center'}}
          handler={isValid}
          label ={'Okay'}/>


      </InputScrollView>

      {loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,

      },
      heading:{
        color:colors.COLOR_PRIMARY,
        lineHeight:30,
        fontSize:15,
        fontWeight:'300',
        alignSelf:'center',
        textAlign:'center',
        maxWidth:'70%'
    },
    planContainer:{
      flexDirection:'row',
      alignItems:'center',
      width:dimensions.SCREEN_WIDTH,
      padding:10
    },
    imageContainer:{
      flex:1.6,
    },
    textContainer:{
      flex:6,
    },
    titleText:{
      color:colors.COLOR_PRIMARY,
      fontWeight:'600'
    },
    priceText:{
      color:colors.COLOR_PRIMARY,
      fontWeight:'400',
      fontSize:12
    },
    seperator:{
      height: .5,
      width: dimensions.SCREEN_WIDTH,
      backgroundColor: colors.COLOR_PRIMARY,
    }
})


export default Plans;


