import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';

import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { postRequestNoBodyApi } from '../redux/actions/api_actions';
import config from '../config/config';
import ProgressWebView from "react-native-progress-webview";

import WebView from 'react-native-webview';



const PrivacyPolicy: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const apiReducer = useSelector(state => state.apiReducer)
    const [data, setData] = useState(''); 

    const dispatch = useDispatch()

    function fetchPrivacy(){
        const URL = config.BASE_URL + config.PRIVACY_POLICY
        
        dispatch(postRequestNoBodyApi(URL))
        .then(responseJson=> {
            //console.log('Response - ',responseJson)
            if(responseJson.status){
                setData(responseJson.data.description)
              }
        })
        .catch(error => {
            showMessage(error.message)
            //console.log('Error - ',error)
        })
    }

    useEffect(() =>{
        fetchPrivacy()
    },[])

   
   
 return (
    <>
     <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
      <BackHeader {...props}/>

        <WebView  originWhitelist={['*']}
        source={{ html: data}}/>

      {apiReducer.loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        justifyContent:'center',
        flexGrow:1,
        backgroundColor:'white',
    },
   
    rowContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
   

})


export default PrivacyPolicy;


