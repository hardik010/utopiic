import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions, fonts} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';

import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { postRequestNoBodyApi } from '../redux/actions/api_actions';
import config from '../config/config';
import SearchInput from '../components/search_input';
import HomeHeader from '../components/home_header';

const Card = (props) =>{
    return(
        <View style={styles.cardContainer}>
            <ImageBackground style={styles.cardBackground}
            source ={{uri:props.image}}>
                <View style={styles.nameContainer}>
                    <Text style={styles.cardTitle}>{props.name}</Text>
                    {/* <Text style={{color:'white'}}>{props.subName}</Text> */}
                </View>
            </ImageBackground>
        </View>
    )
}


const Countries: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const apiReducer = useSelector(state => state.apiReducer)

    const dispatch = useDispatch()
   
    const [countriesGlobal, setCountriesGlobal] = useState([]); 
    const [countries, setCountries] = useState(false); 

    function fetchCountries(){
        const URL = config.BASE_URL + config.COUNTRIES
        
        dispatch(postRequestNoBodyApi(URL))
        .then(response=> {
            console.log('Response - ',JSON.stringify(response))
            if(response.status) {
                setCountries(response.data.list)
                setCountriesGlobal(response.data.list)
            }
        })
        .catch(error => {
            showMessage(error.message)
            //console.log('Error - ',error)
        })
    }

    useEffect(() =>{
        fetchCountries()
    },[])


    function navigateToDetails(id){
        props.navigation.navigate('country_details',{'id':id})
    }

    function renderItem (item,index)  {
        return(
            <TouchableOpacity onPress={()=> navigateToDetails(item.id)}>
                <Card image = {item.image}
                name={item.name} subName ={''}/>
            </TouchableOpacity>


        )
      }

      const searchInput = useRef();
      const searchRef = useRef();
  
    function _onSearch (){
        var searchText = searchInput.current.getInputValue()
        //console.log(searchText)

        var results = [];    
        var name = ''
        for(var i = 0; i < countriesGlobal.length  ; i++) {
             name = countriesGlobal[i].name.toLowerCase()
            if(name  != null ){
                 var check = name.indexOf(searchText.toLowerCase()) > -1
                  if(check) {
                     results.push(countriesGlobal[i]);
                  }
            }
        }
    
        if(results.length == 0){
         showMessage('No Destinations Found')
        }
        setCountries(results)
        
      }

   
   
 return (
    <>
     <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
      <HomeHeader haveBack {...props}/>
      <ScrollView 
        bounces={false}
        useAnimatedScrollView={true}
        contentContainerStyle={styles.scrollContainer}>
        <View style={{height:10}}/>
        <Text style={styles.heading}>COUNTRIES</Text>    
        <SearchInput
         placeholder={'Search Destinations..'}
         onSubmitEditing={()=> _onSearch()}
         ref={searchInput}
         inputRef={ searchRef }
         style={{width :'60%',alignSelf:'center'}}/>
        <View style={{height:20}}/>

            <FlatList 
                contentContainerStyle={{alignItems:'center'}}
                showsVerticalScrollIndicator={false}
                scrollEnabled={false}
                data={countries}
                renderItem={({ item ,index}) => renderItem(item,index)}
                keyExtractor={(item) => item.id } />   

      </ScrollView>
      {apiReducer.loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        justifyContent:'center',
        flexGrow:1
    },
   
    rowContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    heading:{
        color:colors.COLOR_PRIMARY,
        lineHeight:26,
        fontSize:20,
        fontWeight:'700',
        alignSelf:'center',
        maxWidth:'90%',
        marginVertical:12,
        fontFamily:fonts.heading_font
    },
   
    cardContainer:{
        width:dimensions.SCREEN_WIDTH,
        height:dimensions.SCREEN_HEIGHT * 0.31,
        marginVertical:7,
        backgroundColor:colors.COLOR_PRIMARY
     },
     nameContainer:{
         marginBottom:20,
         alignItems:'center',
         padding:10,
         backgroundColor:'rgba(0,0,0,0.3)',
        width:'100%'
    },
     cardBackground:{
        height:'100%',width:'100%',
        justifyContent:'flex-end',alignItems:'center'
     },
    cardTitle:{color:'white',fontSize:17,fontWeight:'800',textTransform:'uppercase',lineHeight:30},
    cardSubTitle:{color:'white',lineHeight:28},
   
    

})


export default Countries;


