import messaging from '@react-native-firebase/messaging';
import { showMessage } from './snackmsg';

/**
 * Service file to get device token and firebase permission
 */

export const  getDeviceToken = async () => {
  const authStatus = await messaging().requestPermission({
    sound: false,
    announcement: true,
    // ... other permission settings
  });
  const enabled =
    authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
    authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (authStatus === messaging.AuthorizationStatus.AUTHORIZED) {
        //console.log('User has notification permissions enabled.');
      } else if (authStatus === messaging.AuthorizationStatus.PROVISIONAL) {
        //console.log('User has provisional notification permissions.');
      } else {
        //console.log('User has notification permissions disabled');
      }

  if (enabled) {
   // //console.log('Authorization status:', authStatus);
    return await getToken()
  }
  else {
    showMessage("Firebase Permission Denied.")
    // //console.log("Firebase Permission Denied.");
    return null
  }
}

export const  getToken = async () => {
     // Get the device token
     let token = await messaging().getToken()
     if(token) return token
     else return null
     
   // If using other push notification providers (ie Amazon SNS, etc)
   // you may need to get the APNs token instead for iOS:
   // if(Platform.OS == 'ios') { messaging().getAPNSToken().then(token => { return saveTokenToDatabase(token); }); }

   // Listen to whether the token changes
   return messaging().onTokenRefresh(token => {
    return token
   });
  }




  //other methods
  const  checkNotificationPermission = () => {
    
    messaging()
    .hasPermission()
    .then(enabled => {
      if (!enabled) {
        promptForNotificationPermission();
      }
    });
};

const  promptForNotificationPermission = () => {
  messaging()
    .requestPermission({provisional: true})
    .then(() => {
      ////console.log('Permission granted.');
    })
    .catch(() => {
      //console.log('Permission rejected.');
    });
};



