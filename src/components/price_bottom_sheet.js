import React, { useImperativeHandle,useEffect,useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import {colors,urls,dimensions} from '../utils/constants';
import RBSheet from "react-native-raw-bottom-sheet";
import RadioButton from 'react-native-radio-button'
import FastImage from 'react-native-fast-image';
import { showMessage } from '../utils/snackmsg';



 const PriceBottomSheet = React.forwardRef((props,ref) =>   {

    const [selectedIndex, setSelectedIndex] = useState(null);
    const [item, setItem] = useState(null);


 

   
    useImperativeHandle(ref, () => ({
        getSelectedIndex() {
            return selectedIndex
        },
     }));


     const itemSeparator = () => {
        return (
          <View style={styles.seperator} />
        );
      }
      
      function  onPress  (index) {
        setSelectedIndex(index)
      }


      function renderPrice(element,index)  {
        return(
           <View style={[styles.rowContainer,{width:dimensions.SCREEN_WIDTH * 0.9
           ,margin:10}]}>
               <View>
               <Text style={{color:colors.COLOR_PRIMARY,fontSize:16,textTransform:'capitalize'}}>
                   {element.description}
               </Text>
               <Text style={{color:colors.GREY,fontSize:14}}>
                  Price : INR {element.price}
               </Text>
               </View>
             
               <RadioButton
                size={12}
                innerColor={colors.COLOR_PRIMARY}
                outerColor={colors.COLOR_PRIMARY}
                animation={'bounceIn'}
                isSelected={selectedIndex == index}
                onPress={() => {onPress(index)}}
                />
            
           </View>
        )
           }
      

           function onDone(){
            
             if(selectedIndex == null) showMessage("Select Price First")
             else props.onDone(selectedIndex)
             setSelectedIndex(null)
           }


           function onClose(){
            
            props.onClose()
            setSelectedIndex(null)
          }

  
  return(
    <RBSheet
            ref={props.inputRef}
            //closeOnPressMask={false}
            closeOnSwipeDown={false}
            closeOnPressBack={false}
            dragFromTopOnly={true}
            closeOnDragDown={true}
            height={dimensions.SCREEN_HEIGHT * 0.7}
            customStyles={{
              wrapper: {
                backgroundColor: "rgba(0,0,0,0.8)"
              },
              draggableIcon: {
                backgroundColor: 'black'
              },
              container:{
                backgroundColor:'black',
                borderTopColor:colors.COLOR_PRIMARY,
                borderWidth:1,
                paddingVertical:10,
                paddingHorizontal:12
              }
            }}
          >
      <ScrollView>
              <View style={styles.rowContainer}>
                      <Text style={{color:colors.COLOR_PRIMARY,lineHeight:40,
                        fontWeight:'bold',fontSize:19}}>
                          Select price : 
                    </Text>

                    <TouchableOpacity onPress={onClose}>
                    <FastImage source ={require('../assets/close.png')}
                      style={{height:25,width:25}}
                      resizeMode={'contain'}
                      />

                    </TouchableOpacity>
              </View>
              <View style={{height:30}}/>
           
               <FlatList 
                showsVerticalScrollIndicator={false}
                scrollEnabled={false}
                 //ItemSeparatorComponent={itemSeparator}
                data={props.item ? props.item.request_option:[]}
                renderItem={({ item ,index}) => renderPrice(item,index)}
                keyExtractor={(item) => item.id } />   


                <View style={{height:30}}/>

                        <TouchableOpacity onPress={onDone}
                        style={{
                          borderRadius:20,
                          borderWidth:0.7,
                          borderColor:colors.COLOR_PRIMARY,
                          justifyContent:'center',
                          alignItems:'center',
                         paddingHorizontal:17,
                         paddingVertical:9,
                         width:170
                        }}>

                          <View style={[styles.rowContainer,{marginBottom:0}]}>
                              <Text style={{color:colors.COLOR_PRIMARY,fontSize:19}}> Pay Now    </Text>

                              <TouchableOpacity >
                              <FastImage source ={require('../assets/next.png')}
                                style={{height:25,width:25}}
                                resizeMode={'contain'}
                                />
                                </TouchableOpacity>
                          </View>

                        </TouchableOpacity>
                <View style={{height:30}}/>
         </ScrollView> 
       </RBSheet>
  );
});


PriceBottomSheet.defaultProps = {

  handler: () => {},
  label: 'Enter',
};


export default PriceBottomSheet

const styles = StyleSheet.create({

  container:{
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:colors.COLOR_PRIMARY,
    paddingHorizontal:15,
    paddingVertical:10,
    borderRadius:4,
    marginTop:20,
    marginBottom:10,
    borderWidth:0.7,
    borderColor:colors.COLOR_PRIMARY
  },
  seperator:{
    height: .6,
    width: dimensions.SCREEN_WIDTH,
    backgroundColor: 'grey',
  },
  rowContainer:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between',
    marginBottom:15
},
})
