import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';


import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import Carousel from 'react-native-snap-carousel';
import { addUser } from '../redux/actions/user_actions';
import config from '../config/config';
import { postRequestApiWithToken, postRequestNoBodyApiWithToken } from '../redux/actions/api_actions';


const Interest: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const apiReducer = useSelector(state => state.apiReducer)
    const [list, setList] = useState([]); 
    const [baseUrl, setBaseUrl] = useState(''); 
    const dispatch = useDispatch()


   function fetchPreferences(){
      const URL = config.BASE_URL + config.PREFERENCES
        dispatch(postRequestNoBodyApiWithToken(URL,props.route.params.profile.token ))
        .then(response=> {
            //console.log('Response - ',JSON.stringify(response))
            if(response.status) {
              setList(response.data[0].list)
              setBaseUrl(response.data[0].image_path)
            }
        })
        .catch(error => {
            showMessage(error.message)
            //console.log('Error - ',error)
        })
    }
    useEffect(()=>{
      //console.log('------>>>',props.route.params.profile)
      fetchPreferences()
    },[])


    function _onOkay(){
         props.navigation.navigate('welcome_pager')
    }

    function  _renderItem ({item, index}) {
       if(item.length > 0  && baseUrl.length > 0){
        return (
          <View style={{height:dimensions.SCREEN_HEIGHT * 0.25}}>
             <FastImage 
                  source={{uri: baseUrl + item.image}}
                  style={{height:'100%',width:'100%'}} 
                  resizeMode={FastImage.resizeMode.cover}/>
              <View style={styles.bottom}>
                   <Text style={{color:colors.COLOR_PRIMARY,textTransform:'uppercase'}}>{item.name}</Text>
              </View>
          </View>
      );
       }
    }
   
 return (
    <>
      <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
      <ScrollView 
        bounces={false}
        useAnimatedScrollView={true}
        contentContainerStyle={styles.scrollContainer}>
        <View style={{height:80}}/> 
          <Text style={styles.heading}>INTERESTS</Text>
          <View style={{height:20}}/> 
          <Text style={styles.labelText}>Swipe left or right and let us know what you want</Text>
       
          <View style={{height:50}}/> 
           <Carousel
             // ref={(c) => { this._carousel = c; }}
              data={list}
              layout={'tinder'} layoutCardOffset={'13'}
              renderItem={_renderItem}
              sliderWidth={dimensions.SCREEN_WIDTH}
              itemWidth={dimensions.SCREEN_WIDTH}
            />

           <CustomButton 
            style={{width :dimensions.SCREEN_WIDTH * 0.8,
            marginTop:65,marginBottom:19,alignSelf:'center'}}
            handler={_onOkay}
            label ={'done'}/>

       

      </ScrollView>

      {apiReducer.loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        
    },
    heading:{
        color:colors.COLOR_PRIMARY,
        lineHeight:30,
        fontSize:17,
        fontWeight:'500',
        alignSelf:'center',
        textAlign:'center',
        maxWidth:'70%'
    },
    headingSub:{
        color:colors.COLOR_PRIMARY,
        lineHeight:40,
        fontWeight:'600',
        fontSize:15
    },
    rowContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    labelText:{
        color:colors.COLOR_PRIMARY,
        fontSize:13
    },
  bottom:{
      position:'absolute',
      bottom:0,
      left:0,
      right:0,
      justifyContent:'center',
      alignItems:'center',
      backgroundColor:'rgba(0,0,0,0.9)',
      paddingVertical:10
  }
})


export default Interest;


