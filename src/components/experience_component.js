import React, { useState ,forwardRef, useRef, useImperativeHandle } from 'react'
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import FastImage from 'react-native-fast-image'


import {colors,urls,dimensions} from '../utils/constants';



const ExperienceComponent  = (props) => {


    
      return (
       <TouchableOpacity style={styles.container}>
        <FastImage 
            source={{uri: props.object.cover_image}}
            style={styles.experienceImage} 
            resizeMode={FastImage.resizeMode.cover}/>

         <View style={{height:10}}/>
         
         <Text style={styles.nameText}>{props.object.name.substring(0,20)}</Text>
         {/* <Text style={styles.infoText}>{props.object.name}</Text>
         <Text style={styles.infoText}>{props.object.summary}</Text>
     */}
       </TouchableOpacity>
      )
};

ExperienceComponent.defaultProps = {
};


const styles = StyleSheet.create({

    container:{
      alignItems:'center',
      backgroundColor:colors.BLACK,
      width:dimensions.SCREEN_WIDTH * 0.42,
      margin:10
     
    },
    experienceImage:{
        height:160,width:'100%',backgroundColor:colors.DARK_GREY
    },
    nameText:{
        lineHeight:28,
        fontWeight:'600',
        fontSize:15,
        color:'white',
        textAlign:'center',
        textTransform:'capitalize'
    },
    infoText:{
        lineHeight:28,
        fontSize:13,
        color:'white',textAlign:'center'
    }
  })

export default ExperienceComponent;
