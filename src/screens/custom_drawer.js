import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TouchableOpacity
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';


import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList } from 'react-native-gesture-handler';
import { removeUser } from '../redux/actions/user_actions';
import Modal from 'react-native-modal';
import { postRequestNoBodyApiWithToken } from '../redux/actions/api_actions';
import config from '../config/config';




const CustomDrawer: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false); 
    const [logoutModalVisibility, setLogoutModalVisibility] = useState(false);
    const [notificationModalVisibility, setNotificationModalVisibility] = useState(false); 


      // const showModal = () => setVisible(true);
      // const hideModal = () => setVisible(false);
      // const containerStyle = {backgroundColor: 'white', padding: 20};

    useEffect(() =>{
    },[])

    function logout(){
      props.navigation.toggleDrawer()
      setLogoutModalVisibility(true)
    }
    function notification(){
      props.navigation.toggleDrawer()
      setNotificationModalVisibility(true)
    }

   

    function _logout(){
      setLogoutModalVisibility(false)
      const URL = config.BASE_URL+ config.LOGOUT
      dispatch(postRequestNoBodyApiWithToken(URL,userReducer.user.token))
      .then(response=> {
          console.log('Response - ',JSON.stringify(response))
          if(!response.status){
           // showMessage(response.message),false
            dispatch(removeUser({}))
          }
          else showMessage(response.message)
      })
      .catch(error => {
           showMessage(error.message)
           //console.log('Error - ',error)
      })
  }

   
 return (
    <>
      <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
      <ScrollView 
        bounces={false}
        useAnimatedScrollView={true}
        contentContainerStyle={styles.scrollContainer}>

        {/* Logout Modal */}

        <Modal 
          onBackdropPress={() => setLogoutModalVisibility(false)}
          onBackButtonPress={() => setLogoutModalVisibility(false)}
          backdropColor={'black'}
          backdropOpacity={0.4}
          animationIn={'zoomInDown'}
          animationOut={'zoomOutUp'}
          animationInTiming={200}
          animationOutTiming={300}
          backdropTransitionInTiming={200}
          backdropTransitionOutTiming={300}
          style={{backgroundColor:'rgba(0,0,0,0.4)',margin:0}}
          isVisible={logoutModalVisibility}>
            <View style={styles.modalContainer}>
                <Text style={styles.modalText}>Don't say goodbye just yet! We are still young and have a plethora of new brands, experiences & updates are still in work.</Text>
                <View style={{height:40}}/>
                <View style={styles.rowContainer}>
                  <TouchableOpacity onPress={() => {_logout()}}
                  style={styles.modalLeftButton}>
                      <Text style={styles.leftButtonText}>Logout</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => setLogoutModalVisibility(false)}
                       style={styles.modalRightButton}>
                        <Text style={styles.rightButtonText}>Cancel</Text>
                        </TouchableOpacity>
                </View>
           </View>
        </Modal>

           {/* Logout Modal  end*/}


              {/* Notification Modal */}

         <Modal 
          onBackdropPress={() => setNotificationModalVisibility(false)}
          onBackButtonPress={() => setNotificationModalVisibility(false)}
          backdropColor={'black'}
          backdropOpacity={0.4}
          animationIn={'zoomInDown'}
          animationOut={'zoomOutUp'}
          animationInTiming={200}
          animationOutTiming={300}
          backdropTransitionInTiming={200}
          backdropTransitionOutTiming={300}
          style={{backgroundColor:'rgba(0,0,0,0.4)',margin:0}}
          isVisible={notificationModalVisibility}>
            <View style={styles.modalContainer}>
                <Text style={styles.modalText}>Show Notifications even when app is inactive ?</Text>
                <View style={{height:40}}/>
                <View style={styles.rowContainer}>
                  <TouchableOpacity onPress={() => setNotificationModalVisibility(false)}
                  style={styles.modalLeftButton}>
                      <Text style={styles.leftButtonText}>Yes</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => setNotificationModalVisibility(false)}
                       style={styles.modalRightButton}>
                        <Text style={styles.rightButtonText}>No</Text>
                        </TouchableOpacity>
                </View>
           </View>
        </Modal>


                 {/* Notification Modal */}

          {/* Header */}
          <View style={styles.headerContainer}>
           <View style={styles.rowContainer}>
            <FastImage 
            source={require('../assets/logo.png')}
              style={{height:40,width:40,margin:10}} 
              resizeMode={FastImage.resizeMode.contain}/>
            <FastImage 
            source={require('../assets/logo-info.png')}
              style={{height:74,width:100}} 
              resizeMode={FastImage.resizeMode.contain}/>
           </View>
          <Text style={{color:colors.COLOR_PRIMARY,marginLeft:20,fontSize:16,fontWeight:'900'}}>{userReducer.user.passcode}</Text>
          </View>
        {/* Header end */}


        <TouchableOpacity style={styles.rowContainerTwo}
        onPress={()=> props.navigation.navigate('account')}>
          <FastImage 
            source={require('../assets/account.png')}
              style={styles.labelImage} 
              resizeMode={FastImage.resizeMode.contain}/>
          <Text style={styles.labelText}>Account</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.rowContainerTwo}
           onPress={()=> props.navigation.navigate('payment_history')}>
          <FastImage 
            source={require('../assets/dollar.png')}
              style={styles.labelImage} 
              resizeMode={FastImage.resizeMode.contain}/>
          <Text style={styles.labelText}>Payment History</Text>
        </TouchableOpacity>


        <TouchableOpacity style={styles.rowContainerTwo}
           onPress={()=> props.navigation.navigate('booking_history')}>
          <FastImage 
            source={require('../assets/booking.png')}
              style={styles.labelImage} 
              resizeMode={FastImage.resizeMode.contain}/>
          <Text style={styles.labelText}>Request History</Text>
        </TouchableOpacity>
{/* 
        <TouchableOpacity style={styles.rowContainerTwo}>
          <FastImage 
            source={require('../assets/card.png')}
              style={styles.labelImage} 
              resizeMode={FastImage.resizeMode.contain}/>
          <Text style={styles.labelText}>Add Card</Text>
        </TouchableOpacity> */}


        <TouchableOpacity onPress={()=> notification()}
         style={styles.rowContainerTwo}>
          <FastImage 
            source={require('../assets/notification.png')}
              style={styles.labelImage} 
              resizeMode={FastImage.resizeMode.contain}/>
          <Text style={styles.labelText}>Notifications</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.rowContainerTwo}
        onPress={()=> props.navigation.navigate('terms')}>
          <FastImage 
            source={require('../assets/terms.png')}
              style={styles.labelImage} 
              resizeMode={FastImage.resizeMode.contain}/>
          <Text style={styles.labelText}>Terms and Conditions</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.rowContainerTwo}
         onPress={()=> props.navigation.navigate('privacy')}>
          <FastImage 
            source={require('../assets/privacy.png')}
              style={styles.labelImage} 
              resizeMode={FastImage.resizeMode.contain}/>
          <Text style={styles.labelText}>Privacy Policy</Text>
        </TouchableOpacity>

        <TouchableOpacity  onPress={()=> logout()}
        style={styles.rowContainerTwo}>
          <FastImage 
            source={require('../assets/logout.png')}
              style={styles.labelImage} 
              resizeMode={FastImage.resizeMode.contain}/>
          <Text style={styles.labelText}>Log Out</Text>
        </TouchableOpacity>

      </ScrollView>

      {loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,  backgroundColor:'black'
      //backgroundColor:'rgba(0,0,0,0.9)',
    },
    scrollContainer:{
        padding:10,
        flexGrow:1,
        borderLeftColor:'white',
        borderLeftWidth:2,
        backgroundColor:'black'
    },
    headerContainer:{alignSelf:'center',alignItems:'center',margin:20}, 
    rowContainer:{
        flexDirection:'row',
        alignItems:'center'
    },
    rowContainerTwo:{
      flexDirection:'row',
      alignItems:'center',
      marginVertical:15
  },
  labelImage:{height:26,width:26,marginHorizontal:10},
  labelText:{color:colors.COLOR_PRIMARY,fontSize:17,fontWeight:'700',marginHorizontal:10},
  modalContainer:{
    width: dimensions.SCREEN_WIDTH * 0.7,
    height:dimensions.SCREEN_HEIGHT * 0.5,
    height: 300,backgroundColor:colors.COLOR_PRIMARY,
    borderRadius:20,
    justifyContent:'center',
    alignSelf:'center',
    alignItems:'center',
    padding:20
    },
    modalText:{
      fontWeight:'bold',
      fontSize:18,
      lineHeight:28,
      marginVertical:15,
      textAlign:'center'
    },
    modalLeftButton:{
      backgroundColor:'black',
      paddingHorizontal:10,
      paddingVertical:8,
      justifyContent:'center',
      alignItems:'center',
      borderWidth:1,
      borderColor:'black',
      width:100,
      borderRadius:12,
      margin:10,
      elevation:10,
      shadowOpacity: 0.3,
      shadowRadius: 3,
      shadowOffset: {
            height: 0,
            width: 0
       },
  },
  modalRightButton:{
    backgroundColor:colors.COLOR_PRIMARY,
    paddingHorizontal:10,
    paddingVertical:8,
    justifyContent:'center',
    alignItems:'center',
    borderWidth:1,
    borderColor:'black',
    borderRadius:12,
    width:100,
    margin:10,
    elevation:10,
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowOffset: {
          height: 0,
          width: 0
     },
    },
    leftButtonText:{color:colors.COLOR_PRIMARY,fontWeight:'bold'},
    rightButtonText:{color:colors.BLACK,fontWeight:'bold'},

})


export default CustomDrawer;


