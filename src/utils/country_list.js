export default {
   
    "countries": [
        {
            "value": "1",
            "name": "United States",
            "label": "+1"
        },
        {
            "value": "2",
            "name": "India",
            "label": "+91"
        },
        {
            "value": "3",
            "name": "Portugal",
            "label": "+351"
        },
        {
            "value": "4",
            "name": "Afganistan",
            "label": "+93"
        },
        {
            "value": "5",
            "name": "Albania",
            "label": "+355"
        },
        {
            "value": "6",
            "name": "Algeria",
            "label": "+213"
        },
        {
            "value": "7",
            "name": "Argentina",
            "label": "+54"
        },
        {
            "value": "8",
            "name": "Australia",
            "label": "+61"
        },
        {
            "value": "9",
            "name": "Azerbaijan",
            "label": "+994"
        },
        {
            "value": "10",
            "name": "Bahrain",
            "label": "+973"
        },
        {
            "value": "11",
            "name": "Barbados",
            "label": "+1-246"
        },
        {
            "value": "12",
            "name": "Belgium",
            "label": "+32"
        },
        {
            "value": "13",
            "name": "Benin",
            "label": "+229"
        },
        {
            "value": "14",
            "name": "Bolivia",
            "label": "+591"
        },
        {
            "value": "15",
            "name": "Botswana",
            "label": "+267"
        },
        {
            "value": "16",
            "name": "Brunei",
            "label": "+673"
        },
        {
            "value": "17",
            "name": "Burkina Faso",
            "label": "+226"
        },
        {
            "value": "18",
            "name": "Cambodia",
            "label": "+855"
        },
        {
            "value": "19",
            "name": "Canada",
            "label": "+1"
        },
        {
            "value": "20",
            "name": "Central African Rep.",
            "label": "+236"
        },
        {
            "value": "21",
            "name": "Chile",
            "label": "+56"
        },
        {
            "value": "22",
            "name": "Colombia",
            "label": "+57"
        },
        {
            "value": "23",
            "name": "Congo, The Democr. Rep. of the",
            "label": "+243"
        },
        {
            "value": "24",
            "name": "Cote d'Ivoire",
            "label": "+225"
        },
        {
            "value": "25",
            "name": "Cuba",
            "label": "+53"
        },
        {
            "value": "26",
            "name": "Czech Republic",
            "label": "+420"
        },
        {
            "value": "27",
            "name": "Burundi",
            "label": "+257"
        },
        {
            "value": "28",
            "name": "Cameroon",
            "label": "+237"
        },
        {
            "value": "29",
            "name": "Cape Verde",
            "label": "+238"
        },
        {
            "value": "30",
            "name": "Cayman Islands",
            "label": "+1-345"
        },
        {
            "value": "31",
            "name": "Chad",
            "label": "+235"
        },
        {
            "value": "32",
            "name": "China",
            "label": "+86"
        },
        {
            "value": "33",
            "name": "Christmas Island",
            "label": "+61"
        },
        {
            "value": "34",
            "name": "Cocos Islands",
            "label": "+61"
        },
        {
            "value": "35",
            "name": "Comoros",
            "label": "+269"
        },
        {
            "value": "36",
            "name": "Cook Islands",
            "label": "+682"
        },
        {
            "value": "37",
            "name": "Costa Rica",
            "label": "+506"
        },
        {
            "value": "38",
            "name": "Croatia",
            "label": "+385"
        },
        {
            "value": "39",
            "name": "Curacao",
            "label": "+599"
        },
        {
            "value": "40",
            "name": "Cyprus",
            "label": "+357"
        },
        {
            "value": "41",
            "name": "Denmark",
            "label": "+45"
        },
        {
            "value": "42",
            "name": "Djibouti",
            "label": "+253"
        },
        {
            "value": "43",
            "name": "Dominica",
            "label": "+1-767"
        },
        {
            "value": "44",
            "name": "Dominican Republic",
            "label": "+1-809" // 1-829, 1-849
        },
        {
            "value": "45",
            "name": "East Timor",
            "label": "+670"
        },
        {
            "value": "46",
            "name": "Ecuador",
            "label": "+593"
        },
        {
            "value": "47",
            "name": "Egypt",
            "label": "+20"
        },
        {
            "value": "48",
            "name": "El Salvador",
            "label": "+503"
        },
        {
            "value": "49",
            "name": "Equatorial Guinea",
            "label": "+240"
        },
        {
            "value": "50",
            "name": "Eritrea",
            "label": "+291"
        },
        {
            "value": "51",
            "name": "Estonia",
            "label": "+372"
        },
        {
            "value": "52",
            "name": "Ethiopia",
            "label": "+251"
        },
        {
            "value": "53",
            "name": "Falkland Islands",
            "label": "+500"
        },
        {
            "value": "54",
            "name": "Faroe Islands",
            "label": "+298"
        },
        {
            "value": "55",
            "name": "Fiji",
            "label": "+679"
        },
        {
            "value": "56",
            "name": "Finland",
            "label": "+358"
        },
        {
            "value": "57",
            "name": "France",
            "label": "+33"
        },
        {
            "value": "58",
            "name": "French Polynesia",
            "label": "+689"
        },
        {
            "value": "59",
            "name": "Gabon",
            "label": "+241"
        },
        {
            "value": "60",
            "name": "Gambia",
            "label": "+220"
        },
        {
            "value": "61",
            "name": "Georgia",
            "label": "+995"
        },
        {
            "value": "62",
            "name": "Germany",
            "label": "+49"
        },
        {
            "value": "63",
            "name": "Ghana",
            "label": "+233"
        },
        {
            "value": "64",
            "name": "Gibraltar",
            "label": "+350"
        },
        {
            "value": "65",
            "name": "Greece",
            "label": "+30"
        },
        {
            "value": "66",
            "name": "Greenland",
            "label": "+299"
        },
        {
            "value": "67",
            "name": "Grenada",
            "label": "+1-473"
        },
        {
            "value": "68",
            "name": "Guam",
            "label": "+1-671"
        },
        {
            "value": "69",
            "name": "Guatemala",
            "label": "+502"
        },
        {
            "value": "70",
            "name": "Guernsey",
            "label": "+44-1481"
        },
        {
            "value": "71",
            "name": "Guinea",
            "label": "+224"
        },
        {
            "value": "72",
            "name": "Guinea-Bissau",
            "label": "+245"
        },
        {
            "value": "73",
            "name": "Guyana",
            "label": "+592"
        },
        {
            "value": "74",
            "name": "Haiti",
            "label": "+509"
        },
        {
            "value": "75",
            "name": "Honduras",
            "label": "+504"
        },
        {
            "value": "76",
            "name": "Hong Kong",
            "label": "+852"
        },
        {
            "value": "77",
            "name": "Hungary",
            "label": "+36"
        },
        {
            "value": "78",
            "name": "Iceland",
            "label": "+354"
        },
        {
            "value": "79",
            "name": "Indonesia",
            "label": "+62"
        },
        {
            "value": "80",
            "name": "Iran",
            "label": "+98"
        },
        {
            "value": "81",
            "name": "Iraq",
            "label": "+964"
        },
        {
            "value": "82",
            "name": "Ireland",
            "label": "+353"
        },
        {
            "value": "83",
            "name": "Isle of Man",
            "label": "+44-1624"
        },
        {
            "value": "84",
            "name": "Israel",
            "label": "+972"
        },
        {
            "value": "85",
            "name": "Italy",
            "label": "+39"
        },
        {
            "value": "86",
            "name": "Ivory Coast",
            "label": "+225"
        },
        {
            "value": "87",
            "name": "Jamaica",
            "label": "+1-876"
        },
        {
            "value": "88",
            "name": "Japan",
            "label": "+81"
        },
        {
            "value": "89",
            "name": "Jersey",
            "label": "+44-1534"
        },
        {
            "value": "90",
            "name": "Jordan",
            "label": "+962"
        },
        {
            "value": "91",
            "name": "Kazakhstan",
            "label": "+7"
        },
        {
            "value": "92",
            "name": "Kenya",
            "label": "+254"
        },
        {
            "value": "93",
            "name": "Kiribati",
            "label": "+686"
        },
        {
            "value": "94",
            "name": "Kosovo",
            "label": "+383"
        },
        {
            "value": "95",
            "name": "Kuwait",
            "label": "+965"
        },
        {
            "value": "96",
            "name": "Kyrgyzstan",
            "label": "+996"
        },
        {
            "value": "97",
            "name": "Laos",
            "label": "+856"
        },
        {
            "value": "98",
            "name": "Latvia",
            "label": "+371"
        },
        {
            "value": "99",
            "name": "Lebanon",
            "label": "+961"
        },
        {
            "value": "100",
            "name": "Lesotho",
            "label": "+266"
        },
        {
            "value": "101",
            "name": "Liberia",
            "label": "+231"
        },
        {
            "value": "102",
            "name": "Libya",
            "label": "+218"
        },
        {
            "value": "103",
            "name": "Liechtenstein",
            "label": "+423"
        },
        {
            "value": "104",
            "name": "Lithuania",
            "label": "+370"
        },
        {
            "value": "105",
            "name": "Luxembourg",
            "label": "+352"
        },
        {
            "value": "106",
            "name": "Macau",
            "label": "+853"
        },
        {
            "value": "107",
            "name": "Macedonia",
            "label": "+389"
        },
        {
            "value": "108",
            "name": "Madagascar",
            "label": "+261"
        },
        {
            "value": "109",
            "name": "Malawi",
            "label": "+265"
        },
        {
            "value": "110",
            "name": "Malaysia",
            "label": "+60"
        },
        {
            "value": "111",
            "name": "Maldives",
            "label": "+960"
        },
        {
            "value": "112",
            "name": "Mali",
            "label": "+223"
        },
        {
            "value": "113",
            "name": "Malta",
            "label": "+356"
        },
        {
            "value": "114",
            "name": "Marshall Islands",
            "label": "+692"
        },
        {
            "value": "115",
            "name": "Mauritania",
            "label": "+222"
        },
        {
            "value": "116",
            "name": "Mauritius",
            "label": "+230"
        },
        {
            "value": "117",
            "name": "Mayotte",
            "label": "+262"
        },
        {
            "value": "118",
            "name": "Mexico",
            "label": "+52"
        },
        {
            "value": "119",
            "name": "Micronesia",
            "label": "+691"
        },
        {
            "value": "120",
            "name": "Moldova",
            "label": "+373"
        },
        {
            "value": "121",
            "name": "Monaco",
            "label": "+377"
        },
        {
            "value": "122",
            "name": "Mongolia",
            "label": "+976"
        },
        {
            "value": "123",
            "name": "Montenegro",
            "label": "+382"
        },
        {
            "value": "124",
            "name": "Montserrat",
            "label": "+1-664"
        },
        {
            "value": "125",
            "name": "Morocco",
            "label": "+212"
        },
        {
            "value": "126",
            "name": "Mozambique",
            "label": "+258"
        },
        {
            "value": "127",
            "name": "Myanmar",
            "label": "+95"
        },
        {
            "value": "128",
            "name": "Namibia",
            "label": "+264"
        },
        {
            "value": "129",
            "name": "Nauru",
            "label": "+674"
        },
        {
            "value": "130",
            "name": "Nepal",
            "label": "+977"
        },
        {
            "value": "131",
            "name": "Netherlands",
            "label": "+31"
        },
        {
            "value": "132",
            "name": "Netherlands Antilles",
            "label": "+599"
        },
        {
            "value": "133",
            "name": "New Caledonia",
            "label": "+687"
        },
        {
            "value": "134",
            "name": "New Zealand",
            "label": "+64"
        },
        {
            "value": "135",
            "name": "Nicaragua",
            "label": "+505"
        },
        {
            "value": "136",
            "name": "Niger",
            "label": "+227"
        },
        {
            "value": "137",
            "name": "Nigeria",
            "label": "+234"
        },
        {
            "value": "138",
            "name": "Niue",
            "label": "+683"
        },
        {
            "value": "139",
            "name": "North Korea",
            "label": "+850"
        },
        {
            "value": "140",
            "name": "Northern Mariana Islands",
            "label": "+1-670"
        },
        {
            "value": "141",
            "name": "Norway",
            "label": "+47"
        },
        {
            "value": "142",
            "name": "Oman",
            "label": "+968"
        },
        {
            "value": "143",
            "name": "Pakistan",
            "label": "+92"
        },
        {
            "value": "144",
            "name": "Palau",
            "label": "+680"
        },
        {
            "value": "145",
            "name": "Palestine",
            "label": "+970"
        },
        {
            "value": "146",
            "name": "Panama",
            "label": "+507"
        },
        {
            "value": "147",
            "name": "Papua New Guinea",
            "label": "+675"
        },
        {
            "value": "148",
            "name": "Paraguay",
            "label": "+595"
        },
        {
            "value": "149",
            "name": "Peru",
            "label": "+51"
        },
        {
            "value": "150",
            "name": "Philippines",
            "label": "+63"
        },
        {
            "value": "151",
            "name": "Pitcairn",
            "label": "+64"
        },
        {
            "value": "152",
            "name": "Poland",
            "label": "+48"
        },
        {
            "value": "153",
            "name": "Puerto Rico",
            "label": "+1-787"
        },
        {
            "value": "154",
            "name": "Qatar",
            "label": "+974"
        },
        {
            "value": "155",
            "name": "Republic of the Congo",
            "label": "+242"
        },
        {
            "value": "156",
            "name": "Reunion",
            "label": "+262"
        },
        {
            "value": "157",
            "name": "Romania",
            "label": "+40"
        },
        {
            "value": "158",
            "name": "Russia",
            "label": "+7"
        },
        {
            "value": "159",
            "name": "Rwanda",
            "label": "+250"
        },
        {
            "value": "160",
            "name": "Saint Barthelemy",
            "label": "+590"
        },
        {
            "value": "161",
            "name": "Saint Helena",
            "label": "+290"
        },
        {
            "value": "162",
            "name": "Saint Kitts and Nevis",
            "label": "+1-869"
        },
        {
            "value": "163",
            "name": "Saint Lucia",
            "label": "+1-758"
        },
        {
            "value": "164",
            "name": "Saint Martin",
            "label": "+590"
        },
        {
            "value": "165",
            "name": "Saint Pierre and Miquelon",
            "label": "+508"
        },
        {
            "value": "166",
            "name": "Saint Vincent and the Grenadines",
            "label": "+1-784"
        },
        {
            "value": "167",
            "name": "Samoa",
            "label": "+685"
        },
        {
            "value": "168",
            "name": "San Marino",
            "label": "+378"
        },
        {
            "value": "169",
            "name": "Sao Tome and Principe",
            "label": "+239"
        },
        {
            "value": "170",
            "name": "Saudi Arabia",
            "label": "+966"
        },
        {
            "value": "171",
            "name": "Senegal",
            "label": "+221"
        },
        {
            "value": "172",
            "name": "Serbia",
            "label": "+381"
        },
        {
            "value": "173",
            "name": "Seychelles",
            "label": "+248"
        },
        {
            "value": "174",
            "name": "Sierra Leone",
            "label": "+232"
        },
        {
            "value": "175",
            "name": "Singapore",
            "label": "+65"
        },
        {
            "value": "176",
            "name": "Sint Maarten",
            "label": "+1-721"
        },
        {
            "value": "177",
            "name": "Slovakia",
            "label": "+421"
        },
        {
            "value": "178",
            "name": "Slovenia",
            "label": "+386"
        },
        {
            "value": "179",
            "name": "Solomon Islands",
            "label": "+677"
        },
        {
            "value": "180",
            "name": "Somalia",
            "label": "+252"
        },
        {
            "value": "181",
            "name": "South Africa",
            "label": "+27"
        },
        {
            "value": "182",
            "name": "South Korea",
            "label": "+82"
        },
        {
            "value": "183",
            "name": "South Sudan",
            "label": "+211"
        },
        {
            "value": "184",
            "name": "Spain",
            "label": "+34"
        },
        {
            "value": "185",
            "name": "Sri Lanka",
            "label": "+94"
        },
        {
            "value": "186",
            "name": "Sudan",
            "label": "+249"
        },
        {
            "value": "187",
            "name": "Suriname",
            "label": "+597"
        },
        {
            "value": "188",
            "name": "Svalbard and Jan Mayen",
            "label": "+47"
        },
        {
            "value": "189",
            "name": "Swaziland",
            "label": "+268"
        },
        {
            "value": "190",
            "name": "Sweden",
            "label": "+46"
        },
        {
            "value": "191",
            "name": "Switzerland",
            "label": "+41"
        },
        {
            "value": "192",
            "name": "Syria",
            "label": "+963"
        },
        {
            "value": "193",
            "name": "Taiwan",
            "label": "+886"
        },
        {
            "value": "194",
            "name": "Tajikistan",
            "label": "+992"
        },
        {
            "value": "195",
            "name": "Tanzania",
            "label": "+255"
        },
        {
            "value": "196",
            "name": "Thailand",
            "label": "+66"
        },
        {
            "value": "197",
            "name": "Togo",
            "label": "+228"
        },
        {
            "value": "198",
            "name": "Tokelau",
            "label": "+690"
        },
        {
            "value": "199",
            "name": "Tonga",
            "label": "+676"
        },
        {
            "value": "200",
            "name": "Trinvaluead and Tobago",
            "label": "+1-868"
        },
        {
            "value": "201",
            "name": "Tunisia",
            "label": "+216"
        },
        {
            "value": "202",
            "name": "Turkey",
            "label": "+90"
        },
        {
            "value": "203",
            "name": "Turkmenistan",
            "label": "+993"
        },
        {
            "value": "204",
            "name": "Turks and Caicos Islands",
            "label": "+1-649"
        },
        {
            "value": "205",
            "name": "Tuvalu",
            "label": "+688"
        },
        {
            "value": "206",
            "name": "U.S. Virgin Islands",
            "label": "+1-340"
        },
        {
            "value": "207",
            "name": "Uganda",
            "label": "+256"
        },
        {
            "value": "208",
            "name": "Ukraine",
            "label": "+380"
        },
        {
            "value": "209",
            "name": "United Arab Emirates",
            "label": "+971"
        },
        {
            "value": "210",
            "name": "United Kingdom",
            "label": "+44"
        },
        {
            "value": "211",
            "name": "Uruguay",
            "label": "+598"
        },
        {
            "value": "212",
            "name": "Uzbekistan",
            "label": "+998"
        },
        {
            "value": "213",
            "name": "Vanuatu",
            "label": "+678"
        },
        {
            "value": "214",
            "name": "Vatican",
            "label": "+379"
        },
        {
            "value": "215",
            "name": "Venezuela",
            "label": "+58"
        },
        {
            "value": "216",
            "name": "Vietnam",
            "label": "+84"
        },
        {
            "value": "217",
            "name": "Wallis and Futuna",
            "label": "+681"
        },
        {
            "value": "218",
            "name": "Western Sahara",
            "label": "+212"
        },
        {
            "value": "219",
            "name": "Yemen",
            "label": "+967"
        },
        {
            "value": "220",
            "name": "Zambia",
            "label": "+260"
        },
        {
            "value": "221",
            "name": "Zimbabwe",
            "label": "+263"
        }
    ]
}