import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import {colors,urls,dimensions} from '../utils/constants';



 const CustomButton = (props) => {

  function _onPress()  {
    props.handler()
  }
  
  return (
    <TouchableOpacity style={[styles.container,props.style]}
    onPress={()=> _onPress() }>
        <Text style={[{fontWeight:'500',color:colors.BLACK,fontSize:19},props.textStyle]}>{props.label.toUpperCase()}</Text>
     </TouchableOpacity>
  )
};
CustomButton.defaultProps = {

  handler: () => {},
  label: 'Enter',
};


export default CustomButton

const styles = StyleSheet.create({

  container:{
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:colors.COLOR_PRIMARY,
    paddingHorizontal:15,
    paddingVertical:10,
    borderRadius:4,
    marginTop:20,
    marginBottom:10,
    borderWidth:0.7,
    borderColor:colors.COLOR_PRIMARY
  }
})
