import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image, Linking
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';

import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { postRequestNoBodyApi } from '../redux/actions/api_actions';
import config from '../config/config';
// import RNKommunicateChat from 'react-native-kommunicate-chat';

import LiveChat from 'react-native-livechat'
import WebView from 'react-native-webview';
import { Badge } from 'react-native-paper';
import { color } from 'react-native-reanimated';
import HomeHeader from '../components/home_header';


const ChatOption = (props) =>{

  function onChat(){
    props.seeView()
  }
  function onEmail(){
    Linking.openURL('mailto:info@utopiic.com?subject=SendMail&body=Description') 
  }
  return(
    <View style={{flex:1,alignItems:'center',position:'absolute',backgroundColor:'black',bottom:0,top:0}}>
     <HomeHeader  {...props}/>
      <ScrollView> 
        <View style={{height:30}}/>
        <Text style={[styles.heading,{textTransform:'uppercase'}]}>Your utopiic concierge</Text>
        <Text style={styles.heading}>Your personal guide to everything Utopiic can bring to you.</Text>

        <View style={{height:20}}/>

        <View style={{width:'90%'}}>
        <View style={styles.rowContainer}>
        <Badge size={10} style={styles.badgeStyle}/>
        <Text style={styles.badgeText}>Plan Your Travel</Text>
        </View>

        <View style={styles.rowContainer}>
        <Badge size={10} style={styles.badgeStyle}/>
        <Text style={styles.badgeText}>Reservation and Bookings with Utopiic Partners.</Text>
        </View>

        <View style={styles.rowContainer}>
        <Badge size={10} style={styles.badgeStyle}/>
        <Text style={styles.badgeText}>Queries about Utopiic Membership.</Text>
        </View>
        <View style={styles.rowContainer}>
        <Badge size={10} style={styles.badgeStyle}/>
        <Text style={styles.badgeText}>Suggestions for green collabrations</Text>
        </View>
        <View style={styles.rowContainer}>
        <Badge size={10} style={styles.badgeStyle}/>
        <Text style={styles.badgeText}>Recommendations for makeing the most of your Membership.</Text>
        </View>
        </View>

        <CustomButton 
        style={{width :dimensions.SCREEN_WIDTH * 0.6,marginTop:75,marginBottom:19,alignSelf:'center'}}
        handler={onChat}
        label ={'Live Chat'}/>

        <Text style={{color:colors.COLOR_PRIMARY,alignSelf:'center'}}>OR</Text>

        <CustomButton 
        style={{width :dimensions.SCREEN_WIDTH * 0.6,marginTop:25,marginBottom:50,alignSelf:'center'}}
        handler={onEmail}
        label ={'Email'}/>
      </ScrollView>
   </View>
  )
}


const Chat: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const apiReducer = useSelector(state => state.apiReducer)

    const dispatch = useDispatch()

    const [loading, setLoading] = useState(true); 
    const [webViewVisible, setWebViewVisible] = useState(false); 


  function initialize(){
    }

    useEffect(() =>{
 
    },[])

    function _seeView(){
      setLoading(false)
    }

   
   
 return (
    <>
      <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
    
          {/* <LiveChat
           // group={2}
            license={12430191}
            redirectUri="http://localhost/"
            clientId="2601d47ae90dd7144a1929f527153c9d"
            // chatTitle={'abc'}
            // greeting={'hello？'}
            // noAgents={'hissss!'}
          />  */}
          
         
        

    
           <WebView
            startInLoadingState={true}
            style={{backgroundColor:"black"}}
            source={{
              uri: 'https://secure.livechatinc.com/licence/12430191/v2/open_chat.cgi?name='+userReducer.user.name+'&email='+userReducer.user.email,
            }}
            startInLoadingState={true}
           // onLoad={() => setLoading(false)}
            // renderLoading={() => {
            //   //return <ProgressBar/>
            //   //return <ChatOption {...props}/>
            // }}
          />   
         

        {loading &&  <ChatOption seeView={_seeView} {...props} />}

     
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',justifyContent:'flex-start'
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        
        flexGrow:1
    },
   
    rowContainer:{
        flexDirection:'row',
        //justifyContent:'space-between',
        alignItems:'center',
        marginVertical:5
    },
    heading:{
      color:colors.COLOR_PRIMARY,
      lineHeight:26,
      fontSize:16,
      fontWeight:'700',
      alignSelf:'center',
      maxWidth:'90%',
      marginVertical:12,
      textAlign:'center'
  },
  badgeStyle:{
    backgroundColor:colors.COLOR_PRIMARY,
    marginHorizontal:10,
    marginBottom:3
  },
  badgeText:{color:colors.COLOR_PRIMARY,fontSize:13}
 
   

})


export default Chat;


