import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import ViewPager from '@react-native-community/viewpager';
import Swiper from 'react-native-swiper'
import Login from './login';
import FastImage from 'react-native-fast-image';
import { colors, dimensions, fonts } from '../utils/constants';




const Page= (props) =>{
 return(
  <View style={styles.bannerContainer}>
   <FastImage 
      source={props.image}
       style={{height:'100%',width:'100%'}} 
       resizeMode={FastImage.resizeMode.cover}/>
  </View>
 )
}
 

const WelcomePager = (props) => {
  return (
    <Swiper style={styles.wrapper}
     showsButtons={false} 
     loop={false}
     dotColor={colors.DARK_GREY}
     activeDotColor={colors.COLOR_PRIMARY}>

          <Page  image={require('../assets/banner_1.png')} />
          <Page  image={require('../assets/banner_2.png')} />
        <Page  image={require('../assets/banner_3.png')} />
        <Page  image={require('../assets/banner_4.png')} />


        <Login navigation={props.navigation}/>

      </Swiper>
  );
};

export default WelcomePager
 
const styles = StyleSheet.create({
    wrapper: {},
    slide1: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#9DD6EB'
    },
    slide2: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#97CAE5'
    },
    slide3: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#92BBD9'
    },
    text: {
      color: '#fff',
      fontSize: 30,
      fontWeight: 'bold'
    },
    bannerContainer:{
      flex:1,
      backgroundColor:'black'
    },
    bannerText:{
      position:'absolute',
      bottom:dimensions.SCREEN_HEIGHT * 0.25,
      color:'white',
      fontSize:20,
      fontWeight:'400',
      lineHeight:36,
      textAlign:'center',
      left:20,
      right:20,
      fontFamily:fonts.heading_font
    }
  })
   