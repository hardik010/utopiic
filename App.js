import React, { useEffect } from 'react';
import { createStore, combineReducers, applyMiddleware } from 'redux'
import AuthNavigator from './auth_navigator';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native'

import { useScreens } from 'react-native-screens';

import {Provider} from 'react-redux';
import {store, persistor} from './src/redux/store/configureStore';
import {PersistGate} from 'redux-persist/integration/react';
import analytics from '@react-native-firebase/analytics';
import messaging from '@react-native-firebase/messaging';
import firebase from '@react-native-firebase/app'
import { Platform } from 'react-native';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification from "react-native-push-notification";
import { ConfigurePushNotification } from './src/utils/notification_service';


const showLocalPushNotification = remoteMessage => {
  const { notification = {}, data = {} } = remoteMessage;
  PushNotification.localNotification({
    //smallIcon: 'ic_notification', // todo uncomment when resource is added
    channelId: '', // todo
    messageId: 'google:message_id',
    title: notification.title,
    message: notification.body,
    sound: 'default',
    data:{}
    
  });

 
};
   
//useScreens();
var unsubscribe;

//solved ios foreground issue this method
// When a user receives a push notification and the app is in foreground
messaging ().onMessage (async remoteMessage => {
   //console.log('Foreground--',remoteMessage)
  if(Platform.OS == 'ios') showLocalPushNotification(remoteMessage)
});


const App: () => React$Node = () => {
  
  useEffect(() => {
    //intialize()
    (async () => {
      await messaging().requestPermission({provisional: true});
      ConfigurePushNotification()
     
  })();

   
    return unsubscribe;
  }, []);
  
  return (

    <Provider store={store}>
      <PersistGate persistor={persistor} loading={null}>
       <NavigationContainer>
            <AuthNavigator/>
         </NavigationContainer>
      </PersistGate>
    </Provider>
  );
};


export default App;



/**
 * Helper method to deal with push notitifcation
 */


const  foregroundState = () => {
  const unsubscribe = messaging().onMessage(async notification => {
    console.log('Message handled in the foregroundState!', notification);
    
  });

  return unsubscribe;
};

// Register background handler
const backgroundState = () => {
  messaging().setBackgroundMessageHandler(async notification => {
    console.log('Message handled in the background!', notification);
  });
};
