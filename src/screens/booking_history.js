import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import { showMessage } from '../utils/snackmsg';

import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList } from 'react-native-gesture-handler';
import config from '../config/config';
import { postRequestApiWithToken, postRequestNoBodyApi, postRequestNoBodyApiWithToken } from '../redux/actions/api_actions';

import PriceBottomSheet from '../components/price_bottom_sheet';
import RazorpayCheckout from 'react-native-razorpay';

import { Chip } from 'react-native-paper';



const BookingHistory: () => React$Node = (props) => {

     const priceSheet = useRef()
     const priceSheetRef= useRef()

    const userReducer = useSelector(state => state.userReducer)
    const apiReducer = useSelector(state => state.apiReducer)

    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false); 
    const [historyList, setHistoryList] = useState([]);
    
    const [selectedItem, setSelectedItem] = useState(null);


    function reverseArr(input) {
      var ret = new Array;
      for(var i = input.length-1; i >= 0; i--) {
          ret.push(input[i]);
      }
      return ret;
  }



    function fetchHistory(){
      const URL = config.BASE_URL + config.PACKAGE_BOOKING_HISTORY
      var formData =new FormData()
      formData.append("user_id",userReducer.user.user_id)
      dispatch(postRequestApiWithToken(URL,formData,userReducer.user.token))
      .then(response=> {
          console.log('Response - ',JSON.stringify(response.data))
          if(response.status){
             setHistoryList(response.data)
          }
          else showMessage(response.message)
      })
      .catch(error => {
           showMessage(error.message)
           //console.log('Error - ',error)
      })
  }

    useEffect(() =>{
      const unsubscribe = props.navigation.addListener('focus', () => {
        // Screen was focused
        // Do something
        fetchHistory()
      });
        
    },[props.navigation])

   function  onPress  (index) {
       setSelectedIndex(index)
   }

   function isValid(){
     if(!selectedIndex) showMessage('Select Plan First')
     else props.navigation.navigate('payment_method',{'plan':plansList[selectedIndex]})
   }


    function renderItemOld (item,index)  {
        
        return(
          <View style={styles.paymentContainer}>
                <View>
                <Text style={[styles.textStyle,{textTransform:'uppercase'}]}>{item.Type == "Package" ? `Package Name : ${item.detail.name}` : 'Coupon'} </Text>
                <Text style={[styles.textStyle,,{textTransform:'uppercase'}]}>Booking ID : {item.booking_id}</Text>
               
                  {
                 item.Type == "Package" ?
                 <Text style={[styles.textStyle,{textTransform:'uppercase'}]}>{`Package ID : ${item.detail.package_id}` }</Text>
                  : null
                  }
                 

                  {
                  item.Type == "Package" ?
                     <Text style={[styles.textStyle,{textTransform:'uppercase'}]}>{`Price : ${item.final_price} ${item.currency}` }</Text>
                    : null
                  }

                  {
                 ( item.Type == "Package" && item.detail.package_type == 'Lifestyle' ) ? //Means Lifestyle
                 <Text style={[styles.textStyle,{textTransform:'uppercase'}]}>{`Tickets : ${item.booking_person} ` }</Text>
                  : null
                  }

                  {
                 ( item.Type == "Package" &&item.detail.package_type == 'Travel' ) ? //Means Travel
                 <Text style={[styles.textStyle,{textTransform:'uppercase'}]}>{`Rooms : ${item.room}` }</Text>
                  : null
                  }

                {
                 ( item.Type == "Package" &&item.detail.package_type == 'Travel' ) ? //Means Travel
                 <Text style={[styles.textStyle,{textTransform:'uppercase'}]}>{`Members : ${item.booking_person} ` }</Text>
                  : null
                  }


                </View>

                <View style={[styles.rowContainer,{alignItems:'flex-end'}]}>
                 <Text style={styles.textStyle}>Date : {item.date}</Text>
                  <View>
                     

                {
                      (item.Cancelled  && item.status !='Cancelled')
                      ?<Text onPress={() => {
                        _canceBooking(item.booking_id)
                      }}
                      style={[styles.textStyle,{
                        paddingHorizontal:13,paddingVertical:7,
                        borderRadius:10,borderWidth:0.8,
                        borderColor:colors.COLOR_PRIMARY
                        ,marginVertical:5
                      }]}>Cancel</Text>
                      : null
                    }


                    {
                       (item.request_option.length > 0  && item.status !='Cancelled')
                      ?<Text onPress={() => {
                        setSelectedItem(item) 
                        priceSheetRef.current.open()
                      }}
                      style={[styles.textStyle,{
                        paddingHorizontal:13,paddingVertical:7,
                        borderRadius:10,borderWidth:0.8,
                        borderColor:colors.COLOR_PRIMARY
                        ,marginVertical:5
                      }]}>Pay </Text>
                      : null
                    }

                    
            <Text style={[styles.textStyle,{
                      paddingHorizontal:13,paddingVertical:7,
                      borderRadius:10,borderWidth:0.8,
                      borderColor:colors.COLOR_PRIMARY
                      ,marginVertical:5
                    }]}>{item.status}</Text>

                  </View>
                </View>
          </View>
        )
   }


   const getBackgroundColor = (status) =>{
    if(status == 'Confirmed') return 'green'
    else if(status == 'Booked') return 'green'
    else if(status == 'Cancelled') return 'red'
    else if(status == 'Pending') return 'red'
    else return 'white'
  }

  const getTextColor = (status) =>{
    if(status == 'Confirmed') return 'white'
    else if(status == 'Booked') return 'black'
    else if(status == 'Cancelled') return 'white'
    else if(status == 'Pending') return 'white'
    else return 'black'
  }
    function renderItem(item,index)  {
        return (
          <View style={styles.listContainer}>

            <View style={[styles.rowDirection]}>
                <View style={styles.rowDirection}>
                    <Text style={styles.textStyleMain}>Request Type : </Text>
                    <Text style={styles.textStyleValue}>{item.Type == "Package" ? `Package (${item.detail.name.substring(0,15)})` :  `Coupon (${ item.detail  && item.detail.type_name ? item.detail.type_name.substring(0,15) : ''})`} </Text>
                </View>
            </View>

            <View style={{height:12}}/>
            <View style={[styles.rowDirection]}>
              <View style={styles.rowDirection}>
                      <Text style={styles.textStyleMain}>Booking Id : </Text>
                      <Text style={styles.textStyleValue}>{item.booking_id} </Text>
             </View>

              <View/>
              {/**
                item.Type == "Package" ?
                <View style={styles.rowDirection}>
                      <Text style={styles.textStyleMain}>Price : </Text>
                      <Text style={styles.textStyleValue}>{item.final_price}  <Text style={[styles.textStyleValue,{textTransform:'uppercase'}]}>{item.currency} </Text></Text>
               </View>
               : null
               */}
            </View>


            <View style={{height:12}}/>
              <View style={[styles.rowDirection,{justifyContent:'flex-start'}]}>
                      <Text style={styles.textStyleMain}>Date : </Text>
                      <Text style={styles.textStyleValue}>{item.date}</Text>
             </View>  

           
            {
                 item.Type == "Package" ?
                 <>
                 <View style={{height:12}}/>
                  <View style={[styles.rowDirection,{justifyContent:'flex-start'}]}>
                      <Text style={styles.textStyleMain}>Package Id : </Text>
                      <Text style={styles.textStyleValue}>{item.detail.package_id}</Text>
                  </View>  
                  </>
                 : null
            }
            
             {
                 item.Type == "Package" ?
                 <>
                 <View style={{height:12}}/>
                  <View style={[styles.rowDirection,{justifyContent:'flex-start'}]}>
                      <Text style={styles.textStyleMain}>Package Type : </Text>
                      <Text style={styles.textStyleValue}>{item.detail.package_type}</Text>
                  </View>  
                  </>
                 : null
            }

           
            {
                 ( item.Type == "Package" && item.detail.package_type == 'Lifestyle' )  ?
                 <>
                 <View style={{height:12}}/>
                  <View style={[styles.rowDirection,{justifyContent:'flex-start'}]}>
                      <Text style={styles.textStyleMain}>Tickets : </Text>
                      <Text style={styles.textStyleValue}>{item.booking_person}</Text>
                  </View>  
                  </>
                 : null
           }
            {
                  ( item.Type == "Package" && item.detail.package_type == 'Travel' )  ?
                  <>
                  <View style={{height:12}}/>
                  <View style={[styles.rowDirection,{justifyContent:'flex-start'}]}>
                      <Text style={styles.textStyleMain}>Rooms : </Text>
                      <Text style={styles.textStyleValue}>{item.rooms}</Text>
                  </View>  
                  </>
                 : null
            }

            {
                  ( item.Type == "Package" && item.detail.package_type == 'Travel' )  ?
                  <>
                  <View style={{height:12}}/>
                  <View style={[styles.rowDirection,{justifyContent:'flex-start'}]}>
                      <Text style={styles.textStyleMain}>Members: </Text>
                      <Text style={styles.textStyleValue}>{item.booking_person}</Text>
                  </View>  
                  </>
                 : null
            }

            <View style={styles.listDivider}/>
            <View style={{height:12}}/>
            <View style={[styles.rowDirection,{width:'100%'}]}>
            <Chip textStyle={{color:getTextColor(item.status)}}
             style={{backgroundColor:getBackgroundColor(item.status)}}
            onPress={() => {}}>{item.status}</Chip>

            {
                       (item.request_option.length > 0  && item.status !='Cancelled' && item.status !='Confirmed'  && item.status =='Pending') ?
                       <Chip 
                       textStyle={{color:'black'}}
                       style={{backgroundColor:'orange'}} 
                       onPress={() => {
                                   setSelectedItem(item) 
                                   priceSheetRef.current.open()
                         }}>  Pay Now  </Chip>
                      : <View style={{width:30}}/>
                }


            {
             (item.Cancelled  && item.status !='Cancelled' ) ?
            <Chip 
              textStyle={{color:'black'}}
              style={{backgroundColor:'#FF7F7F'}} 
              onPress={() =>  _canceBooking(item.booking_id)}>Cancel</Chip>
              : <View style={{width:30}}/>
            }

            </View>

          </View>
        );
    }


 function _book(txn_id,price,booking_id){
  const URL = config.BASE_URL + config.PAYMENT_API

  var formdata = new FormData()
  formdata.append('user_id',userReducer.user.user_id)
  formdata.append('booking_id',booking_id)
  formdata.append('txn_id', txn_id)
  formdata.append('price', price)
 //console.log(formdata)
  dispatch(postRequestApiWithToken(URL,formdata,userReducer.user.token))
  .then(response=> {
     // console.log('Response - ',JSON.stringify(response))
      if(response.status){
          showMessage('Booked Successfully',false)
          //props.navigation.pop()
          setHistoryList(historyList.map(e => e.booking_id == booking_id ? { ...e, status: 'Confirmed'  } : e))

      }
      else showMessage(response.message)
  })
  .catch(error => {
     showMessage(error.message)
       console.log('Error - ',error)
  })
}

function _canceBooking(booking_id){
  const URL = config.BASE_URL + config.BOOKING_CANCELLED
  var formdata = new FormData()
  formdata.append('user_id',userReducer.user.user_id)
  formdata.append('booking_id',booking_id)
  dispatch(postRequestApiWithToken(URL,formdata,userReducer.user.token))
  .then(response=> {
      //console.log('Response - ',JSON.stringify(response))
      if(response.status){
         // setHistoryList(historyList.filter(element => element.id != booking_id) )
         setHistoryList(historyList.map(e => e.booking_id == booking_id ? { ...e, status: 'Cancelled'  } : e))
      }
      else showMessage(response.message)
  })
  .catch(error => {
     showMessage(error.message)
      //console.log('Error - ',error)
  })
}



function _razorpayPayment(price,booking_id){
    var image ='https://static.wixstatic.com/media/20b4ce_d696dfd13905444d8ca2f37a5e9fe91d~mv2.png/v1/fill/w_344,h_100,al_c,q_85,usm_0.66_1.00_0.01/Asset%2024.webp'
    var options = {description: 'Utopiic - A travel Concierge',
        image: image,
        currency: 'INR',
        key:'rzp_live_QXWEW7zcLOxpvI',
        amount: parseInt(price) *  100 ,
       // key: 'rzp_test_bkOHPTNmXd5abE',
       // amount:'200',
        name: 'Utopiic',
        prefill: {email:userReducer.user.email,contact:userReducer.user.phone,name:userReducer.user.name },
        theme: {color: '#000000'}
      } 
     
      priceSheetRef.current.close()
      RazorpayCheckout.open(options).then((data) => {
        _book(data.razorpay_payment_id,price,booking_id)
      }).catch((error) => {
        showMessage(error.description)
      });
  }


  function _onDone(index){
          if(index != null){
            let price = selectedItem.request_option[index].price
            let id = selectedItem.booking_id
            _razorpayPayment(price,id)
          }
  }

  function _onClose(){
        priceSheetRef.current.close()
  }

   return (
    <>
    <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
       <BackHeader {...props}/>
       <PriceBottomSheet item={selectedItem}
       // ref={priceSheet}
       onClose={_onClose}
         inputRef={priceSheetRef} {...props} onDone={_onDone} />
        <InputScrollView 
        bounces={false}
        useAnimatedScrollView={true}
        contentContainerStyle={styles.scrollContainer}>

        <View style={{height:10}}/>
          <Text style={styles.heading}>REQUEST HISTORY</Text>
           <View style={{height:20}}/>       

               <FlatList 
                contentContainerStyle={{marginBottom:30}}
                showsVerticalScrollIndicator={false}
                scrollEnabled={false}
                //ItemSeparatorComponent={itemSeparator}
                data={historyList}
                renderItem={({ item ,index}) => renderItem(item,index)}
                keyExtractor={(item) => item.id } />         

             <View style={{height:30}}/>         

      </InputScrollView>

      { apiReducer.loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,

      },
      heading:{
        color:colors.COLOR_PRIMARY,
        lineHeight:30,
        fontSize:19,
        fontWeight:'300',
        alignSelf:'center',
        textAlign:'center',
        maxWidth:'70%'
    },
    
    seperator:{
      height: .4,
      width: dimensions.SCREEN_WIDTH,
      backgroundColor: colors.COLOR_PRIMARY,
    },
    paymentContainer:{
        width:dimensions.SCREEN_WIDTH * 0.9,
        borderRadius:10,
        borderColor:colors.COLOR_PRIMARY,
        borderWidth:0.7,
        padding:9,
        //height:dimensions.SCREEN_HEIGHT * 0.2,
        margin:7,
        justifyContent:'space-between'
    },
    rowContainer:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        marginBottom:15
    },
    textStyle:{
        color:colors.COLOR_PRIMARY,
        textTransform:'capitalize'
    },

    //
    listContainer:{
      width:dimensions.SCREEN_WIDTH * 0.9,
      borderRadius:10,
      borderColor:colors.COLOR_PRIMARY,
      borderWidth:0.7,
      padding:10,
      margin:7,
  },
   rowDirection:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between',
   },
   textStyleValue:{
    color:colors.COLOR_PRIMARY,
    textTransform:'capitalize'
   },
   textStyleMain:{
    color:colors.WHITE,
    fontSize:16,
    fontWeight:'700',
    textTransform:'uppercase'
   },
   listDivider:{
     width:'100%',
     height:0.5,
     backgroundColor:'grey',
     marginVertical:7

   }
   //


})


export default BookingHistory;


