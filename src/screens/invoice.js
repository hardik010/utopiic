import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';


import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';




const Invoice: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false); 


    useEffect(() =>{
    },[])

    function _onOkay(){
         props.navigation.navigate('otp')
    }

   
 return (
    <>
       <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
      <ScrollView 
        bounces={false}
        useAnimatedScrollView={true}
        contentContainerStyle={styles.scrollContainer}>

          <Text style={styles.heading}>INVOICE</Text>
          <View style={{height:30}}/> 

          <View style={{width:'70%'}}>  
            <View style={[styles.rowContainer,{margin:5}]}>
                 <Text style={styles.labelText}>Order Date</Text>
                <Text style={styles.labelText}>14 november 2020</Text>
            </View>
            <View style={[styles.rowContainer,{margin:5}]}>
                 <Text style={styles.labelText}>Order #</Text>
                <Text style={styles.labelText}>UT01</Text>
            </View>
            <View style={[styles.rowContainer,{margin:5}]}>
                 <Text style={styles.labelText}>Order </Text>
                <Text style={styles.labelText}>SAPLING PLAN</Text>
            </View>
            <View style={[styles.rowContainer,{margin:5}]}>
                 <Text style={styles.labelText}>Payment Method</Text>
                <Text style={styles.labelText}>credit card</Text>
            </View>
            <View style={[styles.rowContainer,{margin:5}]}>
                 <Text style={styles.labelText}>Billing Address </Text>
                <Text style={styles.labelText}>DELHI,INDIA</Text>
            </View>
          </View>  

          <View style={{height:50}}/> 


        <Text style={styles.headingSub}>Bill Breakup</Text>  
            <View style={{width:'90%'}}>
                <View style={[styles.rowContainer,{margin:5}]}>
                        <Text style={styles.labelText}>Plan Cost</Text>
                        <Text style={styles.labelText}>₹ 7,999</Text>
                </View>
                <View style={[styles.rowContainer,{margin:5}]}>
                        <Text style={styles.labelText}>Tax</Text>
                        <Text style={styles.labelText}>₹ 7,999</Text>
                </View>
                <View style={[styles.rowContainer,{margin:5}]}>
                        <Text style={styles.labelText}>Order Total</Text>
                        <Text style={styles.labelText}>₹ 8,499</Text>
                </View>
            </View>

         <View style={{height:30}}/> 



         <View style={{flexDirection:'row',alignItems:'center',width:'90%',justifyContent:'space-evenly'}}>
             <TouchableOpacity style={styles.buttonContainer}>
                <Text>DOWNLOAD</Text>
             </TouchableOpacity>

             <TouchableOpacity style={styles.buttonContainer}>
                <Text>EMAIL RECEIPT</Text>
             </TouchableOpacity>

             <TouchableOpacity style={styles.buttonContainer}>
                <Text>SIGN IN</Text>
             </TouchableOpacity>
         </View>

      </ScrollView>

      {loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        justifyContent:'center',
        flexGrow:1
    },
    heading:{
        color:colors.COLOR_PRIMARY,
        lineHeight:30,
        fontSize:17,
        fontWeight:'500',
        alignSelf:'center',
        textAlign:'center',
        maxWidth:'70%'
    },
    headingSub:{
        color:colors.COLOR_PRIMARY,
        lineHeight:40,
        fontWeight:'600',
        fontSize:15
    },
    rowContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    labelText:{
        color:colors.COLOR_PRIMARY,
        fontSize:13
    },
    buttonContainer:{
        flex:1,
        margin:5,
        backgroundColor:colors.COLOR_PRIMARY,
        justifyContent:'center',
        alignItems:'center',
        paddingHorizontal:10,
        paddingVertical:5
    }

})


export default Invoice;


