import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TextInput
} from 'react-native';

import FastImage from 'react-native-fast-image'
import { TouchableOpacity } from 'react-native-gesture-handler';

import {colors,urls,dimensions} from '../utils/constants';

const SearchListComponent = (props) =>{
    return(
        <View style={[styles.cardContainer,styles.cardBackground]}>
                <FastImage source ={{uri:props.image}} style={{
                    position:'absolute',
                    top:0,left:0,
                    right:0,left:0,height:'100%',width:'100%'
                }}/>
                <View style={styles.nameContainer}>
                    <Text style={styles.cardTitle}>{props.name}</Text>
                    <Text style={{color:'white',textAlign:'center'}}>{props.subName.substring(0,100)}</Text>
                </View>
        </View>
    )
   }


SearchListComponent.defaultProps = {

    focus: () => {},
    onSearch: () => {},
    image: '',
    name:'',
    subName:''
};


const styles = StyleSheet.create({
    cardContainer:{
        width:dimensions.SCREEN_WIDTH,
        height:dimensions.SCREEN_HEIGHT * 0.31,
        marginVertical:7,
        backgroundColor:'black'
     },
     nameContainer:{
         marginBottom:20,
         alignItems:'center',
         padding:10,
         backgroundColor:'rgba(0,0,0,0.3)',
        width:'100%'
    },
     cardBackground:{
        justifyContent:'flex-end',
        alignItems:'center',
        backgroundColor:colors.COLOR_PRIMARY
     },
    cardTitle:{
            color:'white',
            fontSize:17,
            fontWeight:'800',
            textTransform:'uppercase',
            lineHeight:30,
            textAlign:'center'
        },
    cardSubTitle:{
        color:'white',
        lineHeight:28,
        textTransform:'uppercase'
    },
   
  });

export default SearchListComponent;
