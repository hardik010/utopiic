import React, { useEffect, useState } from 'react';
import {  useSelector, useDispatch } from 'react-redux';
import { NavigationContainer,DefaultTheme } from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack"
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, TouchableOpacity, SafeAreaView,Alert} from 'react-native';

import WelcomePager from '../screens/welcome_pager.js';
import SignUp from '../screens/signup.js';
import Plans from '../screens/plans.js';
import PaymentMethod from '../screens/payment_method.js';
import Invoice from '../screens/invoice.js';
import Otp from '../screens/otp.js';
import Interest from '../screens/interest.js';
import DrawerNavigator from './drawer_navigator.js';
import TermsConditions from '../screens/terms.js';
import PrivacyPolicy from '../screens/privacy.js';

//navigators
const Stack = createStackNavigator()


const LoginNavigator = () => {
     
  return (
        <Stack.Navigator initialRouteName='welcome_pager' mode='modal' options={{ gesturesEnabled: true,}}>
          <Stack.Screen name="welcome_pager" component={WelcomePager} options ={{ headerShown :false }} />
          <Stack.Screen name="signup" component={SignUp}  options = {{headerShown :false}} />
          <Stack.Screen name="plans" component={Plans}  options = {{headerShown :false}} />
          <Stack.Screen name="payment_method" component={PaymentMethod}  options = {{headerShown :false}} />
          <Stack.Screen name="invoice" component={Invoice}  options = {{headerShown :false}} />
          <Stack.Screen name="otp" component={Otp}  options = {{headerShown :false}} />
          <Stack.Screen name="interest" component={Interest}  options = {{headerShown :false}} />
          <Stack.Screen name="terms" component={TermsConditions}  options = {{headerShown :false}} />
          <Stack.Screen name="privacy" component={PrivacyPolicy}  options = {{headerShown :false}} />
          <Stack.Screen name="home" component={DrawerNavigator}  options = {{headerShown :false}} />

        </Stack.Navigator>
    )
}

export default LoginNavigator