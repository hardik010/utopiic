import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';

import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { postRequestNoBodyApi } from '../redux/actions/api_actions';
import config from '../config/config';




const Dummy: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const apiReducer = useSelector(state => state.apiReducer)

    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false); 

    function fetchDummy(){
        const URL = config.BASE_URL + config.Dummy
        
        dispatch(postRequestNoBodyApi(URL))
        .then(response=> {
            console.log('Response - ',response)
        })
        .catch(error => {
           // showMessage(error.message)
            console.log('Error - ',error)
        })
    }

    useEffect(() =>{
        fetchDummy()
    },[])

   
   
 return (
    <>
       <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
      <ScrollView 
        bounces={false}
        useAnimatedScrollView={true}
        contentContainerStyle={styles.scrollContainer}>

      </ScrollView>
      {apiReducer.loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        justifyContent:'center',
        flexGrow:1
    },
   
    rowContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
   

})


export default Dummy;


