import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TouchableOpacity
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';

import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList } from 'react-native-gesture-handler';
import { postRequestApiWithToken, postRequestNoBodyApi } from '../redux/actions/api_actions';
import config from '../config/config';
import HomeHeader from '../components/home_header';
import Clipboard from '@react-native-community/clipboard';

import Carousel from 'react-native-banner-carousel';
import Modal from 'react-native-modal';



const BrandDetails: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const apiReducer = useSelector(state => state.apiReducer)

    const dispatch = useDispatch()

    const [coupons, setCoupons] = useState([]); 
    const [brand, setBrand] = useState(''); 
    const [selectedIndex, setSelectedIndex] = useState(null);

    const [modalVisibility, setModalVisibility] = useState(false);
  

    function fetchCoupons(brandId){
        const URL = config.BASE_URL + config.COUPON_LIST
        var formData = new FormData()
        formData.append('supplier_id',brandId)
       // formData.append('supplier_id',433)
     
       //console.log('Response - ',JSON.stringify(brandId))
        dispatch(postRequestApiWithToken(URL,formData,userReducer.user.token))
        .then(response=> {
            console.log('Response - ',JSON.stringify(response))
            if(response.status){
              // let temp =[]
              // response.data.map(e =>{
              //   if(!(e.coupon.length == 0)) temp.push(e)
              //   //if array mean empty coiupon ele object means coiupin is there
              // })
              // console.log('Response - ',JSON.stringify(temp))
              setCoupons(response.data)
            }
            //setCoupons(response)
        })
        .catch(error => {
            showMessage(error.message)
            //console.log('Error in  - ',error)
        })
    }

    function getCouponId(){
      let item = coupons[selectedIndex]
      // if(item.coupon_type == 'promocode'){
      //     return item.coupon.promocode.coupon_code[0].coupon_id
      // }
      // else if(item.coupon_type == 'speciallink'){
      //   return item.coupon.speciallink.coupon_code[0].coupon_id
      // }
      // else if(item.coupon_type == 'exclusiveoffer'){
      //   return item.coupon.exclusiveoffer.coupon_code[0].coupon_id
      // }
      return item.coupon_id
    }

    function exclusiveBrand(){
      setModalVisibility(false)
      //showMessage("Applied Successfully",false)
      const URL = config.BASE_URL + config.EXCLUSIVE_OFFER_APPLY
      var formData = new FormData()
      formData.append('offer_id',getCouponId())
       formData.append('user_id',userReducer.user.user_id)
       formData.append('user_type',coupons[selectedIndex].coupon_type_id)
      formData.append('brand_id',props.route.params.item.id)
      console.log('formadate=---',formData)
      dispatch(postRequestApiWithToken(URL,formData,userReducer.user.token))
      .then(response=> {
          //console.log('Response - ',JSON.stringify(response))
         
          if(response.status) {
            showMessage("Applied Successfully",false)
          }
          else showMessage(response.message)
      })
      .catch(error => {
         showMessage(error.message)
          //console.log('Error - ',error)
      })
  }

  function getBrandDetails(id,type){
    console.log(id)
    const URL = config.BASE_URL + config.SUPPLIER_DETAIL
    var formData = new FormData()
     formData.append('id',id)
     formData.append('type',type)
    dispatch(postRequestApiWithToken(URL,formData,userReducer.user.token))
    .then(response=> {
        //console.log('Response - ',JSON.stringify(response))
        if(response.status){
          setBrand(response.data)
        }
    })
    .catch(error => {
       showMessage(error.message)
        //console.log('Error - ',error)
    })
}

    useEffect(() =>{
        //fetchCoupons(props.route.params.item.supplier_id)
         fetchCoupons(props.route.params.item.id)
        //console.log(props.route.params.item.id)
         getBrandDetails(props.route.params.item.id,props.route.params.item.type)
       // //console.log(props.route.params.item)
         //setBrand(props.route.params.item)
    },[])

    function navigateToWeb(item){
      props.navigation.navigate('view_web',{'url':item})
   }

   const copyToClipboard = (string) => {
    Clipboard.setString(string);
  };

  const fetchCopiedText = async () => {
    const text = await Clipboard.getString();
  };

    function _redeemCoupon(){
      /**
       * type 1 promo code
       * type 2 special link
       * type 3 exclusive offer
       */
      if(selectedIndex == null ) showMessage("Select Coupon First")
      else {
        let item = coupons[selectedIndex]
        if(item.coupon_type == 'promocode'){
          copyToClipboard(item.coupon_code)
          showMessage('Coupon code copied to clipboard',false)
          navigateToWeb(item.URL)
        }
        else if(item.coupon_type == 'speciallink'){
          navigateToWeb(item.URL)
        }
        else if(item.coupon_type == 'exclusiveoffer'){
          setModalVisibility(true)
        }
      }


    }


    function _redeemCouponOld(){
      /**
       * type 1 promo code
       * type 2 special link
       * type 3 exclusive offer
       */
      if(selectedIndex == null ) showMessage("Select Coupon First")
      else {
        let item = coupons[selectedIndex]
        if(item.URL.length > 0 && item.coupon_list.length > 0){
          copyToClipboard(item.coupon_list[0].coupon_code)
          showMessage('Coupon code copied to clipboard',false)
          navigateToWeb(item.URL)
        }
        else if(item.URL.length > 0 && item.coupon_list.length == 0){
          navigateToWeb(item.URL)
        }
        else if(item.URL.length == 0 && item.coupon_list.length == 0){
          exclusiveBrand()
        }
      }


    }
   
    const itemSeparator = () => {
        return (
          <View style={styles.seperator} />
        );
      }

      function  onPress  (index) {
        setSelectedIndex(index)
    }

    function getDesc(item){
      if(item.coupon_type == 'exclusiveoffer') return item.coupon.exclusiveoffer.description
      else if(item.coupon_type == 'speciallink') return item.coupon.speciallink.description
      else if(item.coupon_type == 'promocode') return item.coupon.promocode.description
    }

      function renderItem (item,index)  {
        return(
           <View style={[styles.rowContainer,{width:dimensions.SCREEN_WIDTH * 0.9
           ,margin:10}]}>
               <Text style={{color:colors.COLOR_PRIMARY,fontSize:16,textTransform:'capitalize',width:'80%'}}>
                   {/* {getDesc(item)} */}
                   {item.description}
               </Text>
             
               <RadioButton
                size={12}
                innerColor={colors.COLOR_PRIMARY}
                outerColor={colors.COLOR_PRIMARY}
                animation={'bounceIn'}
                isSelected={selectedIndex == index}
                onPress={() => {onPress(index)}}
                />
            
           </View>
        )
      }

      function renderPage(item,index){
        return (
            <View key={index}>
                 <FastImage
                style={{ width: dimensions.SCREEN_WIDTH, height: '100%' }}
                source={{ uri: item.image,
                    priority: FastImage.priority.normal,
                }}
                resizeMode={FastImage.resizeMode.cover}/>
            </View>
        );
    }


 return (
    <>
       <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>

          {/*  Modal */}

          <Modal 
          onBackdropPress={() => setModalVisibility(false)}
          onBackButtonPress={() => setModalVisibility(false)}
          backdropColor={'black'}
          backdropOpacity={0.4}
          animationIn={'zoomInDown'}
          animationOut={'zoomOutUp'}
          animationInTiming={100}
          animationOutTiming={100}
          backdropTransitionInTiming={100}
          backdropTransitionOutTiming={100}
          style={{backgroundColor:'rgba(0,0,0,0.4)',margin:0}}
          isVisible={modalVisibility}>
            <View style={styles.modalContainer}>
                <Text style={styles.modalText}>Thanks for the request.Benefit Redemption details will be shared on your registered email ID.</Text>
                <View style={{height:40}}/>
                <View style={styles.rowContainer}>
                  <TouchableOpacity onPress={() => { setModalVisibility(false); exclusiveBrand()}}
                  style={styles.modalLeftButton}>
                      <Text style={styles.leftButtonText}>Okay</Text>
                    </TouchableOpacity>
                </View>
           </View>
        </Modal>

           {/*  Modal  end*/}



      <HomeHeader haveBack {...props}/>
      <ScrollView 
        bounces={false}
        useAnimatedScrollView={true}
        contentContainerStyle={styles.scrollContainer}>
            
        <View style={styles.imageContainer}>
        {
         brand && brand.images ?
         <Carousel
         style={{alignSelf:'center'}}
         autoplay  autoplayTimeout={4000}
         in
         loop index={0} pageSize={dimensions.SCREEN_WIDTH}>
         {brand.images.map((image, index) => renderPage(image, index))}
         </Carousel>
                 : null
               }
        </View> 

        <View style={[styles.rowContainer,{margin:10,justifyContent:'flex-start'}]}>
               <View style={{flex:4}}>
                    <Text style={[styles.nameText,{width:'100%'}]}>{brand.name}</Text>
                    <Text style={styles.countryText}>{brand.country}</Text>
                    <Text style={styles.categoryText}>{brand.industry_name}</Text>
               </View>
               <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <FastImage
                      style={{ width:50, height: 50}}
                      source={require('../assets/brand_logo.png')}
                      resizeMode={FastImage.resizeMode.cover}
                  />
               </View>  
        </View>
        <Text style={[styles.categoryText,{margin:10}]}>{brand.description}</Text>
        <View style={{height:20}}/>     
        <Text style={styles.heading}>Member Benefits</Text>    
        <View style={{height:20}}/>     
        <View style={styles.seperator} />
              <View>
               <FlatList 
                showsVerticalScrollIndicator={false}
                scrollEnabled={false}
                ItemSeparatorComponent={itemSeparator}
                data={coupons}
                renderItem={({ item ,index}) => renderItem(item,index)}
                keyExtractor={(item) => item.id } />   
                </View>      
         <View style={styles.seperator} />

         <CustomButton 
          style={{width :dimensions.SCREEN_WIDTH * 0.5,
            marginTop:25,marginBottom:19,alignSelf:'center'}}
          handler={_redeemCoupon}
          label ={'Redeem'}/>

      </ScrollView>
      {apiReducer.loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        flexGrow:1
    },
   
    rowContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    imageContainer:{
        width:dimensions.SCREEN_WIDTH,
        height:dimensions.SCREEN_HEIGHT * 0.35,
        backgroundColor:colors.COLOR_PRIMARY
    },
    nameText:{
        color:'white',
        fontSize:20,
        fontWeight:'900'
    },
    countryText:{
        color:'white',
        fontSize:15,
        fontWeight:'600'
    },
    categoryText:{
        color:'white',
        fontSize:15,
        fontWeight:'600'
    },
    heading:{
        color:colors.COLOR_PRIMARY,
        margin:10,
        fontSize:17,
        textTransform:'uppercase',
        fontWeight:'700',
        alignSelf:'center'
    },
    seperator:{
        height: 1,
        width: dimensions.SCREEN_WIDTH,
        backgroundColor: colors.COLOR_PRIMARY,
        marginVertical:2
      },
      modalContainer:{
        width: dimensions.SCREEN_WIDTH * 0.7,
        height:dimensions.SCREEN_HEIGHT * 0.5,
        height: 300,backgroundColor:colors.COLOR_PRIMARY,
        borderRadius:20,
        justifyContent:'center',
        alignSelf:'center',
        alignItems:'center',
        padding:20
        },
        modalText:{
          fontWeight:'bold',
          fontSize:18,
          lineHeight:28,
          marginVertical:15,
          textAlign:'center'
        },
        modalLeftButton:{
          backgroundColor:'black',
          paddingHorizontal:10,
          paddingVertical:8,
          justifyContent:'center',
          alignItems:'center',
          borderWidth:1,
          borderColor:'black',
          width:100,
          borderRadius:12,
          margin:10,
          elevation:10,
          shadowOpacity: 0.3,
          shadowRadius: 3,
          shadowOffset: {
                height: 0,
                width: 0
           },
      },
      modalRightButton:{
        backgroundColor:colors.COLOR_PRIMARY,
        paddingHorizontal:10,
        paddingVertical:8,
        justifyContent:'center',
        alignItems:'center',
        borderWidth:1,
        borderColor:'black',
        borderRadius:12,
        width:100,
        margin:10,
        elevation:10,
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowOffset: {
              height: 0,
              width: 0
         },
        },
        leftButtonText:{color:colors.COLOR_PRIMARY,fontWeight:'bold'},
        rightButtonText:{color:colors.BLACK,fontWeight:'bold'},
   

})


export default BrandDetails;


