import React, { useState ,forwardRef, useRef, useImperativeHandle } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TextInput
} from 'react-native';

import FastImage from 'react-native-fast-image'
import {colors,urls,dimensions} from '../utils/constants';

const CustomTextInput = React.forwardRef((props,ref) =>  {

    const [text , setText ]  = useState('');
    const [isFocus , setFocus ]  = useState(false);

     // The component instance will be extended
     // with whatever you return from the callback passed
     // as the second argument
     useImperativeHandle(ref, () => ({
            getInputValue() {
                return text
            },
            setVal(text){
              setText(text)
            }
      }));


  return (
    <View style={[styles.container, props.style]}>
  
       <TextInput  
        {...props}
        ref={props.inputRef}
        value={text}
        placeholderTextColor={colors.COLOR_PRIMARY}
        onFocus={() => setFocus(true)}
        onBlur={() => setFocus(false)}
        onChangeText={props.onChangeText ? props.onChangeText : (text) => setText(text)}
        style={[styles.inputText,{height : props.multiline ? 70 : 50,textAlignVertical:props.multiline ? 'top' : 'auto'}]}></TextInput>
    
  </View>
 
  );
});

CustomTextInput.defaultProps = {

    focus: () => {},
    style: {},
    placeholder: 'Enter',
    blurOnSubmit: false,
    returnKeyType: 'next',
    keyboardType: null,
    secureTextEntry: false,
    autoCapitalize: "none",
    textContentType: "none",
    defaultValue: '',
    editable: true,
    //maxLength:100,
    
};


const styles = StyleSheet.create({
    container: {
     flexDirection:'row',
     alignItems:'center',
     borderColor:colors.COLOR_PRIMARY,
     borderWidth:1,
     borderRadius:8,
     backgroundColor:colors.BLACK,
     margin:0,
     overflow:'hidden',
     paddingVertical:2,
     paddingHorizontal:5,
     marginHorizontal:0,
     marginVertical:10
  
    },
  
    inputText: {
        width:'90%',height:50,color:colors.GREY
    }
  });

export default CustomTextInput;
