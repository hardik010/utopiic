import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';

import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { postRequestNoBodyApi } from '../redux/actions/api_actions';
import config from '../config/config';
import Video from 'react-native-video';
import VideoPlayer from 'react-native-video-controls';
import HomeHeader from '../components/home_header';
import Orientation from 'react-native-orientation-locker';
import { Dimensions } from 'react-native';
import { Platform } from 'react-native';




const NativeVideo: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const apiReducer = useSelector(state => state.apiReducer)

    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false); 
    const [url,setUrl] = useState('')
    const [muted,setMuted] = useState(false)
    const [state, setState] = useState({
      fullscreen: false,
      play: false,
      currentTime: 0,
      duration: 0,
      showControls: true,
    });

    const videoPlayer = useRef()

    const [data,setData] = useState('')


    function fetchVideoData(id){
        setLoading(true)
        const VIMEO_ID = id;
        fetch(`https://player.vimeo.com/video/${VIMEO_ID}/config`)
              .then(res => res.json())
              .then(res => {
                setLoading(false)
                // console.log(res)
                // console.log(res.video.thumbs['640'])
                // console.log( res.request.files.hls.cdns[res.request.files.hls.default_cdn].url)
                setUrl( res.request.files.hls.cdns[res.request.files.hls.default_cdn].url)
                //videoPlayer.presentFullscreenPlayer()
        })
        .catch(e => {
            setLoading(false)
            showMessage("Try Again")
        })
        
    }

    function handleOrientation(orientation: string) {
      orientation === 'LANDSCAPE-LEFT' || orientation === 'LANDSCAPE-RIGHT'
        ? (setState(s => ({...s, fullscreen: true})), StatusBar.setHidden(true))
        : (setState(s => ({...s, fullscreen: false})),
          StatusBar.setHidden(false));
    }

    function handleFullscreen() {
      state.fullscreen
        ? Orientation.unlockAllOrientations()
        : Orientation.lockToLandscapeLeft();
    }

    

 

    useEffect(() => {
      let item =  props.route.params.item
      fetchVideoData(item.vimeo_id)
      setData(item)

      Orientation.addOrientationListener(handleOrientation);
       if(Platform.OS == 'android') handleFullscreen()

      const unsubscribe = props.navigation.addListener('blur', () => {
        Orientation.lockToPortrait();
       });
      
  
      return () => {
        // This would be inside componentWillUnmount()
        Orientation.removeOrientationListener(handleOrientation);
      };
    }, [props.navigation]);

   
   
 return (
    <>
       <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
      

              {/* <Video ref={videoPlayer} 
               source={{uri:url, type: 'm3u8'}} fullscreen resizeMode='cover'
               controls
               muted={muted}  
               style={state.fullscreen ? styles.fullscreenVideo : styles.video} />  */}

              <VideoPlayer ref={videoPlayer} 
               source={{uri:url, type: 'm3u8'}} fullscreen resizeMode='cover'
               seekColor={colors.COLOR_PRIMARY}
               muted={muted} 
               tapAnywhereToPause
               navigator={props.navigation} 
               style={state.fullscreen ? styles.fullscreenVideo : styles.video} /> 



             {/* <Video video={{uri:url, type: 'm3u8'}} 
                thumbnail={{uri:data.thumb}}
                videoHeight={dimensions.SCREEN_HEIGHT}
                videoWidth={dimensions.SCREEN_WIDTH}
                resizeMode='contain'
               controls style={styles.videoContainer} /> */}


                {/* <View style={styles.header}>
                <TouchableOpacity onPress={()=>props.navigation.pop()}>
                <FastImage 
                source={require('../assets/back.png')}
                style={styles.headerImage} 
                resizeMode={FastImage.resizeMode.cover}/>
                </TouchableOpacity>

                {
                  muted ?

                  <TouchableOpacity onPress={()=> setMuted(!muted)}>
                      <FastImage 
                      source={require('../assets/back.png')}
                      style={styles.headerImage} 
                      resizeMode={FastImage.resizeMode.cover}/>
                  </TouchableOpacity>
                  : 
                  <TouchableOpacity onPress={()=> setMuted(!muted)}>
                      <FastImage 
                      source={require('../assets/back.png')}
                      style={styles.headerImage} 
                      resizeMode={FastImage.resizeMode.cover}/>
                  </TouchableOpacity>
                }
                </View> */}


        {loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        flex:1
    },
   
    rowContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    videoContainer:{
        // position:'absolute',
        // top:0,
        // bottom:0,
        // left:0,
        // right:0,
        height:dimensions.SCREEN_HEIGHT * 0.95,
        width:dimensions.SCREEN_WIDTH,
        backgroundColor:'black'
    },
    video: {
      height: Dimensions.get('window').width * (9 / 16),
      width: Dimensions.get('window').width,
      backgroundColor: 'black',
    },
    fullscreenVideo: {
     position:'absolute',top:0,left:0,right:0,bottom:0,
      backgroundColor: 'black',
    },
    headerImage:{
      height:30,
      width:30
    },
    header:{
      backgroundColor:'rgba(0,0,0,0.0000001)',
      height:60,
      justifyContent:'space-between',
      alignItems:'center',
      width:dimensions.SCREEN_HEIGHT,
      flexDirection:'row',
      padding:10
    }
   

})


export default NativeVideo;


