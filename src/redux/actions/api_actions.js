import * as types from '../constants/index';

export const postRequestApi = (url,formdata) => dispatch => 
    new Promise((resolve ,reject) => {
        
        dispatch({type: types.LOADING, isLoading:true });
        fetch(url, {method: 'POST',  body: formdata})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });
})

export const postRequestNoBodyApi = (url) => dispatch => 
    new Promise((resolve ,reject) => {
        
        dispatch({type: types.LOADING, isLoading:true });
        fetch(url, {method: 'POST'})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });
})


export const postRequestApiWithToken = (url,formdata,token) => dispatch => 
    new Promise((resolve ,reject) => {
        dispatch({type: types.LOADING, isLoading:true });
        fetch(url, {method: 'POST',
            body:formdata,
            headers: {  'token': token }
        })
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });
})

export const postRequestNoBodyApiWithToken = (url,token) => dispatch => 
    new Promise((resolve ,reject) => {
        
        dispatch({type: types.LOADING, isLoading:true });
        fetch(url, {method: 'POST',headers: { 'Content-Type': 'application/json', 'token': token }})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });
})

export const getRequestApi = () => dispatch => 
    new Promise((resolve ,reject) => {
     
        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.home, {method: 'GET',  body: formData})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });
})