import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';

import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList } from 'react-native-gesture-handler';
import config from '../config/config';
import { postRequestApi, postRequestApiWithToken, postRequestNoBodyApi, postRequestNoBodyApiWithToken } from '../redux/actions/api_actions';
import { Chip } from 'react-native-paper';
var RNFS = require('react-native-fs');
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import { PermissionsAndroid } from 'react-native';


const htmlTemplate = `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <html xmlns="http://www.w3.org/1999/xhtml">
 <head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <title>Receipt Document</title>
 
 <style>
 table
 {
   border: 1px solid #37399e;
     margin: 0 auto;
     padding: 0px;
 }
 td
 {
     padding: 5px; 
 }
 
 </style>
 </head>
  <?php $point=".00"?>
 <body>
  
 <table width="100%">
   <tr>
     <td>
         
        <div style="text-align: right;">
        
            <img src="https://webmobril.org/dev/goodwash/homeimage/41546218.png" style="width:20%">
         </div>
      </td>
   </tr>
 </table>
   <table width="100%" border="1">
   <tr>
    
   
  
   
   <tr>
   </tr>
      <tr>
     <td colspan="4" style="font-size: 15px;color: #0e0e0d;font-weight: 600;"><p>Declaration:</p>
     I herewith declare that I have read the terms and conditions of eGoodWash.com and agree to adhere to them. I am authorized to make this booking on behalf of me or family and understand that I am liable for payment of this order as per the terms. I further understand that the order will only be processed after the payment is made.



     eGoodWash.com
     
     (Managed by GOODWILL SUCCESS SOLUTIONS SDN BHD)
     
     No 12, Jalan Permatang Dua 26/25B Bukit Saga, Seksyen 26 40000 Shah Alam Selangor.
     
     Tel: 603-5103 3222, Mobile: MAT LEE (6018 389 0073) 
   </td>
    
   </tr>
    
 </table>
 </body>
 </html>`

const PaymentHistory: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const apiReducer = useSelector(state => state.apiReducer)

    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false); 
    const [historyList, setHistoryList] = useState([]);
    const [selectedItem, setSelectedItem] = useState(null);



  async function saveDataIOS(name){
    /**
     * You should be able to enable it by updating your Info.plist. You need to add two keys:

      UIFileSharingEnabled and LSSupportsOpeningDocumentsInPlace should both be added and set to YES.

      UIFileSharingEnabled: Application supports iTunes file sharing
      LSSupportsOpeningDocumentsInPlace: Supports opening documents in place

     */
    showMessage(("PDF file saved succcessfully " + name),false );
  }

  async function moveAttachment(path,name) {

    let filePathDir = `${RNFS.DocumentDirectoryPath}/Utopiic`;
    let filePath = `${$filePathDir }/${name}`;
    let customPromise =  new Promise((resolve, reject) => {
        RNFS.mkdir(filePathDir)
        .then(() => {
            RNFS.moveFile(path, filePath )
            .then(() => resolve())
            .catch(error => reject(error));
        })
        .catch(err => reject(err));
    });
    customPromise.then(() => {
      showMessage(("PDF file saved succcessfully " + name),false );
    })
    .catch(error => showMessage(error.message))
}

  async function saveData(from){
    try {
      const granted = await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      ]);
    } catch (err) {
      showMessage("Write permissions have not been granted,go to settings to enable it." );
    }
    const readGranted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE); 
    const writeGranted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
    if(!readGranted || !writeGranted) {
      console.log('Read and write permissions have not been granted');
      showMessage("Read and write permissions have not been granted" );
      return;
    }
    let r = Math.random().toString(36).substring(7);
    let name = 'egoodwash_booking_id' + selectedItem.booking_id
   
    var path = `${RNFS.ExternalStorageDirectoryPath}/Utopiic`;
    RNFS.mkdir(path);
    path += '/'+ name+ '.pdf';
    RNFS.moveFile(from, path)
      .then((success) => {
        // console.log('Success',path);
        showMessage(("Invoice file saved succcessfully " + name),false );
      })
      .catch((err) => {
        console.log(err.message);
        showMessage(err.message)
      });
  }


  async function createPDF(){
    let r = Math.random().toString(36).substring(7);
    let name = 'utopiic_booking_id_' + selectedItem.booking_id
   
    let options = {
      html:htmlTemplate,
      fileName: name,
     directory: 'Documents',
    };

    

   if(Platform.OS == 'android'){
    try {
      const granted = await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      ]);
    } catch (err) {
      showMessage("Write permissions have not been granted,go to settings to enable it." );
    }
    const readGranted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE); 
    const writeGranted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
    if(!readGranted || !writeGranted) {
      showMessage("Read and write permissions have not been granted" );
      return;
    }
   }

    let file = await RNHTMLtoPDF.convert(options)
   // console.log("Pdf stored location------->>", file.filePath);
    Platform.OS == 'android' ? 
    saveData(file.filePath)
   : saveDataIOS(name)
  // : moveAttachment(file.filePath,name)

    
  }

    function fetchHistory(){
      const URL = config.BASE_URL + config.SUBSCRIPTION_PAYMENT_LIST
      
      var formdata = new FormData()
      formdata.append('user_id',userReducer.user.user_id)
      
      dispatch(postRequestApi(URL,formdata))
      .then(response=> {
          //console.log('Response - ',JSON.stringify(response))
          if(response.status){
             setHistoryList(response.data)
          }
          else showMessage(response.message)
      })
      .catch(error => {
           showMessage(error.message)
           //console.log('Error - ',error)
      })
  }

    useEffect(() =>{
      const unsubscribe = props.navigation.addListener('focus', () => {
        // Screen was focused
        // Do something
        fetchHistory()
     
      });
      //console.log('Do something after selectedItem has changed', selectedItem);
      /** so used selected item in dependecny array in below line */
        
    },[props.navigation,selectedItem])

   function  onPress  (item) {
       setSelectedItem(item)
       createPDF()
   }

   function isValid(){
     if(!selectedIndex) showMessage('Select Plan First')
     else props.navigation.navigate('payment_method',{'plan':plansList[selectedIndex]})
   }

  
      function renderItem(item,index)  {
        return (
          <View style={styles.listContainer}>

            <View style={[styles.rowDirection]}>
                <View style={styles.rowDirection}>
                    <Text style={styles.textStyleMain}>Request Type : </Text>
                    <Text style={styles.textStyleValue}>{item.Type == "Package" ? `Package (${item.detail.name.substring(0,15)})` : 'Coupon'} </Text>
                </View>
            </View>

            <View style={{height:12}}/>
            <View style={[styles.rowDirection]}>
              <View style={styles.rowDirection}>
                      <Text style={styles.textStyleMain}>Booking Id : </Text>
                      <Text style={styles.textStyleValue}>{item.booking_id} </Text>
             </View>

             
              {
                item.Type == "Package" ?
                <View style={styles.rowDirection}>
                      <Text style={styles.textStyleMain}>Price : </Text>
                      <Text style={styles.textStyleValue}>{item.final_price}  <Text style={[styles.textStyleValue,{textTransform:'uppercase'}]}>{item.currency} </Text></Text>
               </View>
               : null
              }
            </View>


            <View style={{height:12}}/>
              <View style={[styles.rowDirection,{justifyContent:'flex-start'}]}>
                      <Text style={styles.textStyleMain}>Date : </Text>
                      <Text style={styles.textStyleValue}>{item.date}</Text>
             </View>  

            <View style={{height:12}}/>
            {
                 item.Type == "Package" ?
                  <View style={[styles.rowDirection,{justifyContent:'flex-start'}]}>
                      <Text style={styles.textStyleMain}>Package Id : </Text>
                      <Text style={styles.textStyleValue}>{item.detail.package_id}</Text>
                  </View>  
                 : null
            }

            <View style={{height:12}}/>
            {
                 ( item.Type == "Package" && item.detail.package_type == 'Lifestyle' )  ?
                  <View style={[styles.rowDirection,{justifyContent:'flex-start'}]}>
                      <Text style={styles.textStyleMain}>Tickets : </Text>
                      <Text style={styles.textStyleValue}>{item.booking_person}</Text>
                  </View>  
                 : null
           }
           <View style={{height:12}}/>
            {
                  ( item.Type == "Package" && item.detail.package_type == 'Travel' )  ?
                  <View style={[styles.rowDirection,{justifyContent:'flex-start'}]}>
                      <Text style={styles.textStyleMain}>Rooms : </Text>
                      <Text style={styles.textStyleValue}>{item.rooms}</Text>
                  </View>  
                 : null
            }

          <View style={{height:12}}/>
            {
                  ( item.Type == "Package" && item.detail.package_type == 'Travel' )  ?
                  <View style={[styles.rowDirection,{justifyContent:'flex-start'}]}>
                      <Text style={styles.textStyleMain}>Members: </Text>
                      <Text style={styles.textStyleValue}>{item.booking_person}</Text>
                  </View>  
                 : null
            }

            <View style={styles.listDivider}/>
            <View style={{height:12}}/>
            <View style={[styles.rowDirection,{width:'100%'}]}>
            <Chip textStyle={{color:'white'}}
             style={{backgroundColor:'green'}}
            onPress={() => {}}>{item.status}</Chip>

            {
                  <View style={{width:30}}/>
                }


            {/** 
              <Chip textStyle={{color:'black'}}
                style={{backgroundColor:colors.COLOR_PRIMARY}}
              onPress={() => onPress(item)}>Invoice</Chip>
              */
            }

            </View>

          </View>
        );
    }


 return (
    <>
     <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
       <BackHeader {...props}/>
        <InputScrollView 
        bounces={false}
        useAnimatedScrollView={true}
        contentContainerStyle={styles.scrollContainer}>

        <View style={{height:10}}/>
          <Text style={styles.heading}>PAYMENT HISTORY</Text>
           <View style={{height:20}}/>       

               <FlatList 
                contentContainerStyle={{marginBottom:30}}
                showsVerticalScrollIndicator={false}
                scrollEnabled={false}
                //ItemSeparatorComponent={itemSeparator}
                data={historyList}
                renderItem={({ item ,index}) => renderItem(item,index)}
                keyExtractor={(item) => item.id } />         

             <View style={{height:30}}/>         

      </InputScrollView>

      { apiReducer.loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,

      },
      heading:{
        color:colors.COLOR_PRIMARY,
        lineHeight:30,
        fontSize:19,
        fontWeight:'300',
        alignSelf:'center',
        textAlign:'center',
        maxWidth:'70%'
    },
    
    seperator:{
      height: .5,
      width: dimensions.SCREEN_WIDTH,
      backgroundColor: colors.COLOR_PRIMARY,
    },
    paymentContainer:{
        width:dimensions.SCREEN_WIDTH * 0.9,
        borderRadius:10,
        borderColor:colors.COLOR_PRIMARY,
        borderWidth:0.7,
        padding:9,
        height:dimensions.SCREEN_HEIGHT * 0.2,
        margin:7,
        justifyContent:'space-between'
    },
    rowContainer:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        marginBottom:15
    },
    textStyle:{
        color:colors.COLOR_PRIMARY,
        textTransform:'capitalize'
    },
    //
    listContainer:{
      width:dimensions.SCREEN_WIDTH * 0.9,
      borderRadius:10,
      borderColor:colors.COLOR_PRIMARY,
      borderWidth:0.7,
      padding:10,
      margin:7,
  },
   rowDirection:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between',
   },
   textStyleValue:{
    color:colors.COLOR_PRIMARY,
    textTransform:'capitalize'
   },
   textStyleMain:{
    color:colors.WHITE,
    fontSize:16,
    fontWeight:'700',
    textTransform:'uppercase'
   },
   listDivider:{
     width:'100%',
     height:0.5,
     backgroundColor:'grey',
     marginVertical:7

   }
})


export default PaymentHistory;


