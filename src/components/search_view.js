import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TextInput
} from 'react-native';

import FastImage from 'react-native-fast-image'
import { TouchableOpacity } from 'react-native-gesture-handler';

import {colors,urls,dimensions} from '../utils/constants';


const SearchView = React.forwardRef((props,ref) =>  {

  return (
    <View style={[styles.container, props.style]}>

            <View
            style={[styles.inputText]} >
                <Text style={{color:colors.COLOR_PRIMARY}}>{props.label}</Text>
            </View>

            <Image source ={require('../assets/search.png')}
            style={styles.imageStyle}
            resizeMode={'contain'} />
  </View>
 
  );
});

SearchView.defaultProps = {

    focus: () => {},
    onSearch: () => {},
    style: {},
    label: 'Search ...',
    blurOnSubmit: false,
    returnKeyType: 'next',
    keyboardType: null,
    secureTextEntry: false,
    autoCapitalize: "none",
    textContentType: "none",
    defaultValue: '',
    editable: true,
    maxLength:40,
};


const styles = StyleSheet.create({
    container: {
     flexDirection:'row',
     alignItems:'center',
     borderColor:colors.COLOR_PRIMARY,
     borderWidth:1,
     backgroundColor:colors.BLACK,
     margin:0,
     overflow:'hidden',
     paddingHorizontal:7,
     paddingVertical:3,
     marginHorizontal:0,
     marginVertical:10,
     borderRadius:20
  
    },
  
    inputText: {
      textAlign:'left',
      fontSize: 16,
      flex:8.5,
      color:colors.DARK_GREY,
      marginHorizontal:3,
      height:40,
      justifyContent:'center'
    },
    imageStyle:{
        flex:1.5,
        height:25,
        width:25,justifyContent:'center',alignItems:'center',
      },
  });

export default SearchView;
