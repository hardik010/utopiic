import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,FlatList
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';

import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import {  TouchableOpacity } from 'react-native-gesture-handler';
import { postRequestNoBodyApi, postRequestNoBodyApiWithToken } from '../redux/actions/api_actions';
import config from '../config/config';
import SearchInput from '../components/search_input';
import HomeHeader from '../components/home_header';
import { Keyboard } from 'react-native';

const Card = (props) =>{
    return(
        <View style={[styles.cardContainer,styles.cardBackground]}>
                <FastImage resizeMode='cover'
                 source ={{uri:props.image}} style={{
                    position:'absolute',
                    top:0,left:0,
                    right:0,left:0,height:'100%',width:'100%',aspectRatio:3/2
                }}/>
                <View style={styles.nameContainer}>
                    <Text style={styles.cardTitle}>{props.name}</Text>
                </View>
        </View>
    )
   }



const Natives: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const apiReducer = useSelector(state => state.apiReducer)

    const dispatch = useDispatch()
    const searchInput = useRef();
    const searchRef = useRef();

    const [loading, setLoading] = useState(false); 
    const [natives, setNatives] = useState([]); 
    const [nativesGlobal, setNativesGlobal] = useState([]); 

    function fetchNatives(){
        const URL = config.BASE_URL + config.GET_NATIVES
        dispatch(postRequestNoBodyApiWithToken(URL,userReducer.user.token))
        .then(response=> {
          //  console.log('Response - ',JSON.stringify(response))
            if(response.status){
                setNatives(response.data)
                setNativesGlobal(response.data)
            }
            else showMessage(response.message)
        })
        .catch(error => {
            showMessage('Try Again.')
            //console.log('Error - ',error)
        })
    }

    useEffect(() =>{
        fetchNatives()
    },[])

    function navigateToDetails(item){
        props.navigation.navigate('native_details',{'item':item})
    }

    function renderItem (item,index)  {
        return(
            <TouchableOpacity onPress={() => navigateToDetails(item)}>
                <Card image = { item.profile}
                name={item.company_name} subName ={item.description.length > 50 ? (item.description.substring(0,50) + '...') :item.description}/>
            </TouchableOpacity>
        )
      }


   
  
    function _onSearch (){
        Keyboard.dismiss()
        var searchText = searchInput.current.getInputValue()
        //console.log(searchText)
    
        var results = [];    
        var name = ''
        for(var i = 0; i < nativesGlobal.length  ; i++) {
             name = nativesGlobal[i].company_name.toLowerCase()
            if(name  != null ){
                 var check = name.indexOf(searchText.toLowerCase()) > -1
                  if(check) {
                     results.push(nativesGlobal[i]);
                  }
            }
        }
        //console.log(results)
    
        if(results.length == 0){
         showMessage('No Natives Found')
        }
        setNatives(results)
        
      }

   
   
 return (
    <>
       <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
      <HomeHeader  {...props}/>
      <ScrollView 
        bounces={false}
        useAnimatedScrollView={true}
        contentContainerStyle={styles.scrollContainer}>
        <View style={{height:10}}/>
        {/* <Text style={styles.heading}>NATIVES</Text>   */}
        <FastImage 
        source={require('../assets/cn.png')}
        style={{height:80,width:150,alignSelf:'center'}} 
        resizeMode={FastImage.resizeMode.contain}/>
        
        <SearchInput
         placeholder={'Search Natives..'}
         onSubmitEditing={()=> _onSearch()}
         ref={searchInput}
         inputRef={ searchRef }
         style={{width :'60%',alignSelf:'center'}}/>
        <View style={{height:20}}/>


            <FlatList
                style={{marginVertical:10}}
                horizontal={false}
                numColumns={1}
                data={natives}
              showsHorizontalScrollIndicator={false}
              renderItem={({ item ,index}) => renderItem(item,index)}
              keyExtractor={item => item.id}
              maxToRenderPerBatch={3}
              windowSize={2}           
              updateCellsBatchingPeriod={3}   
              initialNumToRender={6}
              onEndReachedThreshold={1}
              extraData={natives}
          />

      </ScrollView>
      {apiReducer.loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        justifyContent:'center',
        flexGrow:1
    },
   
    rowContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    cardContainer:{
        width:dimensions.SCREEN_WIDTH,
        height:dimensions.SCREEN_HEIGHT * 0.31,
        marginVertical:7,
        backgroundColor:'black'
     },
     nameContainer:{
         marginBottom:20,
         alignItems:'center',
         padding:10,
         backgroundColor:'rgba(0,0,0,0.3)',
        width:'100%'
    },
     cardBackground:{
        justifyContent:'flex-end',
        alignItems:'center',
        backgroundColor:colors.COLOR_PRIMARY
     },
    cardTitle:{
            color:'white',
            fontSize:17,
            fontWeight:'800',
            textTransform:'uppercase',
            lineHeight:30,textAlign:'center',width:'85%',alignSelf:'center'
        },
    cardSubTitle:{
        color:'white',
        lineHeight:28,
        textTransform:'uppercase'
    },
   

})


export default Natives;


