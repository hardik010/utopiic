import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TouchableOpacity, Keyboard
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'
import { showMessage } from '../utils/snackmsg';
import {colors,urls,dimensions} from '../utils/constants';

import {useSelector, useDispatch} from 'react-redux'
import ProgressBar from '../components/progress_bar';
import ImageInput from '../components/image_input';
import CustomButton from '../components/custom_button';
import InputScrollView from 'react-native-input-scroll-view';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import config from '../config/config';
import { addUser } from '../redux/actions/user_actions';
import { postRequestApi } from '../redux/actions/api_actions';
import { getDeviceToken } from '../utils/firebase_helpers';


const Login: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const apiReducer = useSelector(state => state.apiReducer)

    const dispatch = useDispatch()
    const [checked, setChecked] = useState(false);
    const [deviceToken, setDeviceToken] = useState('');


    // In order to gain access to the child component instance,
    // you need to assign it to a `ref`, so we call `useRef()` to get one
    const nameInput = useRef();
    const nameRef = useRef();
  


  
  function isValid(){
    var result = false
    let name = nameInput.current.getInputValue()
   
    Keyboard.dismiss()
    if(name.length == 0) showMessage("Enter Username")
    else result = true
  
    return result
  }
  
  async function _onLogin()  {
    if(isValid()){
    
    
      let name = nameInput.current.getInputValue()
     
      var formdata = new FormData()
      formdata.append('passcode',name)
      formdata.append('device_id',deviceToken)
      formdata.append('type',Platform.OS == 'android' ? 1 : 2)
   
  
      const URL = config.BASE_URL + config.LOGIN
          
      dispatch(postRequestApi(URL,formdata))
      .then(response=> {
           //console.log('Response - ',JSON.stringify(response))
          if(response.status) {
            let profile={}
            profile.token = response.data.token;
            profile.user_type = response.data.user_type;
            profile.user_id = response.data.user_id;
            profile.name = response.data.name;
            profile.passcode = name.toUpperCase();
            profile.phone = response.data.phone;
            profile.address = response.data.address;
            profile.email = response.data.email;
            profile.age = response.data.age;
            // //console.log('Response - ',JSON.stringify(profile))
            dispatch(addUser(profile))

            //props.navigation.navigate('interest',{profile:profile})
          }
          else showMessage(response.message)
      })
      .catch(error => {
          showMessage(error.message)
          //console.log('Error - ',error)
      })
     
    }
     
  }
 function _onSignUp(){
    props.navigation.navigate('signup')
    //props.navigation.navigate('account')
 }
  function _onForgot(){
    props.navigation.navigate('otp')
  }

  function sendToken(token){
    const URL = config.BASE_URL + config.START_TOKEN
    var formdata = new FormData()
    formdata.append('device_token',token)
    dispatch(postRequestApi(URL,formdata))
    .then(response=> {
         console.log('Response - ',JSON.stringify(response))
    })
    .catch(error => {
        showMessage(error.message)
        //console.log('Error - ',error)
    })
  }

  useEffect(()=>{
    getDeviceToken()
    .then(value => {
      console.log("Value is -> ",value)
      setDeviceToken(value)
      sendToken(value)
    })
    .catch(e => showMessage(e.message))
  },[])

  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
        <KeyboardAwareScrollView
        bounces={false}
        contentContainerStyle={styles.scrollContainer}>
          <View style={{height:0}}/>
          <Text style={styles.heading}>Welcome Aboard !</Text>
          <View style={{height:30}}/>
          <ImageInput
            placeholder={'User name'}
            onSubmitEditing={()=> _onLogin()}
            ref={nameInput}
            inputRef={ nameRef }
            autoCapitalize='characters'
            inputImage={require('../assets/account.png')}
            style={{width:'90%'}}
            returnKeyType="next"
          />
          

        <CustomButton 
        style={{width :dimensions.SCREEN_WIDTH * 0.8,marginTop:75,marginBottom:19}}
        handler={_onLogin}
        label ={'Sign In'}/>

        <Text style={{color:colors.COLOR_PRIMARY,alignSelf:'center'}}>or</Text>

        <CustomButton 
        style={{width :dimensions.SCREEN_WIDTH * 0.8,marginTop:25,marginBottom:0}}
        handler={_onSignUp}
        label ={'Sign Up'}/>

        <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between',width:'90%'}}>
          <View style={{flexDirection:'row',alignItems:'center'}}>
          <CheckBox
              checkedColor={colors.COLOR_PRIMARY}
              checked={checked}
              onPress={() => setChecked(!checked)}
          />
          <Text style={{color:colors.COLOR_PRIMARY,fontWeight:'500',fontSize:14,marginLeft:-10}}>Remember Me </Text>
        </View>

        <TouchableOpacity onPress={()=> _onForgot()}>
           <Text 
         style={styles.forgotText}>Forgot Password?</Text>
       </TouchableOpacity>

        </View>

      </KeyboardAwareScrollView>

      {apiReducer.loading && <ProgressBar/>}

      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor:'black'
    },
    scrollContainer:{
        padding:10,
        justifyContent:'center',
        flexGrow:1,
        alignItems:'center',
      },
    heading:{
        color:colors.COLOR_PRIMARY,
        lineHeight:35,
        fontSize:20,
        fontWeight:'700',
        alignSelf:'center'
    },
    forgotText:{
        color:colors.COLOR_PRIMARY,
        fontSize:14
    }
   

})

export default Login;
