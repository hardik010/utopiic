import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,ImageBackground, Keyboard
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';

import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { postRequestApiWithToken, postRequestNoBodyApi, postRequestNoBodyApiWithToken } from '../redux/actions/api_actions';
import config from '../config/config';
import HomeHeader from '../components/home_header';
import SearchInput from '../components/search_input';
import { color } from 'react-native-reanimated';

const Card = (props) =>{
    return(
        <View style={styles.cardContainer}>
            <ImageBackground style={styles.cardBackground}
            source ={{uri:props.image}}>
                <View style={styles.nameContainer}>
                    <Text style={styles.cardTitle}>{props.name}</Text>
                    <Text style={{color:'white'}}>{props.subName}</Text>
                </View>
            </ImageBackground>
        </View>
    )
   }



const FoodBrands: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const apiReducer = useSelector(state => state.apiReducer)

    const dispatch = useDispatch()

    const [brands, setBrands] = useState([]); 
    const [experiencesCategory, setExperiencesCategory] = useState([]); 

    const searchInput = useRef();
    const searchRef = useRef();

    function fetchBrands(){
        const URL = config.BASE_URL + config.GET_SUPPLIERS
        dispatch(postRequestNoBodyApiWithToken(URL,userReducer.user.token))
        .then(response=> {
            //console.log('Response - ',JSON.stringify(response))
            if(response.status) {
                let temp = []
                response.data.Suppliers.map(e => {
                    if(e.industry_category == 'Food') temp.push(e)
                })
                setBrands(temp)
            }
            else showMessage(response.message)
        })
        .catch(error => {
             showMessage(error.message)
             //console.log('Error - ',error)
        })
    }


    useEffect(() =>{
        fetchBrands()
    },[])

    function navigateToDetails(item){
        props.navigation.navigate('brand_details',{'item':item})
    }

    function renderItem (item,index)  {
        return(
            <TouchableOpacity onPress={() => navigateToDetails(item)}>
                <Card image = {config.BASE_URL_MEDIA + item.brand_logo}
                name={item.name} subName ={item.country}/>
            </TouchableOpacity>
        )
      }

   
 return (
    <>
       <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
      <HomeHeader haveBack {...props}/>
      <ScrollView 
        bounces={false}
        useAnimatedScrollView={true}
        contentContainerStyle={styles.scrollContainer}>
        <View style={{height:10}}/>
        <Text style={styles.heading}>FOOD</Text>  
        
       

         <FlatList
             style={{marginVertical:10}}
             horizontal={false}
             numColumns={1}
             data={brands}
              showsHorizontalScrollIndicator={false}
              renderItem={({ item ,index}) => renderItem(item,index)}
              keyExtractor={item => item.id}
          />

      </ScrollView>
      {apiReducer.loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
       
    },
   
    rowContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
   
    heading:{
        color:colors.COLOR_PRIMARY,
        lineHeight:26,
        fontSize:20,
        fontWeight:'700',
        alignSelf:'center',
        maxWidth:'90%',
        marginVertical:12
    },
    categoryContainer:{
        width:'auto',
        paddingHorizontal:15,
        paddingVertical:8,
        backgroundColor:colors.LIGHT_BLACK,
        borderRadius:20,
        borderWidth:1,
        borderColor:colors.COLOR_PRIMARY,
        marginHorizontal:5,
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'row'
    
      },
      cardContainer:{
        width:dimensions.SCREEN_WIDTH,
        height:dimensions.SCREEN_HEIGHT * 0.31,
        marginVertical:7,
        backgroundColor:colors.COLOR_PRIMARY
     },
     nameContainer:{
         marginBottom:20,
         alignItems:'center',
         padding:10,
         backgroundColor:'rgba(0,0,0,0.3)',
        width:'100%'
    },
     cardBackground:{
        height:'100%',
        width:'100%',
        justifyContent:'flex-end',
        alignItems:'center',
        backgroundColor:colors.COLOR_PRIMARY
     },
    cardTitle:{
            color:'white',
            fontSize:17,
            fontWeight:'800',
            textTransform:'uppercase',
            lineHeight:30,
            textAlign:'center'
        },
    cardSubTitle:{
        color:'white',
        lineHeight:28,
        textTransform:'uppercase'
    },
   

})


export default FoodBrands;


