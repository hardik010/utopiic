import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';

import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { postRequestApiWithToken, postRequestNoBodyApi } from '../redux/actions/api_actions';
import config from '../config/config';
import RazorpayCheckout from 'react-native-razorpay';
import { getDeviceToken } from '../utils/firebase_helpers';
import { sendNotificationToUser } from '../utils/send_notification';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { color } from 'react-native-reanimated';


const Quantity = React.forwardRef((props,ref) =>  {
    
    const [quantity, setQuantity] = useState(1); 
   

    useImperativeHandle(ref, () => ({
        getValue (){
            return quantity
        }
    }));

    function onPlus(){
        props.onChange(quantity + 1)
        setQuantity(quantity + 1)
    }

    function onMinus(){
        if(quantity > 1){
            props.onChange(quantity - 1)
            setQuantity(quantity - 1)
        }
    }



    return(
        <View style={styles.rowContainer}>
        <TouchableOpacity onPress={()=> onPlus()}>
        <FastImage 
            source={require('../assets/plus.png')}
            style={styles.imageStyle} 
            resizeMode={FastImage.resizeMode.contain}/>
        </TouchableOpacity>

            <Text style={styles.quantityText}>{quantity}</Text>

        <TouchableOpacity onPress={()=> onMinus()}>
        <FastImage 
            source={require('../assets/minus.png')}
            style={styles.imageStyle} 
            resizeMode={FastImage.resizeMode.contain}/>
        </TouchableOpacity>


    </View>
    )
   

});

const JoinExperience: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const apiReducer = useSelector(state => state.apiReducer)

    const dispatch = useDispatch()

    const [quantity, setQuantity] = useState(1); 
    const [roomQuantity, setRoomQuantity] = useState(1); 
    const [peopleQuantity, setPeopleQuantity] = useState(1); 
    const [details, setDetails] = useState(''); 

    const [isFromDatePickerVisible, setFromDatePickerVisibility] = useState(false);
    const [isToDatePickerVisible, setToDatePickerVisibility] = useState(false);


    const [fromDate, setFromDate] = useState('');
    const [toDate, setToDate] = useState('');


   const  sendNotification = async() => {
        const notification = {
          "notification": {
              "title": 'Experience Booking',
              "body": 'Thank you for  booking experience ' + details.name.toUpperCase(),
              "sound": "default",
           
          },
          "priority": "high",
          //"action" : "experiences"
         }
          let token = await getDeviceToken()
          //console.log(token)
          sendNotificationToUser(token, notification);
       
    }

    useEffect(() =>{
        setDetails(props.route.params.details)
        /** 
        details.package_category_id == 1 , means Instant payment else  2 means package Request
        details.package_category = Instant/Request
        details.package_type= Lifestyle/Travle
        */
       //console.log(props.route.params.details)
    },[])

    function onPlus(){
        setQuantity(quantity + 1)
    }

    function onMinus(){
        if(quantity > 1) setQuantity(quantity - 1)
    }

    function getTodayDate(){
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        var today = dd + '-' + mm + '-' + yyyy;
        return today
    }

 function _bookLifestyleExperience(txn_id='',type=1){
        const URL = config.BASE_URL + config.PACKAGE_BOOKING

        var formdata = new FormData()
        formdata.append('user_id',userReducer.user.user_id)
        formdata.append('publish_price', parseInt(details.price) * quantity)
        formdata.append('razorpay_txn_id',txn_id)
        formdata.append('package_id',details.package_id)
        formdata.append('razorpay_txn_status','success')
        formdata.append('razorpay_txn_date',getTodayDate())
        formdata.append('package_category_id',type)//1 means instant 2 means request
        formdata.append('booking_qty',quantity)
      
        dispatch(postRequestApiWithToken(URL,formdata,userReducer.user.token))
        .then(response=> {
            //console.log('Response - ',JSON.stringify(response))
            if(response.status){
              props.navigation.navigate('home')
             // sendNotification()
             type == 2 ? showMessage("Request Generated Successfully !!",false)
           : showMessage("Booked Successfully !!",false)
            }
        })
        .catch(error => {
           showMessage(error.message)
            //console.log('Error - ',error)
        })
 }

 function _bookTravelExperience(txn_id ='',type=1){
    const URL = config.BASE_URL + config.TRAVEL_PACKAGE_BOOKING
    //const URL = config.BASE_URL + config.PACKAGE_BOOKING

    var formdata = new FormData()
    formdata.append('user_id',userReducer.user.user_id)
    formdata.append('package_id',details.package_id)
    formdata.append('from_date', fromDate)
    formdata.append('to_date', toDate)
    formdata.append('no_of_people', peopleQuantity)
    formdata.append('room', roomQuantity)
    formdata.append('price', parseInt(details.price))
    formdata.append('razorpay_txn_id',txn_id)
    formdata.append('razorpay_txn_status','success')
    formdata.append('razorpay_txn_date',getTodayDate())
    formdata.append('package_category_id',type)//1 means instant 2 means request
    //console.log('formdata - ',JSON.stringify(formdata))
    dispatch(postRequestApiWithToken(URL,formdata,userReducer.user.token))
    .then(response=> {
   // console.log('Response - ',JSON.stringify(response))
        if(response.status){
          props.navigation.navigate('home')
         // sendNotification()
          type == 2 ? showMessage("Request Generated Successfully !!",false)
           : showMessage("Booked Successfully !!",false)
        }
    })
    .catch(error => {
       showMessage(error.message)
        //console.log('Error - ',error)
    })
}




    function _razorPayLifestyle(){

        var image ='https://static.wixstatic.com/media/20b4ce_d696dfd13905444d8ca2f37a5e9fe91d~mv2.png/v1/fill/w_344,h_100,al_c,q_85,usm_0.66_1.00_0.01/Asset%2024.webp'
        var options = {
            description: 'Utopiic - A travel Concierge',
            image: image,
            currency: 'INR',
            key:'rzp_live_QXWEW7zcLOxpvI',
            amount: parseInt(details.price) * quantity * 100 ,
           // key: 'rzp_test_bkOHPTNmXd5abE',
             //amount:'100',
            name: 'Utopiic',
            //"method": "card",
            prefill: {
              email:userReducer.user.email,
              contact:userReducer.user.phone,
              name:userReducer.user.name,
            },
            theme: {color: '#000000'}
          }
  
  
          RazorpayCheckout.open(options).then((data) => {
            // handle success
            //console.log('successs --- ',JSON.stringify(data))
            // showMessage("Payment Successful",false)
            // alert(`Success: ${data.razorpay_payment_id}`);
                // alert(`Success: ${JSON.stringify(data)}`);
                _bookLifestyleExperience(data.razorpay_payment_id)
           
            
          }).catch((error) => {
            // handle failure
            //console.log(error)
           // alert(`Error: ${error.code} | ${error.description}`);
  
            showMessage(error.description)
  
          });
    }



    function _razorPayTravel(){

        var image ='https://static.wixstatic.com/media/20b4ce_d696dfd13905444d8ca2f37a5e9fe91d~mv2.png/v1/fill/w_344,h_100,al_c,q_85,usm_0.66_1.00_0.01/Asset%2024.webp'
        var options = {
            description: 'Utopiic - A travel Concierge',
            image: image,
            currency: 'INR',
             key:'rzp_live_QXWEW7zcLOxpvI',
            amount: parseInt(details.price) * quantity * 100 ,
            //key: 'rzp_test_bkOHPTNmXd5abE',
           //  amount:'100',
            name: 'Utopiic',
            //"method": "card",
            prefill: {
              email:userReducer.user.email,
              contact:userReducer.user.phone,
              name:userReducer.user.name,
            },
            theme: {color: '#000000'}
          }
  
  
          RazorpayCheckout.open(options).then((data) => {
            // handle success
            //console.log('successs --- ',JSON.stringify(data))
             //showMessage("Payment Successful",false)
            // alert(`Success: ${data.razorpay_payment_id}`);
                // alert(`Success: ${JSON.stringify(data)}`);
                _bookTravelExperience(data.razorpay_payment_id)
           
            
          }).catch((error) => {
            // handle failure
            //console.log(error)
           // alert(`Error: ${error.code} | ${error.description}`);
  
            showMessage(error.description)
  
          });
    }




    function _onDone(){
         if(parseInt(details.price) > 0){
             if(details.package_category_id == 2)  _bookLifestyleExperience('',2)
             else if(details.package_category_id == 1)  _razorPayLifestyle()
         }
         //if(parseInt(details.price) > 0) sendNotification()
         else showMessage("Price should be greater than one.")
    }

    function _onTravelDone(){
        if(!fromDate) showMessage("Choose From Date")
        else if(!toDate) showMessage("Choose To Date")
        else  if(parseInt(details.price) > 0){
            if(details.package_category_id == 2)  _bookTravelExperience('',2)
            else if(details.package_category_id == 1)  _razorPayTravel()
        }
        else showMessage("Price should be greater than one.")
   }



   function  convertDate(date) {
    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth()+1).toString();
    var dd  = date.getDate().toString();

    var mmChars = mm.split('');
    var ddChars = dd.split('');

    return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
  }



  const showFromDatePicker = () => {
    setFromDatePickerVisibility(true);
  };
 
  const hideFromDatePicker = () => {
    setFromDatePickerVisibility(false);
  };
 
  const handleFromDateConfirm = (date) => {
    setFromDate(convertDate(date))
    hideFromDatePicker();
  };


  const showToDatePicker = () => {
    setToDatePickerVisibility(true);
  };
 
  const hideToDatePicker = () => {
    setToDatePickerVisibility(false);
  };
 
  const handleToDateConfirm = (date) => {
    setToDate(convertDate(date))
    hideToDatePicker();
  };




const roomQuantityInput = useRef();
const peopleQuantityInput = useRef();
const ticketQuantityInput = useRef();


function lifestyleExperience(){
        return(
            <>
            <ScrollView 
            bounces={false}
            useAnimatedScrollView={true}
            contentContainerStyle={styles.scrollContainer}>
            <BackHeader {...props}/>
    
            <View style={[styles.rowContainer,{paddingHorizontal:10,
                paddingVertical:20,width:dimensions.SCREEN_WIDTH}]}>
                <View>
                    <Text style={styles.entryText}>Entry Fees</Text>
                    <Text style={styles.topPriceText}>{parseInt(details.price) * quantity} {details.currency}</Text>
                </View>
    
                <View style={styles.rowContainer}>
                    <TouchableOpacity onPress={()=> onPlus()}>
                    <FastImage 
                        source={require('../assets/plus.png')}
                        style={styles.imageStyle} 
                        resizeMode={FastImage.resizeMode.contain}/>
                    </TouchableOpacity>
    
                        <Text style={styles.quantityText}>{quantity}</Text>
    
                    <TouchableOpacity onPress={()=> onMinus()}>
                    <FastImage 
                        source={require('../assets/minus.png')}
                        style={styles.imageStyle} 
                        resizeMode={FastImage.resizeMode.contain}/>
                    </TouchableOpacity>
    
    
                </View>
            </View>
    
          </ScrollView>
          
          <View style={[styles.rowContainer,styles.bottomContainer]}>
                <View>
                    <Text style={styles.topPriceText}>{parseInt(details.price) * quantity} {details.currency}</Text>
                    <Text style={styles.entryText}>{quantity} Tickets</Text>
                </View>
    
                <CustomButton 
                 style={{width :dimensions.SCREEN_WIDTH * 0.4,margin:15}}
                handler={_onDone}
                label ={'Proceed'}/>
               
           </View>
           </>
        )
}
   
  
function roomChange(val){
   setRoomQuantity(val)
}
function peopleChange(val){
    setPeopleQuantity(val)
}

function travelExperience(){
    return(
        <>

        <DateTimePickerModal
            isVisible={isFromDatePickerVisible}
            mode="date"
            //maximumDate={new Date()}
            minimumDate={new Date()}
            onConfirm={handleFromDateConfirm}
            onCancel={hideFromDatePicker}
        />

        <DateTimePickerModal
            isVisible={isToDatePickerVisible}
            mode="date"
            minimumDate={new Date()}
            onConfirm={handleToDateConfirm}
            onCancel={hideToDatePicker}
        /> 


        <ScrollView 
        bounces={false}
        useAnimatedScrollView={true}
        contentContainerStyle={styles.scrollContainer}>
        <BackHeader {...props}/>


        <View style={{height:20}}/>
        <View style={[styles.rowContainer,{width:'100%'}]}>
            <TouchableOpacity onPress={showFromDatePicker}>
                <Text style={styles.topPriceText}>From Date</Text>
                { fromDate ?  <Text style={styles.topPriceTextGolden}>{fromDate}</Text> : null}
            </TouchableOpacity>

            <TouchableOpacity onPress={showFromDatePicker}>
            <FastImage 
                        source={require('../assets/calendar.png')}
                        style={{height:23,width:23}} 
                        resizeMode={FastImage.resizeMode.contain}/>
            </TouchableOpacity>
        </View>


        <View style={{height:20}}/>
        <View style={[styles.rowContainer,{width:'100%'}]}>
            <TouchableOpacity onPress={showToDatePicker}>
                <Text style={styles.topPriceText}>To Date</Text>
                { toDate ?  <Text style={styles.topPriceTextGolden}>{toDate}</Text> : null}
            </TouchableOpacity>

            <TouchableOpacity onPress={showToDatePicker}>
                   <FastImage 
                        source={require('../assets/calendar.png')}
                        style={{height:23,width:23}} 
                        resizeMode={FastImage.resizeMode.contain}/>
             </TouchableOpacity>
        </View>

        <View style={{height:20}}/>
        <View style={[styles.rowContainer,{width:'100%'}]}>
            <View>
                <Text style={styles.topPriceText}>No. Of Rooms</Text>
            </View>

            <Quantity  onChange={roomChange} ref={roomQuantityInput}/>
        </View>

        <View style={{height:20}}/>
        <View style={[styles.rowContainer,{width:'100%'}]}>
            <View>
                <Text style={styles.topPriceText}>No. Of Members</Text>
            </View>

            <Quantity  onChange={peopleChange} ref={peopleQuantityInput}/>
        </View>

   

      </ScrollView>
      
      <View style={[styles.rowContainer,styles.bottomContainer]}>
            <View>
                <Text style={styles.topPriceText}>From Date : 
                    <Text style={styles.valueText}> {fromDate}</Text>
                </Text>
                <Text style={styles.topPriceText}>To Date : 
                    <Text style={styles.valueText}> {toDate}</Text>
                </Text>
                <Text style={styles.topPriceText}>Rooms : 
                    <Text  style={styles.valueText}> {roomQuantity}</Text>
                </Text>

                <Text style={styles.topPriceText}>Members : 
                    <Text  style={styles.valueText}> {peopleQuantity}</Text>
                </Text>
            </View>

            <CustomButton 
             style={{width :dimensions.SCREEN_WIDTH * 0.4,margin:15}}
            handler={_onTravelDone}
            label ={'Proceed'}/>
           
       </View>
       </>
    )
}
 return (
    <>
      <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>

        {details.package_type == 'Lifestyle'
        ? lifestyleExperience() : travelExperience()}
      
      {apiReducer.loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        flexGrow:1
    },
   
    rowContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    entryText:{
        color:colors.COLOR_PRIMARY,
        fontSize:19,
        fontWeight:'800',
        lineHeight:30
    },
    topPriceText:{
        color:colors.DARK_GREY,
        fontSize:15,
        fontWeight:'800',
        lineHeight:30
    },
    topPriceTextGolden:{
        color:colors.COLOR_PRIMARY,
        fontSize:17,
        fontWeight:'800',
        lineHeight:30
    },
    imageStyle:{
        height:25,
        width:25
    },
    quantityText:{
        color:colors.COLOR_PRIMARY,
        fontSize:15,
        fontWeight:'800',
        marginHorizontal:10
    },
    bottomContainer:{
        position:'absolute',
        bottom:0,left:0,right:0,
        borderTopColor:colors.COLOR_PRIMARY,
        borderTopWidth:1,
        paddingVertical:15,
        paddingHorizontal:10,
    },
    valueText:{color:colors.COLOR_PRIMARY,fontSize:10}
   

})


export default JoinExperience;


