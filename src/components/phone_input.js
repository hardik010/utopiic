import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TextInput
} from 'react-native';

import FastImage from 'react-native-fast-image'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Dropdown } from 'react-native-material-dropdown';
import { color } from 'react-native-reanimated';

import {colors,urls,dimensions} from '../utils/constants';

import countryList from '../utils/country_list';


const PhoneInput = React.forwardRef((props,ref) =>  {

     

    const [text , setText ]  = useState('');
    const [isFocus , setFocus ]  = useState(false);
    const [countryCode , setCountryCode] = useState("");
        // The component instance will be extended
        // with whatever you return from the callback passed
        // as the second argument
        useImperativeHandle(ref, () => ({
            getInputValue() {
                return text
            },
            getInputCode() {
                return countryCode
            },
            setVal(text){
              setText(text)
            },
            setCode(code){
                setCountryCode(code)
            }

         }));

   useEffect(()=>{
     console.log(countryList)
   },[])


   const onCountryCodeChange = (value) => {

    countryList.countries.forEach(element => {
        if(element.value  == value ){
            setCountryCode(element.label);
        }
    })
}
  return (
    <View style={[styles.container, props.style]}>
            <View style={styles.imageStyle}>
            <Dropdown
                            placeholder="Country code"
                            dropdownOffset={{top:10,marginLeft:30,left:0}}
                            value={countryCode}
                            itemCount={8}
                            fontSize={13}
                            onChangeText={(value) => { onCountryCodeChange(value) }}
                            containerStyle={{width:'80%',marginRight:0,}}
                            data={countryList.countries}
                            inputContainerStyle={{ borderBottomColor: 'transparent' }} // remove underline color 
                            style = {{color: colors.WHITE}} //for changed text color
                            baseColor={colors.COLOR_PRIMARY} //for theme color like dropdown button
                            textColor={colors.COLOR_PRIMARY} // for item list color
                            //itemTextStyle={{backgroundColor:"blue",textColor:"white"}}//for item style
                        />
            </View>
        
   
  
            <TextInput
            style={[styles.inputText]}
            value={text}
            autoCapitalize={props.autoCapitalize}
            numberOfLines={1}
            maxLength ={props.maxLength}
            onFocus={() => setFocus(true)}
            onBlur={() => setFocus(false)}
            ref={props.inputRef}
            secureTextEntry={props.secureTextEntry}
            keyboardType={props.keyboardType}
            returnKeyType={props.returnKeyType}
            placeholder={props.placeholder}
            textContentType={props.textContentType}
            onSubmitEditing={props.onSubmitEditing}
            placeholderTextColor={colors.COLOR_PRIMARY}
            onChangeText={props.onChangeText ? props.onChangeText : (text) => setText(text)}
            editable={props.editable}
            multiline={false}
            numberOfLines={1}
            underlineColorAndroid={'rgba(0,0,0,0)'}
            blurOnSubmit={false}
            />
    
  </View>
 
  );
});

PhoneInput.defaultProps = {

    focus: () => {},
    style: {},
    placeholder: 'Enter',
    blurOnSubmit: false,
    returnKeyType: 'next',
    keyboardType: null,
    secureTextEntry: false,
    autoCapitalize: "none",
    textContentType: "none",
    defaultValue: '',
    editable: true,
    maxLength:40,
    inputImage : require('../assets/username.png')
};


const styles = StyleSheet.create({
    container: {
        flexDirection:'row',
        alignItems:'center',
        borderColor:colors.COLOR_PRIMARY,
        borderWidth:1,
        borderRadius:8,
        backgroundColor:colors.BLACK,
        margin:0,
        overflow:'hidden',
        paddingVertical:2,
        paddingHorizontal:5,
        marginHorizontal:0,
        marginVertical:10,
       justifyContent:'flex-start',
       width:null
        
    },
  
    inputText: {
      textAlign:'left',
      fontSize: 16,
      flex:7.5,
      color:colors.DARK_GREY,
      marginHorizontal:3
    },
    imageStyle:{
        flex:2,
        height:40,
        justifyContent:'center',alignItems:'center',
        borderRightColor:colors.COLOR_PRIMARY,
        borderRightWidth:0.5
      },
  });

export default PhoneInput;
