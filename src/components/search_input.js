import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TextInput
} from 'react-native';

import FastImage from 'react-native-fast-image'
import { TouchableOpacity } from 'react-native-gesture-handler';

import {colors,urls,dimensions} from '../utils/constants';


const SearchInput = React.forwardRef((props,ref) =>  {

     

    const [text , setText ]  = useState('');
    const [isFocus , setFocus ]  = useState(false);
        // The component instance will be extended
        // with whatever you return from the callback passed
        // as the second argument
        useImperativeHandle(ref, () => ({
            getInputValue() {
                return text
            },
            setVal(text){
              setText(text)
            }

         }));

   useEffect(()=>{
     
   },[])


  return (
    <View style={[styles.container, props.style]}>

        <TextInput
        style={[styles.inputText]}
        value={text}
        autoCapitalize={props.autoCapitalize}
        numberOfLines={1}
        maxLength ={props.maxLength}
        onFocus={() => setFocus(true)}
        onBlur={() => setFocus(false)}
        ref={props.inputRef}
        secureTextEntry={props.secureTextEntry}
        keyboardType={props.keyboardType}
        returnKeyType={props.returnKeyType}
        placeholder={props.placeholder}
        textContentType={props.textContentType}
        onSubmitEditing={props.onSubmitEditing}
        placeholderTextColor={colors.COLOR_PRIMARY}
        onChangeText={props.onChangeText ? props.onChangeText : (text) => setText(text)}
        editable={props.editable}
        multiline={false}
        numberOfLines={1}
        autoFocus={props.autoFocus}
        underlineColorAndroid={'rgba(0,0,0,0)'}
        blurOnSubmit={false}
        />

        <TouchableOpacity onPress={props.onSearch()}>
            <Image source ={require('../assets/search.png')}
            style={styles.imageStyle}
            resizeMode={'contain'} />
        </TouchableOpacity>
    
  </View>
 
  );
});

SearchInput.defaultProps = {

    focus: () => {},
    onSearch: () => {},
    style: {},
    placeholder: 'Search ...',
    blurOnSubmit: false,
    returnKeyType: 'next',
    keyboardType: null,
    secureTextEntry: false,
    autoCapitalize: "none",
    textContentType: "none",
    defaultValue: '',
    editable: true,
    maxLength:40,
    autoFocus:false
};


const styles = StyleSheet.create({
    container: {
     flexDirection:'row',
     alignItems:'center',
     borderColor:colors.COLOR_PRIMARY,
     borderWidth:1,
     backgroundColor:colors.BLACK,
     margin:0,
     overflow:'hidden',
     paddingHorizontal:7,
     paddingVertical:3,
     marginHorizontal:0,
     marginVertical:10,
     borderRadius:20,
    //  width:dimensions.SCREEN_WIDTH 
  
    },
  
    inputText: {
      textAlign:'left',
      fontSize: 16,
      flex:8.5,
      color:colors.DARK_GREY,
      marginHorizontal:3,
      height:40,
      
    },
    imageStyle:{
        flex:1.5,
        height:22,
        width:22,justifyContent:'center',alignItems:'center',
       
      },
  });

export default SearchInput;
