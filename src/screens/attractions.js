import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,ImageBackground, Keyboard
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';

import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { postRequestApiWithToken, postRequestNoBodyApi, postRequestNoBodyApiWithToken } from '../redux/actions/api_actions';
import config from '../config/config';
import HomeHeader from '../components/home_header';
import SearchInput from '../components/search_input';
import { color } from 'react-native-reanimated';

const Card = (props) =>{
    return(
        <View style={[styles.cardContainer,styles.cardBackground]}>
                <FastImage source ={{uri:props.image}} style={{
                    position:'absolute',
                    top:0,left:0,
                    right:0,left:0,height:'100%',width:'100%'
                }}/>
                <View style={styles.nameContainer}>
                    <Text style={styles.cardTitle}>{props.name}</Text>
                    <Text style={{color:'white'}}>{props.subName}</Text>
                </View>
        </View>
    )
   }



const Attractions: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const apiReducer = useSelector(state => state.apiReducer)

    const dispatch = useDispatch()

    const [attractions, setAttractions] = useState([]); 
    const [attractionsGlobal, setAttractionsGlobal] = useState([]); 
  

    function fetchAttractions(){
        const URL = config.BASE_URL + config.ATTRACTIONS_LIST
        dispatch(postRequestNoBodyApiWithToken(URL,userReducer.user.token))
        .then(response=> {
           // console.log('Response - ',JSON.stringify(response))
            if(response.status) {
                setAttractions(response.data.list)
                setAttractionsGlobal(response.data.list)
            }
            else showMessage(response.message)
        })
        .catch(error => {
             showMessage(error.message)
            // console.log('Error - ',error)
        })
    }


    useEffect(() =>{
        fetchAttractions()
    },[])

    function navigateToDetails(item){
        props.navigation.navigate('attraction_details',{'id':item.id})
    }

    function renderItem (item,index)  {
        return(
            <TouchableOpacity onPress={() => navigateToDetails(item)}>
                <Card image = {item.images}
                name={item.name} subName ={item.city_name}/>
            </TouchableOpacity>
        )
      }
      const searchInput = useRef();
      const searchRef = useRef();

    function _onSearch (){
        var searchText = searchInput.current.getInputValue()
       // console.log(searchText)

        var results = [];    
        var name = ''
        for(var i = 0; i < attractionsGlobal.length  ; i++) {
             name = attractionsGlobal[i].name.toLowerCase()
            if(name  != null ){
                 var check = name.indexOf(searchText.toLowerCase()) > -1
                  if(check) {
                     results.push(attractionsGlobal[i]);
                  }
            }
        }
    
        if(results.length == 0){
         showMessage('No Destinations Found')
        }
        setAttractions(results)
        
      }


   
 return (
    <>
      <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
      <HomeHeader haveBack {...props}/>
      <ScrollView 
        bounces={false}
        useAnimatedScrollView={true}
        contentContainerStyle={styles.scrollContainer}>
        <View style={{height:10}}/>
        <Text style={styles.heading}>ATTRACTIONS</Text>  
        <SearchInput
         placeholder={'Search Attractions..'}
         onSubmitEditing={()=> _onSearch()}
         ref={searchInput}
         inputRef={ searchRef }
         style={{width :'60%',alignSelf:'center'}}/>
        <View style={{height:20}}/>
       

         <FlatList
             style={{marginVertical:10}}
             horizontal={false}
             numColumns={1}
             data={attractions}
              showsHorizontalScrollIndicator={false}
              renderItem={({ item ,index}) => renderItem(item,index)}
              keyExtractor={item => item.id}
            //   removeClippedSubviews={true}
            //   maxToRenderPerBatch={60}
            //   windowSize={30}
            //   initialNumToRender={14}
            //   shouldComponentUpdate={false}
            //   onEndReachedThreshold={0.001}
            //   legacyImplementation={true}
            //   bounces={false}
            maxToRenderPerBatch={3}
            windowSize={2}           
            updateCellsBatchingPeriod={3}   
            initialNumToRender={6}
            onEndReachedThreshold={1}
            extraData={attractions}
          />

      </ScrollView>
      {apiReducer.loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
       
    },
   
    rowContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
   
    heading:{
        color:colors.COLOR_PRIMARY,
        lineHeight:26,
        fontSize:20,
        fontWeight:'700',
        alignSelf:'center',
        maxWidth:'90%',
        marginVertical:12
    },
    categoryContainer:{
        width:'auto',
        paddingHorizontal:15,
        paddingVertical:8,
        backgroundColor:colors.LIGHT_BLACK,
        borderRadius:20,
        borderWidth:1,
        borderColor:colors.COLOR_PRIMARY,
        marginHorizontal:5,
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'row'
    
      },
      cardContainer:{
        width:dimensions.SCREEN_WIDTH,
        height:dimensions.SCREEN_HEIGHT * 0.31,
        marginVertical:7,
        backgroundColor:'black'
     },
     nameContainer:{
         marginBottom:20,
         alignItems:'center',
         padding:10,
         backgroundColor:'rgba(0,0,0,0.3)',
        width:'100%'
    },
     cardBackground:{
        justifyContent:'flex-end',
        alignItems:'center',
        backgroundColor:colors.COLOR_PRIMARY
     },
    cardTitle:{
            color:'white',
            fontSize:17,
            fontWeight:'800',
            textTransform:'uppercase',
            lineHeight:30,
            textAlign:'center',width:'85%',alignSelf:'center'
        },
    cardSubTitle:{
        color:'white',
        lineHeight:28,
        textTransform:'uppercase'
    },
   

})


export default Attractions;


