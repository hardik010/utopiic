import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';


import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import stripe from 'tipsi-stripe'
import RazorpayCheckout from 'react-native-razorpay';
import config from '../config/config';
import { postRequestApiWithToken } from '../redux/actions/api_actions';


stripe.setOptions({
   publishableKey:'pk_test_1AVBJuOm8e3dhijjUrUrNplA00o1EuKQl9',
})



const PaymentMethod: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false); 
    const [plan, setPlan] = useState([]);
    const [checked, setChecked] = useState(false);

    useEffect(() =>{
      //console.log('0000---',props.route.params.profile)
       setPlan(props.route.params.profile.plan)
    },[])


    function _bookPlan(txn_id){
      const URL = config.BASE_URL + config.SUBSCRIPTION_PAYMENT

      var formdata = new FormData()
      formdata.append('user_id',props.route.params.profile.user_id)
     // formdata.append('razorpay_plan_id', txn_id)
      formdata.append('razorpay_plan_id', plan.razorpay_plan_id)
      formdata.append('plan_id',plan.plan_id)
   
     // console.log(formdata)
      dispatch(postRequestApiWithToken(URL,formdata,userReducer.user.token))
      .then(response=> {
        //  console.log('Response - ',JSON.stringify(response))
          if(response.status){
            props.navigation.navigate('otp',{'profile':props.route.params.profile})
            showMessage("Booked Successfully !!")
          }
          else showMessage(response.message)
      })
      .catch(error => {
         showMessage(error.message)
          console.log('Error - ',error)
      })
  }

   function _razorPay(){

    var image ='https://static.wixstatic.com/media/20b4ce_d696dfd13905444d8ca2f37a5e9fe91d~mv2.png/v1/fill/w_344,h_100,al_c,q_85,usm_0.66_1.00_0.01/Asset%2024.webp'

      var options = {
          description: 'Utopiic - A travel Concierge',
          image:image,
          currency: 'INR',
         
           key:'rzp_live_QXWEW7zcLOxpvI',
           amount: parseInt(props.route.params.profile.plan.plan_price) * 100 ,
         //amount:'200',
          //key: 'rzp_test_bkOHPTNmXd5abE',
          name: 'Utopiic',
          //"method": "card",
          prefill: {
            email:props.route.params.profile.email,
            contact:props.route.params.profile.phone,
            name:props.route.params.profile.name,
          },
          theme: {color: '#000000'}
        }


        RazorpayCheckout.open(options).then((data) => {
          // handle success
          //console.log('successs --- ',JSON.stringify(data))
          // showMessage("Payment Successful",false)
         // alert(`Success: ${data.razorpay_payment_id}`);
          // alert(`Success: ${JSON.stringify(data)}`);
          _bookPlan(data.razorpay_payment_id)
          
        }).catch((error) => {
          // handle failure
          //console.log(error)
         // alert(`Error: ${error.code} | ${error.description}`);

          showMessage(error.description)

        });
  }



    async function _stripe(){
      const options = {
        requiredBillingAddressFields: 'full',
        prefilledInformation: {
          email:'patrik@gmail.com',
          billingAddress: {
            name: '',
            line1: '',
            line2: '',
            city: '',
            state: '',
            country: '',
            postalCode: '',
          },
        },
      }
    
      const token = await stripe.paymentRequestWithCardForm(options)
      var cardId = token.card.cardId
      var tokenId = token.tokenId
      //console.log("carddd",JSON.stringify(token))
      props.navigation.navigate('otp',{'profile':props.route.params.profile})
     
      // this.stripePayment(tokenId)
    }


    function _onDone(){
         if(checked) _razorPay()
         else showMessage("Accept terms & conditions first")
    }


    function onTerms(){
      props.navigation.navigate('terms')
    }

    function onPrivacy(){
      props.navigation.navigate('privacy')
    }

   
 return (
    <>
      <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
       <BackHeader {...props}/>
     <ScrollView 
        bounces={false}
        useAnimatedScrollView={true}
        contentContainerStyle={styles.scrollContainer}>

          <Text style={styles.heading}>PAYMENT METHOD</Text>
          <View style={{height:30}}/> 

          <Text style={styles.headingSub}>Order Details</Text>      
            <View style={[styles.rowContainer,{width:'90%'}]}>
                <View style={styles.rowContainer}>
                    <FastImage 
                    source={plan.image}
                    style={{height:40,width:40}} 
                    resizeMode={FastImage.resizeMode.contain}/>
                    <Text style={styles.labelText}>  {plan.plan_name}</Text>
                </View>
                <Text style={styles.labelText}>₹ {plan.plan_price}</Text>
            </View>


        <Text style={styles.headingSub}>Bill Breakup</Text>  
            <View style={{width:'70%'}}>
                <View style={[styles.rowContainer,{margin:5}]}>
                        <Text style={styles.labelText}>Plan Cost</Text>
                     <Text style={styles.labelText}>₹ {parseFloat(parseFloat(plan.plan_price) / 1.18).toFixed(2)}</Text>
                </View>
                <View style={[styles.rowContainer,{margin:5}]}>
                        <Text style={styles.labelText}>Tax</Text>
                    <Text style={styles.labelText}>₹ {parseFloat(plan.plan_price - parseFloat(plan.plan_price) / 1.18).toFixed(2)}</Text>
                </View>
                <View style={[styles.rowContainer,{margin:5}]}>
                        <Text style={styles.labelText}>Order Total</Text>
                      <Text style={styles.labelText}>₹ {parseFloat(plan.plan_price).toFixed(2)}</Text>
                </View>
            </View>

            <View style={{height:20}}/> 
            <View style={{flexDirection:'row',
            alignItems:'center',width:'80%',justifyContent:'center',alignSelf:'center'}}>
            <CheckBox
                checkedColor={colors.COLOR_PRIMARY}
                checked={checked}
                onPress={() => setChecked(!checked)}
            />
            <Text style={{color:colors.WHITE,fontWeight:'500',fontSize:16,marginLeft:-10}}>
             I accept to the
             <Text onPress={onPrivacy}
             style={{color:colors.COLOR_PRIMARY,fontSize:16}}> Privacy Policy </Text> and
             <Text onPress={onTerms}
              style={{color:colors.COLOR_PRIMARY,fontSize:16}}> Terms of Use </Text>
              </Text>
        </View>

         <View style={{height:10}}/> 

{/* 
        <TouchableOpacity style={styles.paymentContainer}>
                  <FastImage 
                    source={require('../assets/logo.png')}
                    style={{height:20,width:20,marginHorizontal:10}} 
                    resizeMode={FastImage.resizeMode.contain}/>
                 <Text style={styles.labelText}>Credit/Debit Card</Text>
        </TouchableOpacity> */}


        {/* <TouchableOpacity style={styles.paymentContainer}>
                  <FastImage 
                    source={require('../assets/logo.png')}
                    style={{height:20,width:20,marginHorizontal:10}} 
                    resizeMode={FastImage.resizeMode.contain}/>
                 <Text style={styles.labelText}>Net Banking</Text>
        </TouchableOpacity>


        <TouchableOpacity style={styles.paymentContainer}>
                  <FastImage 
                    source={require('../assets/logo.png')}
                    style={{height:20,width:20,marginHorizontal:10}} 
                    resizeMode={FastImage.resizeMode.contain}/>
                 <Text style={styles.labelText}>UPI</Text>
        </TouchableOpacity> */}

          <CustomButton 
          style={{width :dimensions.SCREEN_WIDTH * 0.6,
            marginTop:55,marginBottom:19,alignSelf:'center'}}
          handler={_onDone}
          label ={'Paynow'}/>


      </ScrollView>

      {loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
    },
    heading:{
        color:colors.COLOR_PRIMARY,
        lineHeight:30,
        fontSize:17,
        fontWeight:'500',
        alignSelf:'center',
        textAlign:'center',
        maxWidth:'70%'
    },
    headingSub:{
        color:colors.COLOR_PRIMARY,
        lineHeight:40,
        fontWeight:'600',
        fontSize:15
    },
    rowContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    labelText:{
        color:colors.COLOR_PRIMARY,
        fontSize:13
    },
    paymentContainer:{
        width :dimensions.SCREEN_WIDTH * 0.8,
        padding:11,
        borderWidth:1,
        borderRadius:4,
        borderColor:colors.COLOR_PRIMARY,
        alignItems:'center',
        flexDirection:'row',
        margin:6
    }

})


export default PaymentMethod;


