import React, { useState ,forwardRef, useRef, useImperativeHandle } from 'react'
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  TouchableOpacity,
} from 'react-native';
import FastImage from 'react-native-fast-image'


import {colors,urls,dimensions} from '../utils/constants';



const HomeHeader  : () => React$Node = (props) => {


    function _onPressBack()  {
        props.navigation.goBack()
    }

    function _onDrawerToggle()  {
        props.navigation.toggleDrawer()
    }

    function _onSearch()  {
      props.navigation.navigate('search')
  }
      
      return (
       <View style={styles.container}>
          {
            props.haveBack 
            ?
            <TouchableOpacity onPress={_onPressBack}>
            <FastImage 
            source={require('../assets/back.png')}
            style={styles.headerImage} 
            resizeMode={FastImage.resizeMode.cover}/>
             </TouchableOpacity>
            :   <View style={[styles.headerImage,{width:70}]} />
          }

            <FastImage 
             source={require('../assets/logo.png')}
              style={[styles.headerImage,{marginLeft:props.haveBack ? 20 : 0}]} 
              resizeMode={FastImage.resizeMode.cover}/>

            <View style={{flexDirection:'row',alignItems:'center'}}>
                <TouchableOpacity onPress={_onSearch}>
                    <FastImage 
                    source={require('../assets/search-menu.png')}
                    style={{height:25,width:25}} 
                    resizeMode={FastImage.resizeMode.contain}/>
                </TouchableOpacity>
                <TouchableOpacity onPress={_onDrawerToggle}>
                    <FastImage 
                    source={require('../assets/menu.png')}
                    style={[{height:20,width:20},{marginLeft:14}]} 
                    resizeMode={FastImage.resizeMode.contain}/>
                </TouchableOpacity>
            </View>
 
       </View>
      )
};

HomeHeader.defaultProps = {
  haveBack: false,
};


const styles = StyleSheet.create({

    container:{
      flexDirection:'row',
      alignItems:'center',
      backgroundColor:colors.BLACK,
      padding:10,
      justifyContent:'space-between',
      width:dimensions.SCREEN_WIDTH,
      height:50,
        //ios    
      shadowOpacity: 0.3,
      shadowRadius: 3,
      shadowOffset: {
            height: 0,
            width: 0
       },
        //android
      elevation: 2
    },
    headerImage:{
        height:30,width:30
    }
  })

export default HomeHeader;
