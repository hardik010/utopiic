import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TextInput
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';
import Carousel from 'react-native-banner-carousel';


import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { color } from 'react-native-reanimated';
import HomeHeader from '../components/home_header';
import BottomTab from '../components/bottom_tab';
import ExperienceComponent from '../components/experience_component';
import SearchInput from '../components/search_input';
import SearchView from '../components/search_view';


//

const Card = (props) =>{
    return(
        <View style={styles.cardContainer}>
            <ImageBackground style={styles.cardBackground}
            resizeMode='stretch'
            source ={props.image}>
            </ImageBackground>
        </View>
    )
   }

const TravelWorld: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false); 

    useEffect(() =>{
    },[])

    function navigateToSearch(){
        props.navigation.navigate('search')
    }

  return (
    <>
    <HomeHeader {...props}/>
    <ScrollView  bounces={false} style={{flex:1,backgroundColor:'black'}}>
        <View style={{height:10}}/>
        {/* <Text style={styles.heading}>TRAVEL THE WORLD</Text> */}

        <FastImage 
        source={require('../assets/ttw.png')}
        style={{height:80,width:150,alignSelf:'center'}} 
        resizeMode={FastImage.resizeMode.contain}/>


        <TouchableOpacity onPress={()=> navigateToSearch()}>    
        <SearchView style={{width :'60%',alignSelf:'center'}}/>
        </TouchableOpacity>

        <View style={{height:20}}/>

        <TouchableOpacity onPress={()=> props.navigation.navigate('countries')}>
            <Card image = {require('../assets/country_banner.jpg')}/>
        </TouchableOpacity>

        <TouchableOpacity onPress={()=> props.navigation.navigate('hotels')}>
            <Card image = {require('../assets/hotel_banner.jpg')}/>
        </TouchableOpacity>


        <TouchableOpacity onPress={()=> props.navigation.navigate('attractions')}>
            <Card image = {require('../assets/attraction_banner.jpg')}/>
        </TouchableOpacity>

        <TouchableOpacity onPress={()=> props.navigation.navigate('experiences')}>
            <Card image = {require('../assets/experience_banner.jpg')}/>
        </TouchableOpacity>

      

    </ScrollView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        flexGrow:1
    },
    heading:{
        color:colors.COLOR_PRIMARY,
        lineHeight:26,
        fontSize:19,
        fontWeight:'700',
        alignSelf:'center',
        maxWidth:'90%',
        marginVertical:12
    },
    cardContainer:{
        width:dimensions.SCREEN_WIDTH,
        height:dimensions.SCREEN_HEIGHT * 0.31,
         marginVertical:7,
       
     },
     cardBackground:{
        height:'100%',width:dimensions.SCREEN_WIDTH,
        justifyContent:'center',alignItems:'center',aspectRatio:3/2
     },
    cardTitle:{color:'white',fontSize:17,fontWeight:'800',textTransform:'uppercase',lineHeight:30},
    cardSubTitle:{color:'white',lineHeight:28},
   
    
})


export default TravelWorld;


