import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import {createDrawerNavigator } from '@react-navigation/drawer';
import { useWindowDimensions } from 'react-native';
import { createStackNavigator } from "@react-navigation/stack"


import CustomDrawer from '../screens/custom_drawer.js';
import HomeNavigator from './home_navigator.js';
import Account from '../screens/account.js';
import PaymentHistory from '../screens/payment_history.js';
import TermsConditions from '../screens/terms.js';
import PrivacyPolicy from '../screens/privacy.js';
import BookingHistory from '../screens/booking_history.js';




const DrawerNavigator = (props) => {
    const dimensions = useWindowDimensions();
    const isLargeScreen = dimensions.width >= 768;
    const Drawer = createDrawerNavigator();
    return(
            <Drawer.Navigator  drawerPosition="right" 
                 //drawerType={isLargeScreen ? 'permanent' : 'back'}
                drawerStyle={isLargeScreen ? null : { width: '70%' }} 
                overlayColor="transparent"
                initialRouteName="home"
                drawerContent={(props) => <CustomDrawer {...props} />}
             >
                <Drawer.Screen  component = {HomeNavigator} name="home" />
                <Drawer.Screen  component = {Account} name="account" />
                <Drawer.Screen  component = {PaymentHistory} name="payment_history" />
                <Drawer.Screen  component = {BookingHistory} name="booking_history" />
                <Drawer.Screen  component = {TermsConditions} name="terms" />
                <Drawer.Screen  component = {PrivacyPolicy} name="privacy" />
            </Drawer.Navigator>
        
    )
}

export default DrawerNavigator;