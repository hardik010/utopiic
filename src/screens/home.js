import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TextInput
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';


import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { color } from 'react-native-reanimated';
import HomeHeader from '../components/home_header';
import BottomTab from '../components/bottom_tab';
import HomeData from './home_data';
import TravelWorld from './travel_world';
import SubstainableLifestyle from './substainable_lifestyle';
import WhatsNext from './whats_next';
import Chat from './chat';

import config from '../config/config';
import { postRequestNoBodyApiWithToken } from '../redux/actions/api_actions';
import { removeUser } from '../redux/actions/user_actions';

import analytics from '@react-native-firebase/analytics';
import Natives from './natives';

const Home: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false); 
    const [tab, setTab] = useState(''); 

    const bottomTabRef = useRef();

    function checkAuthToken(){
        const URL = config.BASE_URL + config.CHECK_AUTH_TOKEN
        
        dispatch(postRequestNoBodyApiWithToken(URL,userReducer.user.token))
        .then(response=> {
            if(!response.status){
                //means token expired
                showMessage("Token Expired",false)
                dispatch(removeUser({}))
              }
        })
        .catch(error => {
            showMessage(error.message)
            //console.log('Error - ',error)
        })
    }

    
    useEffect(() => {
        (async () => {
            if(userReducer.user.user_id){
                await analytics().logEvent('user', {
                    id: userReducer.user.user_id,
                    name: userReducer.user.name,
                })
            }

            checkAuthToken()
            setTab(bottomTabRef.current.getSelectedTabValue())
        })();
    }, []);
     

   

   function getData(){
    switch(tab) {
        case 'tab_1':
           return  <HomeData {...props}/>;
        case 'tab_2':
           return  <TravelWorld {...props}/>;
        case 'tab_3':
           return  <SubstainableLifestyle {...props}/>;
        case 'tab_4':
           return  <Natives {...props}/>;
        case 'tab_5':
            return <Chat {...props}/>;
        default:
           return <HomeData {...props}/>;
      }
       
    }

    function callback(tabValue)  {
        //console.log(tabValue)
        setTab(tabValue)
    }

  return (

  <View style={{backgroundColor:'black',flex:1}}>
        <SafeAreaView/>
        <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
        <View style={{flex:1}}>

        {  getData() }

        </View>
        <BottomTab  ref={bottomTabRef} parentCallback={callback}/>
        {loading && <ProgressBar/>}
        <SafeAreaView style={{backgroundColor:'rgba(30,30,30,0.7)'}}/>
  </View>

  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        flexGrow:1
    },
    heading:{
        color:colors.COLOR_PRIMARY,
        lineHeight:30,
        fontSize:20,
        fontWeight:'500',
        alignSelf:'center',
        textAlign:'center',
        maxWidth:'70%'
    },
    headingSub:{
        color:colors.COLOR_PRIMARY,
        fontWeight:'400',
        fontSize:13,
        width:'70%',
        textAlign:'center'
    },
    rowContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    labelText:{
        color:colors.COLOR_PRIMARY,
        fontSize:13
    },
   textBox:{
    borderBottomColor:colors.COLOR_PRIMARY,
    borderBottomWidth:1,
    backgroundColor:colors.BLACK,
    paddingHorizontal:5,
    paddingVertical:9,
    marginHorizontal:0,
    marginVertical:10,
    height:50,
    width:'80%',
    color:colors.DARK_GREY,
    textAlign:'center'
   }

})


export default Home;


