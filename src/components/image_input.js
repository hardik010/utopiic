import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TextInput
} from 'react-native';

import FastImage from 'react-native-fast-image'
import { TouchableOpacity } from 'react-native-gesture-handler';

import {colors,urls,dimensions} from '../utils/constants';


const ImageInput = React.forwardRef((props,ref) =>  {

     

    const [text , setText ]  = useState('');
    const [isFocus , setFocus ]  = useState(false);
        // The component instance will be extended
        // with whatever you return from the callback passed
        // as the second argument
        useImperativeHandle(ref, () => ({
            getInputValue() {
                return text
            },
            setVal(text){
              setText(text)
            }

         }));

   useEffect(()=>{
     
   },[])


  return (
    <View style={[styles.container, props.style]}>

      <Image source ={props.inputImage}
      style={styles.imageStyle}
      resizeMode={'contain'}
      />
   
  
     <TextInput
      style={[styles.inputText]}
      value={text}
      autoCapitalize={props.autoCapitalize}
      numberOfLines={1}
      maxLength ={props.maxLength}
      onFocus={() => setFocus(true)}
      onBlur={() => setFocus(false)}
      ref={props.inputRef}
      secureTextEntry={props.secureTextEntry}
      keyboardType={props.keyboardType}
      returnKeyType={props.returnKeyType}
      placeholder={props.placeholder}
      textContentType={props.textContentType}
      onSubmitEditing={props.onSubmitEditing}
      placeholderTextColor={colors.COLOR_PRIMARY}
      autoCapitalize='characters'
      onChangeText={props.onChangeText ? props.onChangeText : (text) => setText(text)}
      editable={props.editable}
      multiline={false}
      numberOfLines={1}
      underlineColorAndroid={'rgba(0,0,0,0)'}
      blurOnSubmit={false}
    />
    
  </View>
 
  );
});

ImageInput.defaultProps = {

    focus: () => {},
    style: {},
    placeholder: 'Enter',
    blurOnSubmit: false,
    returnKeyType: 'next',
    keyboardType: null,
    secureTextEntry: false,
    autoCapitalize: "none",
    textContentType: "none",
    defaultValue: '',
    editable: true,
    maxLength:40,
    inputImage : require('../assets/username.png')
};


const styles = StyleSheet.create({
    container: {
     flexDirection:'row',
     alignItems:'center',
     borderBottomColor:colors.COLOR_PRIMARY,
     borderBottomWidth:1,
     backgroundColor:colors.BLACK,
     margin:0,
     overflow:'hidden',
     paddingHorizontal:5,
     paddingVertical:9,
     marginHorizontal:0,
     marginVertical:10
  
    },
  
    inputText: {
      textAlign:'left',
      fontSize: 16,
      flex:8.5,
      color:colors.DARK_GREY,
      marginHorizontal:3
    },
    imageStyle:{
        flex:1.5,
        height:30,
        width:'78%',justifyContent:'center',alignItems:'center'
      },
  });

export default ImageInput;
