import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,FlatList,TouchableOpacity
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';

import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { postRequestNoBodyApi } from '../redux/actions/api_actions';
import config from '../config/config';
import HomeHeader from '../components/home_header';




const NativeDetails: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const apiReducer = useSelector(state => state.apiReducer)
    const dispatch = useDispatch()
    const [nativeDetail, setNativeDetail] = useState(''); 

    useEffect(() =>{
        let item =  props.route.params.item
        //console.log(item)
        setNativeDetail(item)
    },[])

    const itemSeparator = () => {
        return (
          <View style={styles.seperator} />
        );
    }
    function navigateToVideo(item){
        props.navigation.navigate('native_video',{'item':item})
    }

    function renderItem (item,index)  {
        return(
           <TouchableOpacity onPress={() => navigateToVideo(item)}
            style={[styles.rowContainer,styles.programContainer]}>
               <View style={{flex:2.5,marginHorizontal:2}}>
                <FastImage style={{ width: '100%', height:100 }}
                    source={{ uri: item.thumb ,priority: FastImage.priority.normal,}}
                    resizeMode={FastImage.resizeMode.cover}/>
               </View>

               <View style={{flex:8,marginHorizontal:6}}>
                     <Text style={styles.titleText}>{item.title}</Text>
                    <Text  style={styles.durationText}><Text style={{color:colors.COLOR_PRIMARY}}>Duration :</Text> {item.duration}</Text>
                    <Text  style={styles.durationText}><Text style={{color:colors.COLOR_PRIMARY}}>Category :</Text>{item.category}</Text>
               </View>
           </TouchableOpacity>
        )
      }

   
 return (
    <>
       <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
      <HomeHeader haveBack {...props}/>
      <ScrollView 
            bounces={false}
            useAnimatedScrollView={true}
            contentContainerStyle={styles.scrollContainer}>

            <FastImage style={{ width: dimensions.SCREEN_WIDTH, height:dimensions.SCREEN_HEIGHT * 0.33 }}
                source={{ uri: nativeDetail.profile ,priority: FastImage.priority.normal,}}
                resizeMode={FastImage.resizeMode.cover}/>

            <View style={{height:10}}/>


            <Text style={styles.nameText}>{nativeDetail.company_name}</Text>
            <Text style={styles.categoryText}>{nativeDetail.category}</Text>
            <Text style={styles.descriptionText}>{nativeDetail.description}</Text>

            <View style={{height:10}}/>
            <Text style={styles.headingText}>Videos</Text>
            <View style={{height:10}}/>

            { nativeDetail.product && nativeDetail.product.length > 0 ?
               <View>
                    <View style={styles.seperator} />
                    <View>
                  <FlatList 
                      showsVerticalScrollIndicator={false}
                      scrollEnabled={false}
                      ItemSeparatorComponent={itemSeparator}
                      data={nativeDetail.product}
                      renderItem={({ item ,index}) => renderItem(item,index)}
                      keyExtractor={(item) => item.id } />   
                  </View>      
                <View style={styles.seperator} />
               </View>
                :
                <Text style={{color:'red',margin:10}}>No Videos Found</Text>

            }


      </ScrollView>
      {apiReducer.loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        flexGrow:1
    },
   
    rowContainer:{
        flexDirection:'row',
        alignItems:'center'
    },
    nameText:{
        color:'white',
        fontSize:17,
        fontWeight:'900',
        textTransform:'capitalize',
        lineHeight:32
    },
    categoryText:{
        color:'white',
        fontSize:13,
        textTransform:'capitalize',
        lineHeight:32
    },
    descriptionText:{
        color:'white',
        fontSize:13,
        textTransform:'capitalize',
        lineHeight:32
    },
    headingText:{
        color:colors.COLOR_PRIMARY,
        fontSize:19,
        fontWeight:'bold',
    },
    seperator:{
        height: 1,
        width: dimensions.SCREEN_WIDTH,
        backgroundColor: colors.COLOR_PRIMARY,
        marginVertical:2
      },
      programContainer:{
        width:dimensions.SCREEN_WIDTH * 0.9 ,margin:10,alignSelf:'center'
      },
      titleText:{
          color:'white',
          lineHeight:30,
          fontWeight:'800',
          textTransform:'capitalize',
      },
      durationText:{
          color:'#eee',
          lineHeight:28,
          fontSize:11,
          textTransform:'capitalize',

      }
   

})


export default NativeDetails;


