import React, { useEffect, useState } from 'react';
import {  useSelector, useDispatch } from 'react-redux';
import { NavigationContainer,DefaultTheme } from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack"
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, TouchableOpacity, SafeAreaView,Alert} from 'react-native';

import WelcomePager from '../screens/welcome_pager.js';
import Home from '../screens/home.js';
import Countries from '../screens/countries.js';
import Experiences from '../screens/experiences.js';
import CountryDetails from '../screens/country_details.js';
import Brands from '../screens/brands.js';
import BrandDetails from '../screens/brand_details.js';
import FashionBrands from '../screens/fashion_brands.js';
import FoodBrands from '../screens/food_brands.js';
import ViewWeb from '../screens/view_web.js';
import ExperienceDetail from '../screens/exprience_detail.js';
import Attractions from '../screens/attractions.js';
import AttractionDetails from '../screens/attraction_details.js';
import JoinExperience from '../screens/join_experience.js';
import Bloglist from '../screens/blog_list.js';
import Hotels from '../screens/hotels.js';
import Search from '../screens/search.js';
import HomeBrands from '../screens/home_brands.js';
import FitnessBrands from '../screens/fitness_brands.js';
import Natives from '../screens/natives.js';
import NativeDetails from '../screens/native_details.js';
import NativeVideo from '../screens/native_video.js';



//navigators
const Stack = createStackNavigator()


const HomeNavigator = () => {
     
  return (
        <Stack.Navigator initialRouteName='welcome_pager' mode='modal' options={{ gesturesEnabled: true,}}>
          <Stack.Screen name="home" component={Home} options ={{ headerShown :false }} />
          <Stack.Screen name="countries" component={Countries} options ={{ headerShown :false }} />
          <Stack.Screen name="experiences" component={Experiences} options ={{ headerShown :false }} />
          <Stack.Screen name="country_details" component={CountryDetails} options ={{ headerShown :false }} />
          <Stack.Screen name="brands" component={Brands} options ={{ headerShown :false }} />
          <Stack.Screen name="hotels" component={Hotels} options ={{ headerShown :false }} />
          <Stack.Screen name="brand_details" component={BrandDetails} options ={{ headerShown :false }} />
          <Stack.Screen name="fashion_brands" component={FashionBrands} options ={{ headerShown :false }} />
          <Stack.Screen name="food_brands" component={FoodBrands} options ={{ headerShown :false }} />
          <Stack.Screen name="home_brands" component={HomeBrands} options ={{ headerShown :false }} />
          <Stack.Screen name="fitness_brands" component={FitnessBrands} options ={{ headerShown :false }} />
          <Stack.Screen name="view_web" component={ViewWeb} options ={{ headerShown :false }} />
          <Stack.Screen name="experience_detail" component={ExperienceDetail} options ={{ headerShown :false }} />
          <Stack.Screen name="attractions" component={Attractions} options ={{ headerShown :false }} />
          <Stack.Screen name="attraction_details" component={AttractionDetails} options ={{ headerShown :false }} />
          <Stack.Screen name="join_experience" component={JoinExperience} options ={{ headerShown :false }} />
          <Stack.Screen name="blog_list" component={Bloglist} options ={{ headerShown :false }} />
          <Stack.Screen name="search" component={Search} options ={{ headerShown :false }} />
          <Stack.Screen name="natives" component={Natives} options ={{ headerShown :false }} />
          <Stack.Screen name="native_details" component={NativeDetails} options ={{ headerShown :false }} />
          <Stack.Screen name="native_video" component={NativeVideo} options ={{ headerShown :false }} />
        </Stack.Navigator>
    )
}

export default HomeNavigator