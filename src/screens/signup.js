import React, { useState ,forwardRef, useRef, useImperativeHandle } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image, Keyboard
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';


import { showMessage } from '../utils/snackmsg';


import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { postRequestApi } from '../redux/actions/api_actions';
import config from '../config/config';
import { addUser } from '../redux/actions/user_actions';
import PhoneInput from '../components/phone_input';


const SignUp: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const apiReducer = useSelector(state => state.apiReducer)

    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false);
    const [checked, setChecked] = useState(false);


  const nameInput = useRef();
  const nameRef = useRef();
  const phoneInput = useRef();
  const phoneRef = useRef();
  const emailInput = useRef();
  const emailRef = useRef();
  const addressInput = useRef();
  const addressRef = useRef();
  const ageInput = useRef();
  const ageRef = useRef();
 

 function validEmail(value){
  return  (/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(value))
}
function isValidNum(value){
  return (/^\d+$/).test(value)
}

function isValid(){
  Keyboard.dismiss()
  var result = false
  let email = emailInput.current.getInputValue()
  let name = nameInput.current.getInputValue()
  let age = ageInput.current.getInputValue()
  let address = addressInput.current.getInputValue()
  let phone = phoneInput.current.getInputValue()
  let code = phoneInput.current.getInputCode()


  if(name.length == 0) showMessage("Enter Name ");
  else if(code.length == 0) showMessage("Enter Country Code")
  else if(phone.length == 0) showMessage("Enter Phone Number")
  else if(phone.length < 9 ) showMessage("Enter valid  Phone Number")
  else if(!isValidNum(phone)) showMessage("Enter Valid Phone Number")
  else if(email.length == 0) showMessage("Enter Email")
  else if(!validEmail(email)) showMessage("Enter valid Email")
  else if(age.length == 0) showMessage("Enter Age")
  else if(!isValidNum(age)) showMessage("Enter Valid Age")
  else  if(address.length == 0) showMessage("Enter Address ");
  else result = true

  return result
}


async function _onSignUp()  {
  if(isValid()){
  
    let email = emailInput.current.getInputValue()
    let name = nameInput.current.getInputValue()
    let age = ageInput.current.getInputValue()
    let address = addressInput.current.getInputValue()
    let phone = phoneInput.current.getInputValue()
    let code = phoneInput.current.getInputCode()

    var formdata = new FormData()
    formdata.append('name',name)
    formdata.append('email',email)
    formdata.append('phone',phone)
    formdata.append('address',address)
    formdata.append('age',age)
    formdata.append('country_code',code.toString().substring(1,code.toString().length))
    formdata.append('device_id','1212121x')
    formdata.append('type',Platform.OS == 'android' ? 1 : 2)
 

    const URL = config.BASE_URL + config.REGISTER
        
    dispatch(postRequestApi(URL,formdata))
    .then(response=> {
        console.log('Response - ',JSON.stringify(response))
        if(response.status) {
          let profile={}
          profile.token = response.data.token;
          profile.user_type = response.data.user_type;
          profile.user_id = response.data.user_id;
          profile.passcode = response.data.passcode;
          profile.phone = response.data.phone;
          profile.address = response.data.address;
          profile.email = response.data.email;
          profile.name = name;
          profile.age = response.data.age;
          profile.otp = response.data.otp;
          props.navigation.navigate('plans',{profile:profile})

          
        }
        else showMessage(response.message)
    })
    .catch(error => {
        showMessage(error.message)
        //console.log('Error - ',error)
    })
   
  }
   
}
async function _onSignUps(){
    props.navigation.navigate('plans')
} 


 return (
    <>
       <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
          <BackHeader {...props}/>
        <InputScrollView useAnimatedScrollView={true}
        contentContainerStyle={styles.scrollContainer}>

         <View style={{height:40}}/>
          <Text style={styles.heading}>Sign Up!</Text>
          <View style={{height:50}}/>       

          <CustomTextInput
              ref={nameInput}
              placeholder={'Name'}
              onSubmitEditing={()=> phoneRef.current.focus()}
              inputRef={ nameRef }
              style={{width:'90%',marginTop:-20}}
              returnKeyType="next"
         />
        
            <PhoneInput
            ref={phoneInput}
            placeholder={'Phone Number'}
            keyboardType="numeric"
            maxLength={10}
              textContentType='telephoneNumber'
            onSubmitEditing={()=>  emailRef.current.focus()}
            inputRef={phoneRef}
            style={{width:'82%'}}
            returnKeyType="next"
        />
        <CustomTextInput
              ref={emailInput}
              placeholder={'Email Address'}
              keyboardType={'email-address'}
              onSubmitEditing={()=> ageRef.current.focus()}
              inputRef={ emailRef }
              style={{width:'90%'}}
              returnKeyType="next"
         />

          

           
       
        <CustomTextInput
            ref={ageInput}
            placeholder={'Age'}
            keyboardType="numeric"
            maxLength={2}
            textContentType='telephoneNumber'
            onSubmitEditing={()=> addressRef.current.focus()}
            inputRef={ageRef}
            style={{width:'90%'}}
            returnKeyType="next"
        />

      <CustomTextInput
              ref={addressInput}
              placeholder={'Billing Address'}
              keyboardType={'email-address'}
              onSubmitEditing={()=> _onSignUp()}
              inputRef={ addressRef }
              style={{width:'90%'}}
              multiline={true}
              returnKeyType="next"
         />
        
    

        <CustomButton 
        style={{width :dimensions.SCREEN_WIDTH * 0.77,marginTop:35,marginBottom:19}}
        handler={_onSignUp}
        label ={'Submit'}/>


    
                         

     




      </InputScrollView>

      {apiReducer.loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        backgroundColor:'black',
        flexGrow:1
      },
      heading:{
        color:colors.COLOR_PRIMARY,
        lineHeight:35,
        fontSize:15,
        fontWeight:'500',
        alignSelf:'center'
    },
})


export default SignUp;


