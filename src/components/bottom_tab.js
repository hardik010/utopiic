import React, { useState ,forwardRef, useRef, useImperativeHandle } from 'react'
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  TouchableOpacity,
} from 'react-native';
import FastImage from 'react-native-fast-image'
import {colors,urls,dimensions} from '../utils/constants';

const BottomTab  = React.forwardRef((props,ref) =>  {

  const [selectedTab, setSelectedTab] = useState('tab_1')

    useImperativeHandle(ref, () => ({
          getSelectedTabValue() {
              return selectedTab
          },
          setTab(val){
            setSelectedTab(val)
          }
    }));

    function _onPress(val)  {
      setSelectedTab(val)
      props.parentCallback(val)
    }
      
   return (
      <View style={styles.parentContainer}>
       <View style={styles.container}>

       <TouchableOpacity style={styles.imageContainer}
             onPress={()=>_onPress('tab_1')}>
                <FastImage 
                source={require('../assets/home.png')}
                style={selectedTab == 'tab_1' ? styles.headerImage : styles.headerImageUnselected} 
                resizeMode={FastImage.resizeMode.cover}/>
            </TouchableOpacity>

            <TouchableOpacity style={styles.imageContainer}
             onPress={()=>_onPress('tab_2')}>
                <FastImage 
                source={require('../assets/travel.png')}
                style={selectedTab == 'tab_2' ? styles.headerImage : styles.headerImageUnselected} 
                resizeMode={FastImage.resizeMode.cover}/>
            </TouchableOpacity>


            <TouchableOpacity style={styles.imageContainer}
             onPress={()=>_onPress('tab_3')}>
                <FastImage 
                source={require('../assets/fashion.png')}
                style={selectedTab == 'tab_3' ? styles.headerImage : styles.headerImageUnselected} 
                resizeMode={FastImage.resizeMode.cover}/>
            </TouchableOpacity>



            <TouchableOpacity style={styles.imageContainer}
             onPress={()=>_onPress('tab_4')}>
                <FastImage 
                source={require('../assets/video.png')}
                style={selectedTab == 'tab_4' ? styles.headerImage : styles.headerImageUnselected} 
                resizeMode={FastImage.resizeMode.contain}/>
            </TouchableOpacity>


            <TouchableOpacity style={styles.imageContainer}
             onPress={()=>_onPress('tab_5')}>
                <FastImage 
                source={require('../assets/chat.png')}
                style={selectedTab == 'tab_5' ? styles.headerImage : styles.headerImageUnselected}
                resizeMode={FastImage.resizeMode.contain}/>
            </TouchableOpacity>

       </View>
      </View>
   )
})

BottomTab.defaultProps = {
};


const styles = StyleSheet.create({
    parentContainer:{
        // position:'absolute',
        // bottom:0,
        // right:0,
        // left:0,
        height:60,
        paddingHorizontal:20,
        backgroundColor:colors.BLACK,
        //ios    
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowOffset: {
            height: 0,
            width: 0
        },
        //android
        elevation: 2
    },
    container:{
      flexDirection:'row',
      alignItems:'center',
      borderTopColor:colors.COLOR_PRIMARY,
      borderTopWidth:1,
      paddingHorizontal:10,
      paddingVertical:10,
      width:'100%',
      height:'100%'
      
    },
    imageContainer:{
       flex:1,
       justifyContent:'center',
       alignItems:'center'
    },
    headerImage:{
        height:30,width:30
    },
    headerImageUnselected:{
      height:22,width:22
   },
   headerImageTwo:{
    height:25,width:25
},
headerImageUnselectedTwo:{
  height:21,width:21
}
  })

export default BottomTab;
