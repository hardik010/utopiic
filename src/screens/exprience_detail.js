import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';

import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { postRequestApiWithToken, postRequestNoBodyApi, postRequestNoBodyApiWithToken } from '../redux/actions/api_actions';
import config from '../config/config';
import HomeHeader from '../components/home_header';
import Carousel from 'react-native-banner-carousel';




const ExperienceDetail: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const apiReducer = useSelector(state => state.apiReducer)

    const dispatch = useDispatch()

    const [details, setDetails] = useState(''); 
    const [images, setImages] = useState(''); 

    function fetchDetails(id){
        const URL = config.BASE_URL + config.EXPERIENCE_DETAIL 
        var formdata = new FormData()
        formdata.append('package_id',id)
        dispatch(postRequestApiWithToken(URL,formdata,userReducer.user.token))
        .then(response=> {
           // console.log('Response - ',JSON.stringify(response))
            if(response.status){
                setDetails(response.data.details)
                setImages(response.data.details.images)
            }
        })
        .catch(error => {
           showMessage(error.message)
            //console.log('Error - ',error)
        })
    }

    useEffect(() =>{
        //console.log(props.route.params)
        fetchDetails(props.route.params.id)
    },[])

    function renderPage(item,index){
        return (
            <View key={index}>
                 <FastImage
                style={{ width: dimensions.SCREEN_WIDTH, height: '100%' }}
                source={{ uri: item,
                    priority: FastImage.priority.normal,
                }}
            resizeMode={FastImage.resizeMode.cover}/>
            </View>
        );
    }

  function  _renderIncludes ({item, index}){
        return (
           <View
            style={styles.listContainer}>
          <View style={styles.imgContainer}>
           <FastImage 
           source={{uri:item.image}}
           style={styles.facilitiesImage} 
           resizeMode={FastImage.resizeMode.contain}/>
           </View>
      
            <Text style={styles.facilitiesText}>{item.name}</Text>
           </View>
        )
      }

  function _onJoin(){
      //console.log(details)
      if(details.package_id){
          props.navigation.navigate('join_experience',{details:details})
        
      }
  }
   
 return (
    <>
      <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
      <HomeHeader haveBack {...props}/> 
      <ScrollView 
        bounces={false}
        useAnimatedScrollView={true}
        contentContainerStyle={styles.scrollContainer}>

             {/** image  */}
            <View style={styles.imageContainer}>
                {
                images && images.length >0 ?
                <Carousel
                style={{alignSelf:'center'}}
                autoplay  autoplayTimeout={4000}
                in
                loop index={0} pageSize={dimensions.SCREEN_WIDTH}>
                {images.map((image, index) => renderPage(image, index))}
                </Carousel>  
                :null

                }
            </View>
        {/** image end */}

           <View style={{height:15}}/>
           <Text style={styles.nameText}>{details.name}</Text>
           <Text style={{color:'white'}}>{details.city_name} {details.country_name}</Text>
           <View style={{height:10}}/>
           <View style={styles.rowContainer}>
           <Text style={{color:'white'}}>{details.package_unique_code} </Text>
           <Text style={{color:'white',width:'60%'}}>{details.hashtag}</Text>
           <Text style={{color:'white'}}></Text>
           </View>

           <CustomButton 
             style={{width :dimensions.SCREEN_WIDTH * 0.45,margin:45,alignSelf:'center'}}
            handler={_onJoin}
            label ={'Join'}/>

            <Text style={styles.heading}>About this event</Text>

            <Text style={styles.descriptionText}>{details.description}</Text>

             {/** Includes */}
             {
                 details.includes &&  details.includes.length > 0 ?
                  <View >
                       <Text style={styles.heading}>Includes</Text>
                    <FlatList
                        style={{marginVertical:10}}
                        numColumns={2}
                        data={details.includes}
                        showsHorizontalScrollIndicator={false}
                        renderItem={(item,index) => _renderIncludes(item,index)}
                        keyExtractor={item => item.id}
                    />
                    </View>
                    :null
                }
          {/** Includes  end*/}


      </ScrollView>
      {apiReducer.loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        flexGrow:1
    },
   
    rowContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    imageContainer:{
        width:dimensions.SCREEN_WIDTH,
        height:dimensions.SCREEN_HEIGHT * 0.35,
        backgroundColor:colors.COLOR_PRIMARY
    },
    nameContainer:{
        width:dimensions.SCREEN_WIDTH,
        padding:20,
        justifyContent:'center',
        alignItems:'center',
        position:'absolute',
        top:0,
        left:0,
        right:0,
        backgroundColor:'rgba(0,0,0,0.0001)'
    },
    nameText:{
        color:colors.WHITE,
        fontSize:20,fontWeight:'bold',
        textTransform:'uppercase'
    },
    heading:{
        color:'white',
        fontSize:16,
        fontWeight:'bold',
        margin:10,
        textTransform:'uppercase'
    },
    descriptionText:{
        color:'white',
        fontSize:13,
        fontWeight:'500',
        margin:10,
        textTransform:'capitalize'
    },
    listContainer:{
        width:'45%',
        borderRadius:0,
        borderWidth:0,
        borderColor:colors.COLOR_PRIMARY,
        height:50,
        marginVertical:5,
        alignSelf:'center',
        overflow:'hidden',flexDirection:'row',alignItems:'center',marginHorizontal:10,
       // backgroundColor:colors.COLOR_PRIMARY
      },
      facilitiesImage:{
        height:20,width:20,
       
      },
      facilitiesText:{
          color:colors.COLOR_PRIMARY,
          fontWeight:'100',
          fontSize:13,
          marginLeft:10
      },
      imgContainer:{
        backgroundColor:colors.COLOR_PRIMARY,
        borderRadius:20,padding:10,
        justifyContent:'center',alignItems:'center',
        marginHorizontal:4
    }
   

})


export default ExperienceDetail;


