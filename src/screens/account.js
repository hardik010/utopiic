import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image, Keyboard
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';
import DocumentPicker from 'react-native-document-picker';


import { showMessage } from '../utils/snackmsg';


import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { postRequestApi, postRequestApiWithToken, postRequestNoBodyApiWithToken } from '../redux/actions/api_actions';
import config from '../config/config';
import { updateUser } from '../redux/actions/user_actions';
import PhoneInput from '../components/phone_input';

const Account: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
        const apiReducer = useSelector(state => state.apiReducer)

    const dispatch = useDispatch()

    const [governmentID, setGovernmentID] = useState(null);
    const [governmentIDSource, setGovernmentIDSource] = useState(null);

    const [idLink, setIdLink] = useState('');
    const [checked, setChecked] = useState(false);


    
  const nameInput = useRef();
  const nameRed = useRef();
  const emailInput = useRef();
  const emailRef = useRef();
  const addressInput = useRef();
  const addressRef = useRef();
  const ageInput = useRef();
  const ageRef = useRef();
  const phoneInput = useRef();
  const phoneRef = useRef();

  function validEmail(value){
    return  (/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(value))
  }
  function isValidNum(value){
    return (/^\d+$/).test(value)
  }
  
  function isValid(){
    Keyboard.dismiss()
    var result = false
    let email = emailInput.current.getInputValue()
    let name = nameInput.current.getInputValue()
    let age = ageInput.current.getInputValue()
    let address = addressInput.current.getInputValue()
    let phone = phoneInput.current.getInputValue()
    let country_code= phoneInput.current.getInputCode()


  
    if(name.length == 0) showMessage("Enter Name ");
    else if(country_code.length == 0) showMessage("Enter Country Code")
    else if(phone.length == 0) showMessage("Enter Phone Number")
    else if(phone.length < 9 ) showMessage("Enter valid  Phone Number")
    else if(!isValidNum(phone)) showMessage("Enter Valid Phone Number")
    else if(email.length == 0) showMessage("Enter Email")
    else if(!validEmail(email)) showMessage("Enter valid Email")
    else if(age.length == 0) showMessage("Enter Age")
    else if(!isValidNum(age)) showMessage("Enter Valid Age")
    else  if(address.length == 0) showMessage("Enter Address ");
    else result = true
  
    return result
  }
  

async function _onUpdate()  {
  
    if(isValid()){
      let email = emailInput.current.getInputValue()
      let name = nameInput.current.getInputValue()
      let age = ageInput.current.getInputValue()
      let address = addressInput.current.getInputValue()
      let phone = phoneInput.current.getInputValue()
      let code = phoneInput.current.getInputCode()
  
      var formdata = new FormData()
      formdata.append('name',name)
     // formdata.append('email',email)
    //formdata.append('phone',phone)
    //formdata.append('country_code',code)
      formdata.append('address',address)
      formdata.append('age',age)
      formdata.append('license',governmentID);
   
  
      const URL = config.BASE_URL + config.PROFILE_UPDATE
          
      dispatch(postRequestApiWithToken(URL,formdata,userReducer.user.token))
      .then(response=> {
         // console.log('Response - ',JSON.stringify(response))
          if(response.status) {
            showMessage("Profile Updated Successfully",true)
            dispatch(updateUser({'name':name,'address':address,'age':age}))
            props.navigation.navigate('home')
  
            
          }
          else showMessage(response.message)
      })
      .catch(error => {
          showMessage(error.message)
          //console.log('Error - ',error)
      })
     
     
    }
     
}
async function _onSignUps(){
    props.navigation.navigate('plans')
} 

async function _selectID(){
    try {
      const res = await DocumentPicker.pick({
       // type: [DocumentPicker.types.images,DocumentPicker.types.pdf],
       type: [DocumentPicker.types.images]
      });
      if(res && res.uri){
         var photo = {
              uri: res.uri,
              type:res.type,
              name: res.name,
          };
         setGovernmentID(photo) 
         setGovernmentIDSource(res.uri) 
      }
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
         showMessage('User cancelled the picker')
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        showMessage('Try Again')
      }
    }
  }

  function fetchProfile(){
    const URL = config.BASE_URL + config.USERS_PROFILE
    dispatch(postRequestNoBodyApiWithToken(URL,userReducer.user.token))
    .then(response=> {
        console.log('Response - ',JSON.stringify(response))
         if(response.status) {

          emailInput.current.setVal(response.data.email)
          nameInput.current.setVal(response.data.first_name)
          ageInput.current.setVal(response.data.age)
          addressInput.current.setVal(response.data.address)
          phoneInput.current.setVal(response.data.phone)
          phoneInput.current.setCode( '+'+response.data.country_code)
          setIdLink(response.data.license)

          
         }
         else showMessage(response.message)
    })
    .catch(error => {
         showMessage(error.message)
        // console.log('Error - ',error)
    })
}

    useEffect(() =>{
      const unsubscribe = props.navigation.addListener('focus', () => {
        // Screen was focused
        // Do something
        fetchProfile()
        emailInput.current.setVal(userReducer.user.email)
        nameInput.current.setVal(userReducer.user.name)
        ageInput.current.setVal(userReducer.user.age)
        addressInput.current.setVal(userReducer.user.address)
        phoneInput.current.setVal(userReducer.user.phone)
        //phoneInput.current.setCode('')
      });
        
    },[props.navigation])
    




 function _checkID(){

    if(governmentID === null && idLink.length == 0){
      return (
        <FastImage 
        source={require('../assets/id.png')}
        style={styles.buttonImage} 
        resizeMode={FastImage.resizeMode.contain}/>
          )
    }
    else if(governmentID === null && idLink.length > 0){
      return (  <FastImage 
        source={{uri:idLink}}
        style={styles.buttonImage} 
        resizeMode={FastImage.resizeMode.cover}/>)
    }
    else{
        return (  <FastImage 
            source={{uri:governmentIDSource}}
            style={styles.buttonImage} 
            resizeMode={FastImage.resizeMode.cover}/>)
    }


  }


  const onChange = e => {
    const input = e.currentTarget.value;
    if (/^[a-zA-Z]+$/.test(input) || input === "") {
     // setValue(input);
    }
    
  };


 return (
    <>
      <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
          <BackHeader {...props}/>
        <InputScrollView useAnimatedScrollView={true}
        contentContainerStyle={styles.scrollContainer}>

         <View style={{height:10}}/>
          <Text style={styles.heading}>ACCOUNT</Text>
          <View style={{height:50}}/>       

      
        

         <CustomTextInput
              ref={nameInput}
              placeholder={'Name'}
              onSubmitEditing={()=> phoneRef.current.focus()}
              inputRef={ nameRed }
              style={{width:'90%'}}
              returnKeyType="next"
         />

       

        

          
         <PhoneInput
            ref={phoneInput}
            placeholder={'Phone Number'}
            keyboardType="numeric"
            textContentType='telephoneNumber'
            maxLength={10}
            onSubmitEditing={()=>  emailRef.current.focus()}
            inputRef={phoneRef}
            style={{width:'81%'}}
            returnKeyType="next"
          />



        <CustomTextInput
              ref={emailInput}
              placeholder={'Email Address'}
              keyboardType={'email-address'}
              onSubmitEditing={()=> ageRef.current.focus()}
              inputRef={ emailRef }
              style={{width:'90%'}}
              returnKeyType="next"
         />

         
        

           
       
        <CustomTextInput
            ref={ageInput}
            placeholder={'Age'}
            keyboardType="numeric"
            textContentType='telephoneNumber'
            onSubmitEditing={()=> addressRef.current.focus()}
            //onChange={onChange}
           //restrict = text => text.replace(/[`~0-9!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '')
            inputRef={ageRef}
            style={{width:'90%'}}
            returnKeyType="next"
        />

           <CustomTextInput
              ref={addressInput}
              placeholder={'Billing Address'}
              keyboardType={'email-address'}
              //onSubmitEditing={()=> addressRef.current.focus()}
              inputRef={ addressRef }
              multiline={true}
              style={{width:'90%'}}
              returnKeyType="next"
         />

        <TouchableOpacity onPress={()=> _selectID()}
        style={styles.idContainer}>
            <View style={styles.rowContainer}>
                <Text style={{color:colors.COLOR_PRIMARY}}>Government ID</Text>
                {_checkID()}
                <View style={{width:40}}/>
            </View>
        </TouchableOpacity>
    

        <CustomButton 
        style={{width :dimensions.SCREEN_WIDTH * 0.77,marginTop:35,marginBottom:19}}
        handler={_onUpdate}
        label ={'Submit'}/>


    
                         

     




      </InputScrollView>

      {apiReducer.loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        backgroundColor:'black',
        flexGrow:1
      },
      heading:{
        color:colors.COLOR_PRIMARY,
        lineHeight:35,
        fontSize:20,
        fontWeight:'600',
        alignSelf:'center'
    },
    idContainer:{
        height:100,
        borderColor:colors.COLOR_PRIMARY,
        borderWidth:1,
        marginVertical:10,
        borderRadius:4,
        justifyContent:'center',
        padding:10
    },
    rowContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    }, buttonImage:{
      height:80,width:80,margin:4
  }

})


export default Account;


