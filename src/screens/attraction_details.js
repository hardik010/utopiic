import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';

import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { postRequestNoBodyApi, postRequestNoBodyApiWithToken } from '../redux/actions/api_actions';
import config from '../config/config';
import HomeHeader from '../components/home_header';
import Carousel from 'react-native-banner-carousel';
import MapView , {PROVIDER_GOOGLE} from 'react-native-maps';




const AttractionDetails: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const apiReducer = useSelector(state => state.apiReducer)

    const dispatch = useDispatch()

    const [details, setDetails] = useState(''); 
    const [images, setImages] = useState(''); 
    const [moreVisibility, setMoreVisibility] = useState(false); 
    const [addressRegion,setAddressRegion] = useState({
        latitude: 37.78825,
        longitude: -122.4324,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
       })

    function fetchDetails(id){
        const URL = config.BASE_URL + config.ATTRACTIONS_DETAIL + id
        dispatch(postRequestNoBodyApiWithToken(URL,userReducer.user.token))
        .then(response=> {
           // console.log('Response - ',JSON.stringify(response))
            if(response.status){
                setDetails(response.data.details)
                setImages(response.data.images)
                let temp ={
                    latitude:parseFloat(response.data.details.lat),
                    longitude:parseFloat(response.data.details.lng),
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }
                setAddressRegion(temp)
            }
        })
        .catch(error => {
            showMessage(error.message)
           // console.log('Error - ',error)
        })
    }

    useEffect(() =>{
        //console.log(props.route.params.id)
        fetchDetails(props.route.params.id)
    },[])

    function renderPage(item,index){
        return (
            <View key={index}>
                 <FastImage
                style={{ width: dimensions.SCREEN_WIDTH, height: '100%' }}
                source={{ uri: item,
                    priority: FastImage.priority.normal,
                }}
            resizeMode={FastImage.resizeMode.cover}/>
            </View>
        );
    }

    function _getAbout(){
        if(moreVisibility){
          return(
              <View>
            <Text  style={styles.aboutText}>
                {  details.description+ '  ' } </Text>
                <View style={styles.divider}/>
               <TouchableOpacity onPress={()=> setMoreVisibility(!moreVisibility)}
                 style={styles.readContainer}>
                    <Text style={styles.readText}>Done </Text>
                </TouchableOpacity>
           
            </View>
          )
        }
        else{
          return(
            <View>
            <Text  style={styles.aboutText}>
                {  details.description.substring(0,200) + '  ' }
            </Text>
            <View style={styles.divider}/>
             <TouchableOpacity onPress={()=> setMoreVisibility(!moreVisibility)}
                 style={styles.readContainer}>
                    <Text style={styles.readText}>Read More </Text>
                </TouchableOpacity>
            </View>
          )
        }
      }
   
 return (
    <>
 <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
       <SafeAreaView style={styles.container}>
      <HomeHeader haveBack {...props}/> 
      <ScrollView 
        bounces={false}
        useAnimatedScrollView={true}
        contentContainerStyle={styles.scrollContainer}>

             {/** image  */}
            <View style={styles.imageContainer}>
                {
                images && images.length >0 ?
                <Carousel
                style={{alignSelf:'center'}}
                autoplay  autoplayTimeout={4000}
                in
                loop index={0} pageSize={dimensions.SCREEN_WIDTH}>
                {images.map((image, index) => renderPage(image, index))}
                </Carousel>  
                :null

                }
            </View>
        {/** image end */}

           <View style={{height:15}}/>
           <View style={{margin:10}}>
           <Text style={styles.nameText}>{details.name}</Text>
           <Text style={{color:'white'}}>{details.city_name} {details.country_name}</Text>
           <View style={{height:10}}/>
        

          

        {
           details.description &&   details.description.length > 200 ?
             _getAbout()
           :
            <Text style={styles.aboutText}>{  details.description ?   details.description : ''}
            </Text>
         }

       </View>

            
      <View  pointerEvents={'none'} style = {styles.mapContainer}>
           <MapView
             style = {{height: '100%', width: '100%'}}
             initialRegion={{
                latitude: 28.78825,
                longitude: 77.4324,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              }}
               region={addressRegion}
                showsUserLocation = {true}
                followUserLocation = {true}
                zoomEnabled = {true}
         />
      </View>
           


      </ScrollView>
      {apiReducer.loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        flexGrow:1
    },
   
    rowContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    imageContainer:{
        width:dimensions.SCREEN_WIDTH,
        height:dimensions.SCREEN_HEIGHT * 0.35,
        backgroundColor:colors.COLOR_PRIMARY
    },
    nameContainer:{
        width:dimensions.SCREEN_WIDTH,
        padding:20,
        justifyContent:'center',
        alignItems:'center',
        position:'absolute',
        top:0,
        left:0,
        right:0,
        backgroundColor:'rgba(0,0,0,0.0001)'
    },
    nameText:{
        color:colors.WHITE,
        fontSize:20,fontWeight:'bold',
        textTransform:'uppercase'
    },
    heading:{
        color:'white',
        fontSize:16,
        fontWeight:'bold',
        margin:10,
        textTransform:'uppercase'
    },
    descriptionText:{
        color:'white',
        fontSize:13,
        fontWeight:'500',
        margin:10,
        textTransform:'capitalize'
    },
    
    readContainer:{
      backgroundColor:colors.COLOR_PRIMARY,
      paddingHorizontal:7,
      paddingVertical:6,
      alignItems:'center',
      justifyContent:'center',
      width:dimensions.SCREEN_WIDTH*0.18,
      marginTop:10
    },
    readText:{
      fontSize:11
    },
      aboutText:{
          color:colors.WHITE,
          fontSize:13,
          lineHeight:27
     },
     divider:{
         backgroundColor:colors.COLOR_PRIMARY,
         height:1,
         width:dimensions.SCREEN_WIDTH,
         marginVertical:6

     },
     mapContainer:{
         height:200, width:dimensions.SCREEN_WIDTH* 0.99,marginBottom:0,
        overflow:'hidden',borderRadius:0,alignSelf:'center',
        marginVertical:15
       }

})


export default AttractionDetails;


