import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TextInput, Keyboard
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';


import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { color } from 'react-native-reanimated';
import config from '../config/config';
import { postRequestApiWithToken } from '../redux/actions/api_actions';




const Otp: () => React$Node = (props) => {

      const apiReducer = useSelector(state => state.apiReducer)
      const dispatch = useDispatch()

    const [loading, setLoading] = useState(false); 
    const [otp, setOtp] = useState(''); 


    useEffect(() =>{
      //console.log(props.route.params.profile)
      setOtp(props.route.params.profile.otp.toString())
    },[])

  
    function isValid(){
      var result = false
    
      Keyboard.dismiss()
      if(otp.length == 0) showMessage("Enter OTP")
      else if(otp.length != 4) showMessage("Enter Valid OTP")
      else result = true
    
      return result
    }
    

    function verifyOtp(){
     if(isValid()){
      const URL = config.BASE_URL + config.OTP_VERIFICATION
      var formData = new FormData()
      formData.append('user_id',props.route.params.profile.user_id)
      formData.append('otp',otp)
      dispatch(postRequestApiWithToken(URL,formData,props.route.params.profile.token))
    .then(response=> {
        //console.log('Response - ',JSON.stringify(response))
        if(response.status) {
           props.navigation.navigate('interest',{profile:props.route.params.profile})
           showMessage('OTP Verified Successfully.Please wait till account is verified by admin',false)
        }
        else {
          showMessage(response.message)
        }
    })
    .catch(error => {
        showMessage(error.message)
        //console.log('Error - ',error)
     })
     }
    }



    function _onOkay(){
         props.navigation.pop()
    }

   
 return (
    <>
      <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
       <BackHeader {...props}/>
     <KeyboardAwareScrollView 
        bounces={false}
        useAnimatedScrollView={true}
        contentContainerStyle={styles.scrollContainer}>

          <Text style={[styles.heading,{marginTop:-100}]}>Confirmation</Text>
          <View style={{height:10}}/> 
          <Text style={styles.headingSub}>Code message has been sent to your phone number.And submit OTP below to verify your acccount.</Text>  
          <View style={{height:40}}/> 

       
          <Text style={styles.labelText}>OTP CODE:</Text>

            <TextInput
              value={otp}
              editable={false}
              underlineColorAndroid = 'transparent' 
              maxLength={4}
              style = { styles.textBox }
              onChangeText={(value) => {
                 setOtp(value)
              }}
              onSubmitEditing={()=> verifyOtp()}
              keyboardType="numeric"
              textContentType='telephoneNumber'
            />

         <View style={{height:30}}/> 
          <CustomButton 
            style={{width :dimensions.SCREEN_WIDTH * 0.8,
            marginTop:15,marginBottom:19,alignSelf:'center'}}
            handler={verifyOtp}
            label ={'Submit'}/>
        <Text style={styles.labelText}>Didn't recieved any message ? Resend Code</Text>


      </KeyboardAwareScrollView>

      {apiReducer.loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        justifyContent:'center',
        flexGrow:1
    },
    heading:{
        color:colors.COLOR_PRIMARY,
        lineHeight:30,
        fontSize:20,
        fontWeight:'500',
        alignSelf:'center',
        textAlign:'center',
        maxWidth:'70%'
    },
    headingSub:{
        color:colors.COLOR_PRIMARY,
        fontWeight:'400',
        fontSize:13,
        width:'70%',
        textAlign:'center'
    },
    rowContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    labelText:{
        color:colors.COLOR_PRIMARY,
        fontSize:13
    },
   textBox:{
    borderBottomColor:colors.COLOR_PRIMARY,
    borderBottomWidth:1,
    backgroundColor:colors.BLACK,
    paddingHorizontal:5,
    paddingVertical:9,
    marginHorizontal:0,
    marginVertical:10,
    height:50,
    width:'80%',
    color:colors.DARK_GREY,
    textAlign:'center'
   }

})


export default Otp;


