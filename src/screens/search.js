
import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image, Keyboard
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';

import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { postRequestApiWithToken, postRequestNoBodyApi } from '../redux/actions/api_actions';
import config from '../config/config';
import ProgressWebView from "react-native-progress-webview";

import WebView from 'react-native-webview';
import SearchInput from '../components/search_input';
import SearchView from '../components/search_view';
import SearchListComponent from '../components/search_list_component';


const SUGGESTIONS =[
  {
    'id':1,
    'name':'Attractions',
    'path':'attractions'
  },
  {
    'id':2,
    'name':'Fashion',
    'path':'fashion_brands'
  },
  {
    'id':3,
    'name':'Country',
    'path':'countries'
  },
  {
    'id':4,
    'name':'Hotels',
    'path':'hotels'
  },
  {
    'id':5,
    'name':'Home',
    'path':'home_brands'
  },
  
  {
    'id':6,
    'name':'Fitness',
    'path':'fitness_brands'
  },
  {
    'id':7,
    'name':'Experiences',
    'path':'experiences'
  },
  {
    'id':8,
    'name':'Food',
    'path':'food_brands'
  },
  {
    'id':9,
    'name':'Brands',
    'path':'brands'
  }
]


const Search: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const apiReducer = useSelector(state => state.apiReducer)
    const [data, setData] = useState([]); 
    const [suggestions, setSuggestions] = useState([]); 

    const dispatch = useDispatch()

    function search(text){
        const URL = config.BASE_URL + config.SEARCH_API
        var formData = new FormData()
        formData.append('search',text)
        dispatch(postRequestApiWithToken(URL,formData,userReducer.user.token))
        .then(responseJson=> {
           // console.log('Response - ',responseJson)
           if(responseJson.status){
                setData(responseJson.data)
                if(responseJson.data.length == 0) showMessage("No Result Found")
           }
        })
        .catch(error => {
            showMessage(error.message)
            //console.log('Error - ',error)
        })
    }

    function shuffle(array) {
      var currentIndex = array.length, temporaryValue, randomIndex;
    
      // While there remain elements to shuffle...
      while (0 !== currentIndex) {
    
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
    
        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
      }
    
      return array;
    }

    function initialize(){
      setSuggestions(shuffle(SUGGESTIONS).slice(0,6))

    }

    useEffect(() =>{
      initialize()
    },[])


    const searchInput = useRef();
    const searchRef = useRef();

  function _onSearch (){
      var searchText = searchInput.current.getInputValue()
      //console.log(searchText)
      if(searchText == 'Country' || searchText == 'Country'){
        props.navigation.navigate('countries')
      }
      else if(searchText == 'Attractions' || searchText == 'attractions' ) {
        props.navigation.navigate('attractions')
      }
      else if(searchText == 'Experiences' || searchText == 'experiences' ) {
        props.navigation.navigate('experiences')
      }
      else if(searchText == 'Hotels' || searchText == 'hotels' ) {
        props.navigation.navigate('hotels')
      }
      else if(searchText == 'Fashion' || searchText == 'fashion' ) {
        props.navigation.navigate('fashion_brands')
      }
      else if(searchText == 'Home' || searchText == 'home' ) {
        props.navigation.navigate('home_brands')
      }
      else if(searchText == 'Fitness' || searchText == 'fitness' ) {
        props.navigation.navigate('fitness_brands')
      }
      else if(searchText == 'Food' || searchText == 'food' ) {
        props.navigation.navigate('food_brands')
      }
      else if(searchText == 'Brands' || searchText == 'brands' ) {
        props.navigation.navigate('brands')
      }
      else{
        search(searchText)
      }
      Keyboard.dismiss()
    }


    function navigateToDetails(item){
        if(item.type == 'Hotel') props.navigation.navigate('brand_details',{item:item})
        else if(item.type == 'Lifestyle') props.navigation.navigate('brand_details',{item:item})
        else if(item.type == 'Attraction') props.navigation.navigate('attraction_details',{id:item.id})
        else if(item.type == 'Country') props.navigation.navigate('country_details',{id:item.id})
        else if(item.type == 'Packages') props.navigation.navigate('experience_detail',{id:item.id})
        else showMessage("Coming Soon",false)
    }
   

    function renderItem (item,index)  {
        return(
            <TouchableOpacity onPress={() => navigateToDetails(item)}>
                <SearchListComponent image={item.image} 
                name={item.name} subName={item.type}/>
            </TouchableOpacity>
        )
    }
    function navigate(item){
      searchInput.current.setVal(item.name)
      props.navigation.navigate(item.path)
    }

    function renderSuggestions (item,index)  {
      return (
          <TouchableOpacity onPress={()=> {navigate(item)}}
          style={styles.suggestionContainer}>
            <Text style={{color:'white'}}>{item.name}</Text>
          </TouchableOpacity>
        )
    }
   
 return (
    <>
      <StatusBar barStyle="light-content" backgroundColor={colors.BLACK}/>
      <SafeAreaView style={styles.container}>
      <BackHeader {...props}/>
      <Text style={styles.heading}>SEARCH</Text>    
        <SearchInput 
         placeholder={'Search ..'}
         onSubmitEditing={()=> _onSearch()}
         ref={searchInput}
         autoFocus={true}
         inputRef={ searchRef }
         style={{width :dimensions.SCREEN_WIDTH * 0.7,alignSelf:'center'}}/>

           <FlatList
            style={{marginVertical:10}}
            horizontal={true}
            numColumns={1}
            extraData={suggestions}
            data={suggestions}
            showsHorizontalScrollIndicator={false}
            renderItem={({ item ,index}) => renderSuggestions(item,index)}
            keyExtractor={item => item.id}
           /> 


        <View style={{height:20}}/>

        <FlatList 
            contentContainerStyle={{alignItems:'center'}}
            showsVerticalScrollIndicator={false}
            data={data}
            renderItem={({ item ,index}) => renderItem(item,index)}
            keyExtractor={(item) => item.id } />   


      {apiReducer.loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        justifyContent:'center',
        flexGrow:1,
        backgroundColor:'white',
    },
   
    rowContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    heading:{
        color:colors.COLOR_PRIMARY,
        lineHeight:26,
        fontSize:19,
        fontWeight:'700',
        alignSelf:'center',
        maxWidth:'90%',
        marginVertical:12
    },
    suggestionContainer:{
      paddingHorizontal:13,
      paddingVertical:7,
      borderRadius:20,
      borderColor:colors.COLOR_PRIMARY,
      marginHorizontal:7,
      marginVertical:5,
      borderWidth:1
    }
   

})


export default Search;


