import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TextInput
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import RadioButton from 'react-native-radio-button'
import { showMessage } from '../utils/snackmsg';
import Carousel from 'react-native-banner-carousel';


import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/custom_textinput';
import ProgressBar from '../components/progress_bar';
import { CheckBox } from 'react-native-elements'; // 0.16.0
import InputScrollView from 'react-native-input-scroll-view';
import CustomButton from '../components/custom_button';
import BackHeader from '../components/back_header';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { color } from 'react-native-reanimated';
import HomeHeader from '../components/home_header';
import BottomTab from '../components/bottom_tab';
import ExperienceComponent from '../components/experience_component';


const BANNER  =[
    {  title:'Luxury Travel',
     image: require('../assets/banner_1.png')
},
    {  title:'Luxury Plan',
    image: require('../assets/banner_1.png')
},
    {  title:'Luxury Tour',
    image: require('../assets/banner_1.png')
},
]

const EXPERIENCES  =[
    { 
        title:'Wah Tea Estate',
       description:'A Utopiic Place of stay ',
       country:'India',
       image: require('../assets/banner_1.png')
    },
    { 
        title:'Wah Tea Estate',
        description:'A Utopiic Place of stay ',
        country:'India',
        image: require('../assets/banner_1.png')
    },
    { 
        title:'Wah Tea Estate',
        description:'A Utopiic Place of stay ',
        country:'India',
        image: require('../assets/banner_1.png')
    },
]

const WhatsNext: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false); 

    useEffect(() =>{
    },[])

    function renderPage(item,index){
        return (
            <TouchableOpacity  onPress={()=> console.log('llllllllllllllll')}
             key={index}>
                <Image style={styles.bannerImage}  source={require('../assets/banner_1.png')} />
            </TouchableOpacity>
        );
    }

    function _renderExperiences ({item, index}){
        return (
           <ExperienceComponent
            object = {item} 
            index ={index}
            {...props}/>
        )
      }


  return (
    <>
    <HomeHeader {...props}/>
    <ScrollView  bounces={false} style={{flex:1,backgroundColor:'black'}}>
        <View style={{height:10}}/>
        <Carousel
            style={{alignSelf:'center'}}
            autoplay  autoplayTimeout={4000}
            in
            loop index={0} pageSize={dimensions.SCREEN_WIDTH}
        >
            {BANNER.map((image, index) => renderPage(image, index))}
        </Carousel>  

        <View style={{height:20}}/>

         {/** Past webimar */}
         <View style={styles.listContainer}>
              <View style={styles.rowContainer}>
                   <Text style={styles.listLabel}>PAST WEBINAR</Text>
                    <TouchableOpacity style={styles.rowContainer} onPress={()=>{} }>
                        <Text style={styles.viewLabel}>View all</Text>
                        <FastImage 
                        source={require('../assets/back.png')}
                        style={styles.nextImage} 
                        resizeMode={FastImage.resizeMode.cover}/>
                     </TouchableOpacity>
                </View>


                <FlatList
                    style={{marginVertical:10,alignSelf:'center'}}
                    horizontal={true}
                    data={EXPERIENCES}
                    showsHorizontalScrollIndicator={false}
                    renderItem={(item,index) => _renderExperiences(item,index)}
                    keyExtractor={item => item.id}
                />

               </View>

              {/** Past webimar end */}





              {/** destinations */}
          <View style={styles.listContainer}>
              <View style={styles.rowContainer}>
                   <Text style={styles.listLabel}>Substainable Blog</Text>
                    <TouchableOpacity style={styles.rowContainer} onPress={()=>{} }>
                        <Text style={styles.viewLabel}>View all</Text>
                        <FastImage 
                        source={require('../assets/back.png')}
                        style={styles.nextImage} 
                        resizeMode={FastImage.resizeMode.cover}/>
                     </TouchableOpacity>
                </View>


                <FlatList
                    style={{marginVertical:10,alignSelf:'center'}}
                    horizontal={true}
                    data={EXPERIENCES}
                    showsHorizontalScrollIndicator={false}
                    renderItem={(item,index) => _renderExperiences(item,index)}
                    keyExtractor={item => item.id}
                />

               </View>

              {/** Destinations end */}



              {/** Green Brands */}
          <View style={styles.listContainer}>
              <View style={styles.rowContainer}>
                   <Text style={styles.listLabel}>Podcast</Text>
                    <TouchableOpacity style={styles.rowContainer} onPress={()=>{} }>
                        <Text style={styles.viewLabel}>View all</Text>
                        <FastImage 
                        source={require('../assets/back.png')}
                        style={styles.nextImage} 
                        resizeMode={FastImage.resizeMode.cover}/>
                     </TouchableOpacity>
                </View>


                <FlatList
                    style={{marginVertical:10,alignSelf:'center'}}
                    horizontal={true}
                    data={EXPERIENCES}
                    showsHorizontalScrollIndicator={false}
                    renderItem={(item,index) => _renderExperiences(item,index)}
                    keyExtractor={item => item.id}
                />

               </View>

              {/** Green Brands end */}




              {/** Events */}
          <View style={styles.listContainer}>
              <View style={styles.rowContainer}>
                   <Text style={styles.listLabel}>Travel blog</Text>
                    <TouchableOpacity style={styles.rowContainer} onPress={()=>{} }>
                        <Text style={styles.viewLabel}>View all</Text>
                        <FastImage 
                        source={require('../assets/back.png')}
                        style={styles.nextImage} 
                        resizeMode={FastImage.resizeMode.cover}/>
                     </TouchableOpacity>
                </View>


                <FlatList
                    style={{marginVertical:10,alignSelf:'center'}}
                    horizontal={true}
                    data={EXPERIENCES}
                    showsHorizontalScrollIndicator={false}
                    renderItem={(item,index) => _renderExperiences(item,index)}
                    keyExtractor={item => item.id}
                />

               </View>

              {/** Events end */}






    </ScrollView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1, backgroundColor:'black',
    },
    scrollContainer:{
        padding:10,
        flexGrow:1
    },
    heading:{
        color:colors.WHITE,
        lineHeight:26,
        fontSize:13,
        fontWeight:'500',
        alignSelf:'center',
        maxWidth:'90%',
        marginVertical:12
    },
    headingSub:{
        color:colors.COLOR_PRIMARY,
        fontWeight:'400',
        fontSize:13,
        width:'70%',
        textAlign:'center'
    },
    rowContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    labelText:{
        color:colors.COLOR_PRIMARY,
        fontSize:13
    },
    bannerImage:{
         width: dimensions.SCREEN_WIDTH * 0.99, 
        height: dimensions.SCREEN_HEIGHT * 0.22,
        alignSelf:'center',borderRadius:0,marginTop:0,backgroundColor:'#808080' 
    },
   listContainer:{
       width:'100%',
       padding:10,
       marginVertical:12
   },
   listLabel:{
       color:colors.COLOR_PRIMARY,
       textTransform:'uppercase',
       fontWeight:'500',
       fontSize:16
    },
    nextImage:{
        height:25,width:25,marginHorizontal:6
    },
    viewLabel:{
        color:colors.COLOR_PRIMARY,
        textTransform:'uppercase',
        fontWeight:'400',
        fontSize:12
     },

    
    
})


export default WhatsNext;


