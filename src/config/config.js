/**********************************************************************************************
 * File: Config file to store configuration used in application
********************************************************************************************* */
const MOBILE_NUM_FORMAT = /^[\s()+-]*([0-9][\s()+-]*){6,15}$/

const NAME_FORMAT = new RegExp('^[a-zA-Z0-9][a-zA-Z0-9 ]*$');

const EMAIL_FORMAT = /[a-zA-Z0-9!#$%&'*+\=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+\=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/;

const PASSWORD_POLICY = new RegExp('^(?=.{8,})(?=.*[0-9])(?=.*[a-z])');

/*************Base URL***********/
const BASE_URL = 'http://admin.utopiic.com/rest/'
const BASE_URL_MEDIA = 'http://admin.utopiic.com/'

/***********API URLs******************************************/
const REGISTER = 'auth/register'
const LOGIN = 'auth/passcode'
const PREFERENCES = '/package/preference'
const HOME = 'general/home'
const PACKAGES = 'package/packages'
const PACKAGES_TYPES = 'package/package_types'
const COUNTRIES = 'general/counties'
const USER_PLAN = 'general/userPlan'
const COUNTRY_DETAILS = 'general/country_details/'
const OTP_VERIFICATION = 'auth/verification'
const BRANDS_LIST = 'general/brands'
const TERMS_AND_CONDITIONS = 'cms/terms'
const PRIVACY_POLICY = 'cms/privacy'
const GET_SUPPLIERS  = 'hotels/getsuppliers'
const COUPON_LIST  =  'hotels/coupon_list'
const CHECK_AUTH_TOKEN  =  'auth/checkToken'
const EXPERIENCE_DETAIL  =  'package/details/'
const ATTRACTIONS_LIST  =  'general/attractions'
const ATTRACTIONS_DETAIL  =  'general/attraction_details/'
const PROFILE_UPDATE  =  'users/profile_update/'
const USERS_PROFILE  =  'users/profile/'
const SUBSCRIPTION_PAYMENT  =  'payment/subscriptionPayment/'
const SUBSCRIPTION_PAYMENT_LIST  =  'payment/subscriptionPaymentList/'
const PACKAGE_BOOKING  =  'package/booking/'
const TRAVEL_PACKAGE_BOOKING  =  'package/travelPackagebooking/'
const PACKAGE_BOOKING_HISTORY  =  'package/bookinghistory/'
const EXCLUSIVE_OFFER_APPLY  =  '/hotels/exclusiveOfferApply'
const SEARCH_API  =  '/general/search'
const SUPPLIER_DETAIL  =  '/general/supplier_detail'
const START_TOKEN  =  '/auth/startToken'
const LOGOUT  =  '/users/logout'
const GET_NATIVES  =  '/general/getNatives'
const PAYMENT_API  =  '/payment/paymentApi'
const BOOKING_CANCELLED  =  '/booking/bookingCancelled'

/************************************************************/

/*******API Status Codes*******/
const statusOk = 200
const statusNotFound = 404
const statusError = 401
const statusUnauthorized = 400
const statusProcessing = 202

/******************************/

export default {
    MOBILE_NUM_FORMAT,
    NAME_FORMAT,
    EMAIL_FORMAT,
    PASSWORD_POLICY,
    BASE_URL,
    BASE_URL_MEDIA,
    ATTRACTIONS_DETAIL,
    PROFILE_UPDATE,
    SUBSCRIPTION_PAYMENT,
    EXCLUSIVE_OFFER_APPLY,
    SEARCH_API,
    PAYMENT_API,
    BOOKING_CANCELLED,
    GET_NATIVES,
    SUBSCRIPTION_PAYMENT_LIST,
    PACKAGE_BOOKING_HISTORY,
    PACKAGE_BOOKING,
    TRAVEL_PACKAGE_BOOKING,
    COUNTRIES,
    REGISTER,
    PREFERENCES,
    COUNTRY_DETAILS,
    ATTRACTIONS_LIST,
    SUPPLIER_DETAIL,
    LOGOUT,
    LOGIN,
    CHECK_AUTH_TOKEN,
    COUPON_LIST,
    USER_PLAN,
    USERS_PROFILE,
    OTP_VERIFICATION,
    GET_SUPPLIERS,
    BRANDS_LIST,
    EXPERIENCE_DETAIL,
    HOME,
    PACKAGES,
    PACKAGES_TYPES,
    START_TOKEN,
    TERMS_AND_CONDITIONS,
    PRIVACY_POLICY,
    statusOk,
    statusNotFound,
    statusUnauthorized,
    statusError,
    statusProcessing
}